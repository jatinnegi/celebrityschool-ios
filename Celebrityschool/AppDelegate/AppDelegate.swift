//
//  AppDelegate.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.
//

import UIKit
import IQKeyboardManagerSwift
import Smartech
import SmartPush
import Firebase
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SmartechDelegate ,MessagingDelegate {

    var window: UIWindow?
    var navigateView = CustomNavigationController()
    var myOrientation: UIInterfaceOrientationMask = .portrait

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       
        IQKeyBoardHandling()
        smartTechSetUp(launchOptions)
        firebaseSetUp()
        checkAppUpgrade()
        navigateView.setSplashViewController()
        ApplicationDelegate.shared.application(
                    application,
                    didFinishLaunchingWithOptions: launchOptions
                )
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        AppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//        SocketHelper.shared.closeConnection()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        SocketHelper.shared.establishConnection()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(
            _ app: UIApplication,
            open url: URL,
            options: [UIApplication.OpenURLOptionsKey : Any] = [:]
        ) -> Bool {
            ApplicationDelegate.shared.application(
                app,
                open: url,
                sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                annotation: options[UIApplication.OpenURLOptionsKey.annotation] as Any
            )
        }

    func IQKeyBoardHandling(){
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = ""
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(CommunityChatsViewController.self)

    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return myOrientation
    }
    
    
    func rotateToLandsScapeDevice(){
        myOrientation = .landscapeRight
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
    
    
    func rotateToPotraitScapeDevice(){
        myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UIView.setAnimationsEnabled(true)
    }
    
    func firebaseSetUp(){
        if(FirebaseApp.app() == nil){
            FirebaseApp.configure()
            Messaging.messaging().delegate = self
            Messaging.messaging().isAutoInitEnabled = true
        }
    }
    
    func remoteNotificaionToken(_ token:Data){
        Messaging.messaging().apnsToken = token
        SmartPush.sharedInstance().didRegisterForRemoteNotifications(withDeviceToken: token)
    }
    
    func failToRegisterForRemoteNotifications(_ error: Error){
        SmartPush.sharedInstance().didFailToRegisterForRemoteNotificationsWithError(error)

    }
    
    func smartTechSetUp(_ launchOptions:[UIApplication.LaunchOptionsKey: Any]?){
        Smartech.sharedInstance().initSDK(with: self, withLaunchOptions: launchOptions)
        UNUserNotificationCenter.current().delegate = self
        SmartPush.sharedInstance().registerForPushNotification(authorizationOptions: [.alert, .badge, .sound])
        Smartech.sharedInstance().setDebugLevel(SMTLogLevel.verbose)
        Smartech.sharedInstance().trackAppInstallUpdateBySmartech()
   
    }
    
    func checkAppUpgrade() {
        let currentVersion = Bundle.main.object(forInfoDictionaryKey:     "CFBundleShortVersionString") as? String
        let versionOfLastRun = UserDefaults.standard.object(forKey: "VersionOfLastRun") as? String

        if versionOfLastRun == nil {
            // First start after installing the app
            Smartech.sharedInstance().trackAppInstall()

        } else if versionOfLastRun != currentVersion {
            // App was updated since last run
            Smartech.sharedInstance().trackAppUpdate()
        } else {
            // nothing changed

        }

        UserDefaults.standard.set(currentVersion, forKey: "VersionOfLastRun")
        UserDefaults.standard.synchronize()
    }
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    static func jsonString(from object:Any) -> String? {
        
        guard let data = jsonData(from: object) else {
            return nil
        }
        
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    static func jsonData(from object:Any) -> Data? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        
        return data
    }
    
}
