//
//  CertificateDetailViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 15/09/21.
//

import UIKit
import WebKit

class CertificateDetailViewController: UIViewController {

    
    @IBOutlet weak var containWebView: WKWebView!
    var albumid:String = ""
    
    private var viewModel: CertificateApiServices!
    var pdfString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CertificateApiServices()
        setupViewModel()
        // Do any additional setup after loading the view.
    }
    
    private func setupViewModel() {
        
        viewModel.fetchCertificateApiCall(albumid)
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            
            DispatchQueue.main.async {
                if let base64String = data.certificate_b64{
                    strongSelf.pdfString = base64String
                    if let decodeData = Data(base64Encoded: base64String, options: .ignoreUnknownCharacters) {
                        strongSelf.containWebView.load(decodeData, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: URL(fileURLWithPath: ""))
                    }
                }
            }
        }

    }
    
    
    func savePdf() throws {
        let documentsURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let pdfDocURL = documentsURL.appendingPathComponent("CelebrityCertificate\(albumid).pdf")
        let pdfData = Data(base64Encoded: pdfString)
        try pdfData!.write(to: pdfDocURL)
    }

    func loadPDFAndShare(){
        do {
            let documentsURL = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let pdfDocURL = documentsURL.appendingPathComponent("CelebrityCertificate\(albumid).pdf")

            let document = NSData(contentsOf: pdfDocURL)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [document!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView=self.view
            present(activityViewController, animated: true, completion: nil)
            print("document was not found")
        } catch  {
            print("document was not found")
        }
    }
    
    
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CertificateDetailViewController{
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ShareButtonAction(_ sender: Any) {
        do {
            try savePdf()
            loadPDFAndShare()
        } catch  {
            print("FALLO EL GUARDAR EL PDF")
        }
    }
    @IBAction func downloadButtonAction(_ sender: Any) {
        do {
            try savePdf()
            if let topController = UIApplication.topViewController(){
                AlertIndicator_Toast().showToast("Certificate Downloaded Successfully.", vc: topController)
            }
        } catch  {
            print("FALLO EL GUARDAR EL PDF")
        }
    }
}


