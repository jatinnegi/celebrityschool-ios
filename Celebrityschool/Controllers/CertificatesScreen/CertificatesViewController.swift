//
//  CertificatesViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 30/08/21.
//

import UIKit

class CertificatesViewController: CustomNavigationController {

    @IBOutlet weak var CertificatesCollectionView: UICollectionView!{
        didSet{
            CertificatesCollectionView.delegate = self
            CertificatesCollectionView.dataSource = self
            CertificatesCollectionView.register(CertificateCollectionViewCell.nib, forCellWithReuseIdentifier: CertificateCollectionViewCell.reuseIdentifier)
        }
    }
    
    var courseData:[CourseModel]?
    var lockHide:Bool?
    
    @IBOutlet weak var cc_CollcetionViewBottom: NSLayoutConstraint!
    @IBOutlet weak var cc_CollcetionViewTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var topSafeArea: CGFloat = 0.0
        var bottomSafeArea: CGFloat = 0.0

        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
            bottomSafeArea = view.safeAreaInsets.bottom
        } else {
            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        
        cc_CollcetionViewTop.constant = -topSafeArea
        cc_CollcetionViewBottom.constant = +bottomSafeArea

        // safe area values are now available to use
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.leftBarButtonItem = backBarButton()
        self.navigationItem.title = "Courses Certificates"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension CertificatesViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return courseData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CertificateCollectionViewCell.reuseIdentifier, for: indexPath) as! CertificateCollectionViewCell
        cell.lockUnlockImage.isHidden = lockHide ?? false
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let chekc = lockHide, chekc == true{
            if let albumid = courseData?[indexPath.row].id{
                CustomNavigationController().CertificateDetailViewControllerCall("\(albumid)")
            }
        }else{
            if let topController = UIApplication.topViewController(){
                AlertIndicator_Toast().showToast(Constants.KProfileNotCompleted, vc: topController)
            }
        }
    }
    
    
}

extension CertificatesViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width :CGFloat = collectionView.frame.width
        switch UIDevice().type {
        case .iPad2, .iPad3, .iPad4 , .iPad5 , .iPad6 , .iPad7 , .iPad8, .iPad9, .iPadAir, .iPadAir2, .iPadAir3, .iPadAir4, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadMini6, .iPadPro9_7, .iPadPro10_5, .iPadPro11, .iPadPro2_11, .iPadPro3_11, .iPadPro12_9, .iPadPro2_12_9, .iPadPro3_12_9, .iPadPro4_12_9, .iPadPro5_12_9:
            width = width / 2.1
            case .unrecognized:
                print("Device model unrecognized");
            default:break
        }
        
        let height:CGFloat = 237.0
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top:10, left: 0, bottom:10, right: 0)
    }
    
}
