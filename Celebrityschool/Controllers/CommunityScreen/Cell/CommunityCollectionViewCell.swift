//
//  CommunityCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 16/11/21.
//

import UIKit
import SDWebImageWebPCoder


class CommunityCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "CommunityCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CommunityCollectionViewCell", bundle: nil)
        }
    }

    @IBOutlet weak var lastChatLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!{
        didSet{
            categoryLabel.text = ""
        }
    }
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
//            profileImageView.image = UIImage(named: "profile_icon")
            profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
            profileImageView.layer.masksToBounds = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        profileImageView.sd_cancelCurrentImageLoad()
    }

    func loadCell(_ model:CommunityCategoryModel){
        
        if let image = model.image{
            if let url = URL(string:image){
                profileImageView.sd_setImage(with: url, placeholderImage:  UIImage(named: "profile_icon"))
                profileImageView.contentMode = .scaleAspectFill
            }
        }
        
        if let communityType = model.communityType{
            categoryLabel.text = communityType
        }
        
        if let lastMessage = model.lastmessage{
            lastChatLabel.text = lastMessage.message
            if let createAt = lastMessage.createdAt{
                dateLabel.text = createAt.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outGoingFormat: "h:mm a")
            }
        }
    }
}
