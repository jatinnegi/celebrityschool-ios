//
//  MessageSendTableViewCell.swift
//  socket_demo
//
//  Created by Krishna Soni on 31/12/19.
//  Copyright © 2019 Krishna Soni. All rights reserved.
//

import UIKit

class MessageSendTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var viewContainer: UIView!{
        didSet {
            viewContainer.layer.cornerRadius = 8.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async{
            self.viewContainer.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
            self.viewContainer.roundCorners(corners: [.topLeft, .bottomLeft, .bottomRight], radius: 8.0)
        }
    }
    
    func configureCell(_ message: Message) {
        
        self.lblMessage.text = message.message ?? ""
        if let createAt = message.createdAt{
            self.lblDate.text = createAt.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outGoingFormat: "h:mm a")
        }
    }
}
