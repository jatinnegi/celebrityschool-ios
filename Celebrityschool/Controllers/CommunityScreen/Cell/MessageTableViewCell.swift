//
//  MessageTableViewCell.swift
//  socket_demo
//
//  Created by Krishna Soni on 31/12/19.
//  Copyright © 2019 Krishna Soni. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var viewContainer: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async{
            self.viewContainer.roundCorners(corners: [.topRight, .bottomLeft, .bottomRight], radius: 8.0)
        }
    }
    
    func configureCell(_ message: Message) {
        self.lblName.text = message.userName ?? ""
        self.lblMessage.text = message.message ?? ""
        if let createAt = message.createdAt{
            self.lblDate.text = createAt.UTCToLocal(incomingFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", outGoingFormat: "h:mm a")
        }
    }
    
}
