//
//  ProfileLogoutCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 28/08/21.
//

import UIKit

class ProfileLogoutCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "ProfileLogoutCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "ProfileLogoutCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var logOutButton: UIButton!{
        didSet{
            logOutButton.layer.cornerRadius = 5
            logOutButton.layer.masksToBounds = true
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async{
            self.logOutButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }
    
    
    @IBAction func logOutButtonAction(_ sender: Any) {
        CustomNavigationController().callLogOutViewController()
    }
}
