//
//  CommunityChatsViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 17/11/21.
//

import UIKit
import SDWebImageWebPCoder
import SocketIO


class CommunityChatsViewController:CustomNavigationController {
    
    let fname = UserDefaults.standard.fetchString(key: "first_name") as? String ?? ""
    let userId = UserDefaults.standard.fetchString(key: "UserId") as? String ?? ""
    // MARK: - Public properties
    var communityModel:CommunityCategoryModel?

    var messageList: [Message] = []
    
    @IBOutlet weak var tblChat: UITableView! {
        didSet {
            tblChat.register(UINib(nibName: "MessageSendTableViewCell", bundle: nil), forCellReuseIdentifier: "MessageSendTableViewCell")
            tblChat.register(UINib(nibName: "MessageTableViewCell", bundle: nil), forCellReuseIdentifier: "MessageTableViewCell")
            tblChat.dataSource = self
            tblChat.delegate = self
            tblChat.backgroundColor = .black
        }
    }
    @IBOutlet weak var txtbgView: UIView! {
        didSet {
            txtbgView.layer.cornerRadius = txtbgView.frame.height/2
            txtbgView.layer.borderWidth = 1.0
        }
        
    }
    
    @IBOutlet weak var txtMessage: UITextView! {
        didSet {
            txtMessage.text = "send message.."
            txtMessage.textColor = UIColor.lightGray
            txtMessage.delegate = self
            txtMessage.addObserver(self, forKeyPath: "contentSize", options: (NSKeyValueObservingOptions.new), context: nil);
           
        }
    }
    @IBOutlet weak var cc_txtViewBottom: NSLayoutConstraint!{
        didSet{
            cc_txtViewBottom.constant = 0
        }
    }
    @IBOutlet weak var cc_tableViewBottom: NSLayoutConstraint!
    @IBOutlet weak var cc_titleTextViewHeight: NSLayoutConstraint!//34
    var param:[String:String] = [:]
    var onceCallKeyboard:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        SocketHelper.shared.establishConnection()
        setupViewBinding()
        NotificationCenter.default.addObserver( self, selector: #selector(keyboardWillShow(notification:)), name:  UIResponder.keyboardWillShowNotification, object: nil )
        NotificationCenter.default.addObserver( self, selector: #selector(keyboardWillHide(notification:)), name:  UIResponder.keyboardWillHideNotification, object: nil )
        if #available(iOS 11.0, *) {
            tblChat.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SocketHelper.shared.leaveChatRoom(nickname: param) {
            print("Leave Room")
            SocketHelper.shared.closeConnection()
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let tv = object as! UITextView;
        var topCorrect = (tv.bounds.size.height - tv.contentSize.height * tv.zoomScale)/2.0;
        topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
        tv.contentOffset.x = 0;
        tv.contentOffset.y = -topCorrect;
        cc_titleTextViewHeight.constant = tv.contentSize.height;
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded();
        });
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.cc_txtViewBottom.constant = keyboardSize.height
                self.tblChat.scrollToBottom()
            })
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.cc_txtViewBottom.constant = 0
            self.tblChat.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.leftBarButtonItems = [backBarButton(),barButton(imageName:UIImage(named: "profile_icon")!)]
        let titleLabel = UILabel(frame: CGRect(x: 32, y: 0, width: view.frame.width - 32, height: view.frame.height))
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "Roboto-Regular", size: 16)!
        titleLabel.text = ""
        if let model = communityModel{
            if let image = model.image{
                if let url = URL(string:image){
                     UIImageView().sd_setImage(with: url, placeholderImage: UIImage(named: "profile_icon"), options: [.progressiveLoad]) { image, error, cacheType, imageURL in
                        if let finalImage = image{
                            self.navigationItem.leftBarButtonItems = [self.backBarButton(),self.barButton(imageName: finalImage)]
                        }
                    }
                }
            }
            
            if let communityType = model.communityType{
                titleLabel.text = communityType
            }
        }
       
        navigationItem.titleView = titleLabel
    }
    
    
    
    func setupViewBinding() {
        guard let model = self.communityModel else{return}
        guard let communityId = model.id else{return}
        param["communityId"] = "\(communityId)"
        param["communityType"] = model.communityType ?? ""
        
        var onceCall = false
        
        SocketHelper.shared.sendSocketStatus = {[weak self] status in
            guard let strongSelf = self else {return}
            if !onceCall{
                if status == .connected{
                    SocketHelper.shared.joinChatRoom(nickname: strongSelf.param) {
                        print("Join Room")
                        SocketHelper.shared.getMessage { [weak self] (message: [Message]) in
                            guard let strongSelf = self else {return}
                            
                            DispatchQueue.main.async {
                                strongSelf.messageList.append(contentsOf: message)
                                strongSelf.tblChat.reloadData {
                                    print("done")
                                    strongSelf.tblChat.scrollToBottom()
                                }
                            }
                        }
                    }
                    onceCall = true
                }
            }
        }
        
    }
    
    override func backBarButton() -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "back_whiteicon"), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.widthAnchor.constraint(equalToConstant: 35).isActive = true
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.addTarget(self, action: #selector(self.onBackButtonPressed(_:)), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    func barButton(imageName: UIImage) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(imageName, for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.widthAnchor.constraint(equalToConstant: 35).isActive = true
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.addTarget(self, action: #selector(self.onBackButtonPressed(_:)), for: .touchUpInside)
        button.layer.cornerRadius = button.frame.height / 2
        button.layer.masksToBounds = true
        return UIBarButtonItem(customView: button)
    }
    
    @objc override func onBackButtonPressed(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSendCLK(_ sender: UIButton) {
        
        guard txtMessage.text.count > 0,
            let message = txtMessage.text else {
            print("Please type your message.")
            return
        }
        
        var param:[String:String] = [:]
        param["userId"] = userId
        param["userName"] = fname
        param["message"] = message

        
        txtMessage.resignFirstResponder()
        SocketHelper.shared.sendMessage(message: param)
        txtMessage.text = nil
        txtMessage.text = "send message.."
        txtMessage.textColor = UIColor.lightGray
    }
    
}

// MARK: - Table view data source
extension CommunityChatsViewController:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message:Message = messageList[indexPath.row]
        
        if message.userName == fname{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageSendTableViewCell") as? MessageSendTableViewCell else {
                return UITableView.emptyCell()
            }
            
            cell.configureCell(message)
            return cell
            
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell") as? MessageTableViewCell else {
            return UITableView.emptyCell()
        }
        
        cell.configureCell(message)
        return cell
   }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.count
    }
}

// MARK:- Action Evetns -
extension CommunityChatsViewController:UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {

        if txtMessage.textColor == UIColor.lightGray {
            txtMessage.text = ""
            txtMessage.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if txtMessage.text == "" {
            txtMessage.text = "send message.."
            txtMessage.textColor = UIColor.lightGray
        }
    }

}


