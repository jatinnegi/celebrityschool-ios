//
//  CommunityViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 19/08/21.
//

import UIKit

class CommunityViewController: CustomNavigationController {

    @IBOutlet weak var cc_headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            mainCollectionView.register(CommunityCollectionViewCell.nib, forCellWithReuseIdentifier: CommunityCollectionViewCell.reuseIdentifier)
        }
    }
    

    private var viewModel: CommunityApiServices!
    var community:[CommunityCategoryModel]?

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CommunityApiServices()
        // Do any additional setup after loading the view.
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        var topSafeArea: CGFloat = 0.0
//
//        if #available(iOS 11.0, *) {
//            topSafeArea = view.safeAreaInsets.top
//        } else {
//            topSafeArea = topLayoutGuide.length
//        }
//        
//        if topSafeArea == 20{
//            cc_headerViewHeight.constant = 90
//        }else{
//            cc_headerViewHeight.constant = 110
//        }
//        
//
//        // safe area values are now available to use
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setupViewModel()
        self.setUpTitleBar()
    }
    
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func setupViewModel() {

        viewModel.CommunityApiCall()
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}

            DispatchQueue.main.async {
                if let chatData = data.allCategory, chatData.count > 0 {
                    strongSelf.community?.removeAll()
                    strongSelf.community = chatData
                    strongSelf.mainCollectionView.reloadData()
                }
            }

        }
        
//        viewModel.noDataRecieved = {[weak self] in
//        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CommunityViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return community?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CommunityCollectionViewCell.reuseIdentifier, for: indexPath) as! CommunityCollectionViewCell
        if let model = community?[indexPath.row]{
            cell.loadCell(model)
        }
        return cell
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let rootViewController = CommunityChatsViewController()
        if let model = community?[indexPath.row]{
            rootViewController.communityModel = model
        }
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    
}

extension CommunityViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat = (UIScreen.main.bounds.size.width)
        let height:CGFloat = 80
        
        return .init(width: width, height: height)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}

