//
//  CPDetailsMainCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import UIKit

class CPDetailsMainCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CPDetailsMainCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CPDetailsMainCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            mainCollectionView.register(CPDetailsTitleCollectionViewCell.nib, forCellWithReuseIdentifier: CPDetailsTitleCollectionViewCell.reuseIdentifier)
        }
    }
    
    @IBOutlet weak var cc_titleLabelHeight:NSLayoutConstraint!{
        didSet{
            cc_titleLabelHeight.constant = 0
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    var competitionData:[String]?
    let layout = UICollectionViewFlowLayout()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell(model:[String], header:String){
        competitionData = model
        titleLabel.text = header
        titleLabel.font = UIFont(name: "Roboto-Medium", size: 16.0)!
        if titleLabel.applyGradientWith(startColor: UIColor(red: 128/255, green: 54/255, blue: 231/255, alpha: 1), endColor: UIColor(red: 255/255, green: 51/255, blue: 101/255, alpha: 1)) {
            print("Gradient applied!")
        }
        else {
            print("Could not apply gradient")
            titleLabel.textColor = .white
        }
        
        if header != ""{
            cc_titleLabelHeight.constant = 21
        }else{
            cc_titleLabelHeight.constant = 0
        }
        
        layout.scrollDirection = .vertical //.horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        mainCollectionView.setCollectionViewLayout(layout, animated: false)
        mainCollectionView.reloadData()
    }

}

extension CPDetailsMainCollectionViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return competitionData?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CPDetailsTitleCollectionViewCell.reuseIdentifier, for: indexPath) as! CPDetailsTitleCollectionViewCell
        if let data = competitionData?[indexPath.row]{
            cell.loadCellSubTitle(data)
        }
        return cell
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    
}

extension CPDetailsMainCollectionViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat = UIScreen.main.bounds.size.width
        var height:CGFloat = 5.0
        let font = UIFont(name: "Roboto-Medium", size: 20.0)!
        
        if let data = competitionData?[indexPath.row]{
            height = heightForLable(text: data, font: font, width:width) + height
        }
         
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 5, bottom: 0, right: 10)
    }
    
    func heightForLable(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        // pass string, font, LableWidth
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
         label.numberOfLines = 0
         label.lineBreakMode = NSLineBreakMode.byWordWrapping
         label.font = font
         label.text = text
         label.sizeToFit()

         return label.frame.height
    }
    
}



