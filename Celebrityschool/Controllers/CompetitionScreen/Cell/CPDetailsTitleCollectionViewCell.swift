//
//  CPDetailsTitleCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import UIKit

class CPDetailsTitleCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CPDetailsTitleCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CPDetailsTitleCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var titleLabel:UILabel!{
        didSet{
            titleLabel.text = ""
        }
    }
    
    @IBOutlet weak var cc_titleLabelLeading:NSLayoutConstraint!{
        didSet{
            cc_titleLabelLeading.constant = 16
        }
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func loadCellTitle(_ title:String){
        titleLabel.text = title
        titleLabel.font = UIFont(name: "Roboto-Bold", size: 16.0)!
        titleLabel.textColor = .white
    }
    
    
    func loadCellSubTitle(_ subTitle:String){
        cc_titleLabelLeading.constant = 20
        titleLabel.text = subTitle
        titleLabel.font = UIFont(name: "Roboto-Regular", size: 14.0)!
        titleLabel.textColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
    }
}
