//
//  CompetitionHeaderCollectionReusableView.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import UIKit

class CompetitionHeaderCollectionReusableView: UICollectionReusableView {

    static let reuseIdentifier = "CompetitionHeaderCollectionReusableView"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CompetitionHeaderCollectionReusableView", bundle: nil)
        }
    }
    
    @IBOutlet weak var bannerimageView: UIImageView!{
        didSet{
            bannerimageView.image = UIImage(named: "CompetitionBannericon")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
