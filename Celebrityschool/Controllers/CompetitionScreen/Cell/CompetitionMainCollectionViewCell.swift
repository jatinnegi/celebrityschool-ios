//
//  CompetitionMainCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import UIKit

class CompetitionMainCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CompetitionMainCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CompetitionMainCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            mainCollectionView.register(CompitionCollectionViewCell.nib, forCellWithReuseIdentifier: CompitionCollectionViewCell.reuseIdentifier)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    var competitionData:[CompetitionDataModel]?
    
    let layout = UICollectionViewFlowLayout()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell(model:[CompetitionDataModel]){
        competitionData = model
        titleLabel.text = "Challenges"
        titleLabel.font = UIFont(name: "Roboto-Medium", size: 18.0)!
        layout.scrollDirection = .vertical //.horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        mainCollectionView.setCollectionViewLayout(layout, animated: false)
        mainCollectionView.reloadData()
    }

}

extension CompetitionMainCollectionViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return competitionData?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CompitionCollectionViewCell.reuseIdentifier, for: indexPath) as! CompitionCollectionViewCell
        if let data = competitionData?[indexPath.row]{
            cell.loadCell(data)
        }
        return cell
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let data = competitionData?[indexPath.row]{
            guard let topController = UIApplication.topViewController()else {return}
            let rootViewController = CompitionDetailsViewController()
            rootViewController.model = data
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
        
    }
    
    
}

extension CompetitionMainCollectionViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width :CGFloat = (UIScreen.main.bounds.size.width) / 2.45
        switch UIDevice().type {
        case .iPad2, .iPad3, .iPad4 , .iPad5 , .iPad6 , .iPad7 , .iPad8, .iPad9, .iPadAir, .iPadAir2, .iPadAir3, .iPadAir4, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadMini6, .iPadPro9_7, .iPadPro10_5, .iPadPro11, .iPadPro2_11, .iPadPro3_11, .iPadPro12_9, .iPadPro2_12_9, .iPadPro3_12_9, .iPadPro4_12_9, .iPadPro5_12_9:
            width = (UIScreen.main.bounds.size.width) / 2.20
            case .unrecognized:
                print("Device model unrecognized");
            default:break
        }
        
      
        let height:CGFloat = (UIScreen.main.bounds.size.width) / 1.55
        
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 10, left: 20, bottom: 0, right: 20)
    }
    
}

