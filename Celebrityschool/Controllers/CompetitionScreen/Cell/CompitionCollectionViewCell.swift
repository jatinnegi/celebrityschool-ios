//
//  CompitionCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import UIKit
import SDWebImageWebPCoder

class CompitionCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CompitionCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CompitionCollectionViewCell", bundle: nil)
        }
    }
    @IBOutlet weak var CPimageView: UIImageView!{
        didSet{
            CPimageView.image = UIImage(named: "")
            self.CPimageView.layer.cornerRadius = 5
            self.CPimageView.layer.masksToBounds = true
        }
    }
    
    
    @IBOutlet weak var bgView: UIView!{
        didSet{
            self.bgView.layer.cornerRadius = 5
            self.bgView.layer.masksToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell(_ model:CompetitionDataModel){
        if let image = model.card{
            if let url = URL(string:image){
                CPimageView.sd_setImage(with: url, placeholderImage: nil, options: [.progressiveLoad])

            }
        }
    }

    
}
