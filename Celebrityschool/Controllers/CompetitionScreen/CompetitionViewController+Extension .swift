//
//  CompetitionViewController+Extension .swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import UIKit

extension CompetitionViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CompetitionMainCollectionViewCell.reuseIdentifier, for: indexPath) as! CompetitionMainCollectionViewCell
        if let leasson = competitionData{
            cell.loadCell(model: leasson)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CompetitionHeaderCollectionReusableView.reuseIdentifier, for: indexPath) as? CompetitionHeaderCollectionReusableView else {
                    fatalError("Invalid view type")
            }
            
//            if let banner = homeData?.banner , let model = homeData?.all_lessons_purchased{
//                headerView.loadHeaderCell(banner, purchasedmodel: model)
//            }
            return headerView
        default:
            fatalError("Unexpected element kind")
        }
    }
    
    
}

extension CompetitionViewController:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        // Use this view to calculate the optimal size based on the collection view's width
        switch UIDevice().type {
        case .iPad2, .iPad3, .iPad4 , .iPad5 , .iPad6 , .iPad7 , .iPad8, .iPad9, .iPadAir, .iPadAir2, .iPadAir3, .iPadAir4, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadMini6, .iPadPro9_7, .iPadPro10_5, .iPadPro11, .iPadPro2_11, .iPadPro3_11, .iPadPro12_9, .iPadPro2_12_9, .iPadPro3_12_9, .iPadPro4_12_9, .iPadPro5_12_9:
            return CGSize(width: collectionView.frame.width, height: 400)
            case .unrecognized:
                print("Device model unrecognized");
            default:break
        }
        return CGSize(width: collectionView.frame.width, height: 230)
                                                 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let Cwidth = collectionView.frame.width
        var height:CGFloat = 0.0
        if let data = competitionData{
            height = CGFloat(data.count) * (Cwidth/2.3)
        }
       
        return .init(width: Cwidth, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 20, right: 0)
    }
}
