//
//  CompetitionViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 19/08/21.
//

import UIKit

class CompetitionViewController: UIViewController {
    
    private var viewModel: CompetitionApiServices!
    var competitionData:[CompetitionDataModel]?
    
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            mainCollectionView.register(CompetitionHeaderCollectionReusableView.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:CompetitionHeaderCollectionReusableView.reuseIdentifier)
            mainCollectionView.register(CompetitionMainCollectionViewCell.nib, forCellWithReuseIdentifier: CompetitionMainCollectionViewCell.reuseIdentifier)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CompetitionApiServices()
        setupViewModel()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("CompitionRefersh"), object: nil)


        // Do any additional setup after loading the view.
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        setupViewModel()
    }

    
    private func setupViewModel() {

        viewModel.CompetitionApiCall(false)
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            
            DispatchQueue.main.async {
                strongSelf.competitionData = data
                strongSelf.mainCollectionView.reloadData()
            }
            
        }
        
//        viewModel.noDataRecieved = {[weak self] in
//        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
