//
//  CompitionDetailsViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import UIKit

class CompitionDetailsViewController: UIViewController {
    
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            mainCollectionView.register(CompetitionHeaderCollectionReusableView.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:CompetitionHeaderCollectionReusableView.reuseIdentifier)
            mainCollectionView.register(CPDetailsTitleCollectionViewCell.nib, forCellWithReuseIdentifier: CPDetailsTitleCollectionViewCell.reuseIdentifier)
            mainCollectionView.register(CPDetailsMainCollectionViewCell.nib, forCellWithReuseIdentifier: CPDetailsMainCollectionViewCell.reuseIdentifier)

        }
    }
    @IBOutlet weak var headerView: UIView!{
        didSet{
            headerView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var participateButton: UIButton!{
        didSet{
            participateButton.isHidden = true
        }
    }
    @IBOutlet weak var cc_CollcetionButtonHeight: NSLayoutConstraint!{
        didSet{
            cc_CollcetionButtonHeight.constant = 0
        }
    }
    @IBOutlet weak var cc_CollcetionButtonBottom: NSLayoutConstraint!{//15
        didSet{
            cc_CollcetionButtonBottom.constant = 0
        }
    }
    @IBOutlet weak var cc_CollcetionViewTop: NSLayoutConstraint!
    var model:CompetitionDataModel?
    var descriptionArray:[String]?
    var prizesArray:[String]?
    var rulesArray:[String]?
    var how_to_participateArray:[String]?
    var headerTitle:String = ""
    
    private var viewModel: CompetitionApiServices!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CompetitionApiServices()
        setupViewModel()
        setupView()
        observerCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var topSafeArea: CGFloat = 0.0
//        var bottomSafeArea: CGFloat = 0.0

        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
//            bottomSafeArea = view.safeAreaInsets.bottom
        } else {
            topSafeArea = topLayoutGuide.length
//            bottomSafeArea = bottomLayoutGuide.length
        }
        
        cc_CollcetionViewTop.constant = -topSafeArea
//        cc_CollcetionViewBottom.constant = +bottomSafeArea

        // safe area values are now available to use
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func observerCall(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("CorsePurchasedSuccesFully"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        guard var data = model else{return}
        data.is_paid = true
        self.model = data
        self.setupView()
        NotificationCenter.default.post(name: Notification.Name("CompitionRefersh"), object: nil)
    }

    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func participateButtonAction(_ sender: Any) {
        if participateButton.titleLabel?.text == "PARTICIPATE"{
            guard let data = model else{return}
            guard let plan_id = data.competition_id else{return}
            let rootViewController = CompitionStudentViewController()
            rootViewController.albumId = plan_id
            rootViewController.delegate = self
            rootViewController.modalPresentationStyle = .custom
            present(rootViewController, animated: true, completion: nil)
            
        }else{
            DispatchQueue.main.async{[weak self] in
                guard let strongSelf = self else {return}
                strongSelf.imagePick()
            }
        }
    }
    
    private func setupView() {
        
        guard let data = model else{return}
        
        if let is_participate = data.is_participated, is_participate == false{
            participateButton.isHidden = false
            participateButton.setTitle("PARTICIPATE", for: .normal)
            
            if let is_paid = data.is_paid, is_paid == true{
                participateButton.setTitle("UPLOAD", for: .normal)
            }
            cc_CollcetionButtonHeight.constant = 60
            cc_CollcetionButtonBottom.constant = 15
        }else{
            participateButton.isHidden = true
            cc_CollcetionButtonHeight.constant = 0
            cc_CollcetionButtonBottom.constant = 5
        }
        

        
        if let title = data.title, title != ""{
            headerTitle = title
        }
       
        if let descr = data.description, descr != ""{
            let descArr = descr.components(separatedBy: "\r\n")
            descriptionArray = descArr
        }
        
        if let prizes = data.prizes, prizes != ""{
            let prizeArr = prizes.components(separatedBy: "\r\n")
            prizesArray = prizeArr
        }
        
        if let rules = data.rules, rules != ""{
            let ruleArr = rules.components(separatedBy: "\r\n")
            rulesArray = ruleArr
        }
        
        if let how_to_participate = data.how_to_participate, how_to_participate != ""{
            let how_to_participateArr = how_to_participate.components(separatedBy: "\r\n")
            how_to_participateArray = how_to_participateArr
        }
        
        mainCollectionView.reloadData()
    }
    
    private func setupViewModel() {
        
        guard var data = model else{return}
        viewModel.didReceiveDataImageVideo = {[weak self] uploadStatus in
            guard let strongSelf = self else {return}
            data.is_participated = uploadStatus
            strongSelf.model = data
            strongSelf.setupView()
            
            if uploadStatus == true{
                
                let payloadDictionary: [String : Any] = [
                    "competation_name" : data.title ?? "" ,
                    "course_category" : data.category ?? "",
                    "amount" : Int(data.amount_ios ?? 0)
                ]
                
                AnalyticsManager.sharedInstance.SmartTechEvent(payloadDictionary, eventName: "upload_competition")
                
                NotificationCenter.default.post(name: Notification.Name("CompitionRefersh"), object: nil)

            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CompitionDetailsViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CPDetailsTitleCollectionViewCell.reuseIdentifier, for: indexPath) as! CPDetailsTitleCollectionViewCell
            cell.loadCellTitle(headerTitle)
            return cell
        } else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CPDetailsMainCollectionViewCell.reuseIdentifier, for: indexPath) as! CPDetailsMainCollectionViewCell
            if indexPath.row == 2 {
                cell.loadCell(model: rulesArray ?? [], header: "Notes")
            }else if indexPath.row == 3{
                cell.loadCell(model: prizesArray ?? [], header: "Price Details")
            }else if indexPath.row == 4{
                cell.loadCell(model: how_to_participateArray ?? [], header: "How to Participate")
            }else{
                cell.loadCell(model: descriptionArray ?? [], header: "Description")
            }
            
            return cell
        }
        else{
            return UICollectionViewCell()
        }

    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: CompetitionHeaderCollectionReusableView.reuseIdentifier, for: indexPath) as? CompetitionHeaderCollectionReusableView else {
                    fatalError("Invalid view type")
            }
            
            if let image = model?.banner{
                if let url = URL(string:image){
                    headerView.bannerimageView.sd_setImage(with: url, placeholderImage: nil, options: [.progressiveLoad])
                    headerView.bannerimageView.contentMode = .scaleAspectFill

                }
            }
            return headerView
        default:
            fatalError("Unexpected element kind")
        }
    }
    
    
}

extension CompitionDetailsViewController:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        // Use this view to calculate the optimal size based on the collection view's width
        return CGSize(width: collectionView.frame.width, height: 250)
                                                 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let Cwidth = collectionView.frame.width
        var height:CGFloat = 0.0
        if indexPath.row == 0{
            if headerTitle != ""{
                height = 50
            }
        }else if indexPath.row == 1{
            if let data = descriptionArray{
                height = dataItrate(data)
            }
        }else if indexPath.row == 2{
            if let data = rulesArray{
                height = dataItrate(data)
            }
        }else if indexPath.row == 3{
            if let data = prizesArray{
                height = dataItrate(data)
            }
        }else if indexPath.row == 4{
            if let data = how_to_participateArray{
                height = dataItrate(data)
            }
        }

       
        return .init(width: Cwidth, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 20, right: 0)
    }
    
    func dataItrate(_ data:[String]) -> CGFloat{
        var height:CGFloat = 40.0
        let font = UIFont(name: "Roboto-Medium", size: 20.0)!
        let Cwidth = mainCollectionView.frame.width
        for name in data {
            print("Hello, \(name)!")
            height = heightForLable(text: name, font: font, width:Cwidth) + height
        }
       
        return height
    }
    
    func heightForLable(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        // pass string, font, LableWidth
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
         label.numberOfLines = 0
         label.lineBreakMode = NSLineBreakMode.byWordWrapping
         label.font = font
         label.text = text
         label.sizeToFit()

         return label.frame.height
    }
    
}

extension CompitionDetailsViewController:UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    
    func imagePick(){
        guard let topController = UIApplication.topViewController()else {return}
        guard let data = model else{return}
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .savedPhotosAlbum
        if let upload_Type = data.upload_type,upload_Type == "video"{
            imagePickerController.mediaTypes = ["public.movie"]
        }else{
            imagePickerController.mediaTypes = ["public.image"]

        }
       
        imagePickerController.allowsEditing = true
        topController.present(imagePickerController, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var contenData:Data?
        var contentype:String = "image/jpg"
        if let image = info[.originalImage] as? UIImage{
            print(image)
            if let imageData = image.jpegData(compressionQuality: 1){
                contenData = imageData
            }

        }else if let fileURL = info[.mediaURL] as? URL{
            print(fileURL)
            if let videoData = NSData(contentsOf: fileURL) {
                print(videoData.length)
                contenData = videoData as Data
            }
            contentype = "video/mp4"
        }
        guard let datamodel = model else{return}
        picker.dismiss(animated: true) {[weak self] in
            guard let strongSelf = self else {return}
            if let data = contenData{
                strongSelf.viewModel.imageViewUpload(data, category: datamodel.category ?? "", type:contentype)

            }
        }
       
    }
}

extension CompitionDetailsViewController:CompitionStudentViewDelegate{
    func studentDataUpload() {
        guard let data = model else{return}
        guard let plan_id = data.competition_id else{return}
        guard let a_price = data.amount_ios else{return}
        guard let product_id = data.product_id else{return}

        CourseApiServices().productBuyCall("\(plan_id)", a_price: "\(a_price)", product_id: product_id)

        let payloadDictionary: [String : Any] = [
            "competation_name" : data.title ?? "" ,
            "course_category" : data.category ?? "",
            "amount" : Int(a_price)
        ]

        AnalyticsManager.sharedInstance.SmartTechEvent(payloadDictionary, eventName: "participate_competition")
    }
}

