//
//  CompitionStudentViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 21/01/22.
//

import UIKit
protocol CompitionStudentViewDelegate: AnyObject {
    func studentDataUpload()
}

class CompitionStudentViewController: CustomNavigationController {
    @IBOutlet var scrollViewOutlet: UIScrollView?
    var exscrollView:UIScrollView?
    
    let attributes = [
        NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 12)! // Note the !
    ]
    @IBOutlet weak var schoolNameTxtFiled: UITextField!{
        didSet{
            schoolNameTxtFiled.delegate = self
            schoolNameTxtFiled.returnKeyType = .next
            schoolNameTxtFiled.attributedPlaceholder = NSAttributedString(string: "Enter School Name",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var locationTxtFiled: UITextField!{
        didSet{
            locationTxtFiled.delegate = self
            locationTxtFiled.returnKeyType = .next
            locationTxtFiled.attributedPlaceholder = NSAttributedString(string: "Enter location",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var studentNameTxtFiled: UITextField!{
        didSet{
            studentNameTxtFiled.delegate = self
            studentNameTxtFiled.returnKeyType = .next
            studentNameTxtFiled.attributedPlaceholder = NSAttributedString(string: "Enter Student Name",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var standardTxtFiled: UITextField!{
        didSet{
            standardTxtFiled.delegate = self
            standardTxtFiled.returnKeyType = .next
            standardTxtFiled.attributedPlaceholder = NSAttributedString(string: "Enter Standard",
                                                                   attributes: attributes)
        }
    }
    
    @IBOutlet var allBgTxtFiledView: [UIView]!{
        didSet{
            allBgTxtFiledView.forEach { $0.layer.cornerRadius = 5 }
            allBgTxtFiledView.forEach { $0.layer.masksToBounds = true }
            allBgTxtFiledView.forEach { $0.backgroundColor = .white }
        }
    }
    
    @IBOutlet weak var submitButton: UIButton!{
        didSet{
            submitButton.layer.cornerRadius = 5
            submitButton.layer.masksToBounds = true
        }
    }
    
    private var viewModel: CompetitionApiServices!
    var albumId:Int = 0
    weak var delegate: CompitionStudentViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CompetitionApiServices()
        setupViewModel()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async{
            self.submitButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.exscrollView?.isScrollEnabled = true
        self.registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    deinit {
        self.unregisterKeyboardNotifications()
    }

    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.rightBarButtonItem = rightBarButton()
        self.navigationItem.title = "Student Details"
       
    }
    
    
    private func setupViewModel() {

        
        viewModel.didReceiveStudentData = {[weak self] status in
            guard let strongSelf = self else {return}
            
            if status{
                if let delegate = strongSelf.delegate{
                    delegate.studentDataUpload()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    strongSelf.dismiss(animated: true)
                }
            }

            
        }

        
    }

    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitButtonAction(_ sender: Any) {
        if validateValue(){
            let payloadDictionary: [String : Any] = [
                "schoolName" : schoolNameTxtFiled.text ?? "" ,
                "location" : locationTxtFiled.text ?? "",
                "studentName" : studentNameTxtFiled.text ?? "",
                "standard" : standardTxtFiled.text ?? "",
                "albumId":"\(albumId)"
            ]
            
            viewModel.StudentApiCall(payloadDictionary)
        }
    }
    
    
    private func validateValue() -> Bool{
        
        if schoolNameTxtFiled.text == ""{
            AlertIndicator_Toast().showToast("Please enter School Name", vc: self)
            return false
        }
        
        if locationTxtFiled.text == ""{
            AlertIndicator_Toast().showToast("Please enter Location", vc: self)
            return false
        }
        
        if  studentNameTxtFiled.text == ""{
            AlertIndicator_Toast().showToast("Please enter Student Name", vc: self)
            return false
        }
        
        if standardTxtFiled.text == ""{
            AlertIndicator_Toast().showToast("Please enter Standard", vc: self)
            return false
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:UITextFiled Delegate
extension CompitionStudentViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if textField == self.schoolNameTxtFiled{
            self.locationTxtFiled.becomeFirstResponder()
        }else if textField == self.locationTxtFiled{
            self.studentNameTxtFiled.becomeFirstResponder()
        }else if textField == self.studentNameTxtFiled{
            self.standardTxtFiled.becomeFirstResponder()
        }else if textField == self.standardTxtFiled{
            // Call login api
            textField.resignFirstResponder()
            submitButtonAction(self)
        }
       return true
    }
    
    
}

//MARK:KeyBoard handling
extension CompitionStudentViewController:ScrollViewKeyboardDelegate {
    var scrollView: UIScrollView? {
        get {
            return scrollViewOutlet
        }
        set {
            exscrollView = newValue
        }
    }
    
}
