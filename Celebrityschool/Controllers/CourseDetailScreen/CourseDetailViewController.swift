//
//  CourseDetailViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 11/08/21.
//

import UIKit

class CourseDetailViewController: UIViewController {

    @IBOutlet weak var courseDetailCollcetionView: UICollectionView!{
        didSet{
            courseDetailCollcetionView.delegate = self
            courseDetailCollcetionView.dataSource = self
            courseDetailCollcetionView.register(HeaderCollectionReusableView.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:HeaderCollectionReusableView.reuseIdentifier)
            courseDetailCollcetionView.register(CorseDetailMainCollectionViewCell.nib, forCellWithReuseIdentifier: CorseDetailMainCollectionViewCell.reuseIdentifier)
            courseDetailCollcetionView.register(CourseHeaderCollectionViewCell.nib, forCellWithReuseIdentifier: CourseHeaderCollectionViewCell.reuseIdentifier)
            courseDetailCollcetionView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    @IBOutlet weak var headerView: UIView!{
        didSet{
            headerView.backgroundColor = .clear
        }
    }
    var albumInfo:CourseModel?
    private var viewModel: CourseApiServices!
    var albumData:AlbumDataModel?
    var imageHeight = UIScreen.main.bounds.height / 2
    var height:CGFloat = 297
    var leassonCount = 0
    var leassonHeight:CGFloat = 100.0
    var tempVideoLeasson:[VideoLeassonModel] = []
    
    @IBOutlet weak var cc_CollcetionViewBottom: NSLayoutConstraint!
    @IBOutlet weak var cc_CollcetionViewTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CourseApiServices()
        setupViewModel()
        observerCall()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var topSafeArea: CGFloat = 0.0
        var bottomSafeArea: CGFloat = 0.0

        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
            bottomSafeArea = view.safeAreaInsets.bottom
        } else {
            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        
        cc_CollcetionViewTop.constant = -topSafeArea
        cc_CollcetionViewBottom.constant = +bottomSafeArea

        // safe area values are now available to use
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self, name: Notification.Name("CorsePurchasedSuccesFully"), object: nil)
    }
    
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func observerCall(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("CorsePurchasedSuccesFully"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        imageHeight = UIScreen.main.bounds.height / 2
        height = 297
        leassonCount = 0
        leassonHeight = 100.0
        setupViewModel()
    }

    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    private func setupViewModel() {
       
        if let albumData = albumInfo{
            viewModel.CourseApiCall(false, albumData.id ?? 0)
        }
       
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            
            
            if let album_info = data.album_info{
                if let description = album_info.a_desc{
                    strongSelf.height = strongSelf.heightForView(text: description, font: UIFont(name: "Roboto-Regular", size: 12)!, width: UIScreen.main.bounds.width) + strongSelf.height
                }
                
                if let video_leasson = data.video_lessons{
                    strongSelf.leassonCount = video_leasson.count
                    strongSelf.leassonHeight = CGFloat(strongSelf.leassonCount) * CGFloat(UIScreen.main.bounds.width / 2.95)
                }
            }
            
            DispatchQueue.main.async() {
                strongSelf.albumData = data
                strongSelf.view.layoutIfNeeded()
                strongSelf.courseDetailCollcetionView.layoutIfNeeded()
                strongSelf.courseDetailCollcetionView.reloadData()
            }
            
        }
        
//        viewModel.noDataRecieved = {[weak self] in
//        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
