//
//  ForgotPasswardViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 05/08/21.
//

import UIKit

class ForgotPasswardViewController: CustomNavigationController {
    
    let attributes = [
        NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 12)! // Note the !
    ]
    @IBOutlet weak var emailI_iconImageView: UIImageView!{
        didSet{
            emailI_iconImageView.image = UIImage(named: "email_iconDeselected")//emailPhone_iconSelected
        }
    }
    
    @IBOutlet weak var loginButton: UIButton!{
        didSet{
            loginButton.layer.cornerRadius = 5
            loginButton.layer.masksToBounds = true
        }
    }
    
    @IBOutlet var emailView: UIView!{
        didSet{
            emailView.layer.cornerRadius = 5
            emailView.layer.masksToBounds = true
            emailView.backgroundColor = .white
        }
    }
    
    @IBOutlet var loginView: [UIView]!
    
    @IBOutlet weak var emailTxtFiled: UITextField!{
        didSet{
            emailTxtFiled.delegate = self
            emailTxtFiled.returnKeyType = .done
            emailTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Email Address",
                                         attributes: attributes)
        }
    }
    
    @IBOutlet weak var OTPTxtFiled: UITextField!{
        didSet{
            OTPTxtFiled.delegate = self
            OTPTxtFiled.returnKeyType = .next
            OTPTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type OTP",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var passwardTxtFiled: UITextField!{
        didSet{
            passwardTxtFiled.delegate = self
            passwardTxtFiled.returnKeyType = .next
            passwardTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Password",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var confirmPasswardTxtFiled: UITextField!{
        didSet{
            confirmPasswardTxtFiled.delegate = self
            confirmPasswardTxtFiled.returnKeyType = .done
            confirmPasswardTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Confirm Password",
                                                                   attributes: attributes)
        }
    }
    
    @IBOutlet weak var OTP_iconImageView: UIImageView!{
        didSet{
            OTP_iconImageView.image = UIImage(named: "phone_iconDeselected")//phone_iconSelected
        }
    }
    @IBOutlet weak var pwd_iconImageView: UIImageView!{
        didSet{
            pwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")//pwd_iconSelected
        }
    }
    @IBOutlet weak var Cpwd_iconImageView: UIImageView!{
        didSet{
            Cpwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")//pwd_iconSelected
        }
    }
    
    @IBOutlet var OTPStackViews: [UIStackView]!{
        didSet{
            OTPStackViews.forEach { $0.isHidden = true }
        }
    }
    
    var verifyOTP:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loginButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    
    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.leftBarButtonItem = backBarButton()
        self.navigationItem.title = ""
    }
    
    private func validateValue() -> Bool{

        if !verifyOTP{
            if emailTxtFiled.text == ""{
                AlertIndicator_Toast().showToast(Constants.KBlankFiled, vc: self)
                return false
            }
            
            if !(emailTxtFiled.text?.isEmailCheck ?? false){
                AlertIndicator_Toast().showToast(Constants.KEmailAddressIsNotValid, vc: self)
                return false
            }
        }else{
            if emailTxtFiled.text == "" || OTPTxtFiled.text == "" || passwardTxtFiled.text == "" || confirmPasswardTxtFiled.text == ""{
                AlertIndicator_Toast().showToast(Constants.KBlankFiled, vc: self)
                return false
            }
            
            if !(emailTxtFiled.text?.isEmailCheck ?? false){
                AlertIndicator_Toast().showToast(Constants.KEmailAddressIsNotValid, vc: self)
                return false
            }
            
//            if !(OTPTxtFiled.text?.count < 6){
//                AlertIndicator_Toast().showToast(Constants.KOTPNotValid, vc: self)
//                return false
//            }
            
            if  passwardTxtFiled.text != confirmPasswardTxtFiled.text{
                AlertIndicator_Toast().showToast(Constants.KPasswardNotMatch, vc: self)
                return false
            }
            
            if !(confirmPasswardTxtFiled.text?.isValidPassword ?? false){
                AlertIndicator_Toast().showToast(Constants.KPasswardNotValid, vc: self)
                return false
            }
        }
        return true
    }

    func forPwdCall(){
        let param : Dictionary<String, Any> = [
            "email":emailTxtFiled.text ?? "",
        ]
        
        print("Forgot Passward param==\(param)")
        DispatchQueue.global(qos: .background).async {
            LoginApiServices().forgotPwdAPICall(param) { status, response in
                if status == true{
                    DispatchQueue.main.async{[weak self] in
                        self?.setUI()
                    }
                }
            }
        }
    }
    
    func setUI(){
        self.verifyOTP = true
        self.emailTxtFiled.returnKeyType = .next
        self.OTPStackViews.forEach { $0.isHidden = false }
        loginView.forEach { $0.layer.cornerRadius = 5 }
        loginView.forEach { $0.layer.masksToBounds = true }
        loginView.forEach { $0.backgroundColor = .white }
    }
    
    func verifyOTPCall(){
        let param : Dictionary<String, Any> = [
            "email":emailTxtFiled.text ?? "",
            "otp":OTPTxtFiled.text ?? "",
            "password":passwardTxtFiled.text ?? "",
            "confirmpassword":confirmPasswardTxtFiled.text ?? ""
        ]

        print("Verify Passward param==\(param)")
        DispatchQueue.global(qos: .background).async {
            LoginApiServices().verifyAPICall(param) { status, response in
                if status == true{
                    DispatchQueue.main.async{[weak self] in
                        self?.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:UIButton Action
extension ForgotPasswardViewController{
    @IBAction func fPwdButtonAction(_ sender: Any) {
        if validateValue(){
            if !Reachability.isConnectedToNetwork() {
                let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
                alert.show()
                return
            }
            
            if !verifyOTP{
                self.forPwdCall()
            }else{
                self.verifyOTPCall()
            }

        }
    }
    
    
    @IBAction func cShowHidePwdButtonAction(_ sender: UIButton) {
        if !sender.isSelected{
            sender.isSelected = true
            confirmPasswardTxtFiled.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            confirmPasswardTxtFiled.isSecureTextEntry = true
        }
    }
    @IBAction func showhidePwdButtonAction(_ sender: UIButton) {
        if !sender.isSelected{
            sender.isSelected = true
            passwardTxtFiled.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            passwardTxtFiled.isSecureTextEntry = true
        }
    }
}

extension ForgotPasswardViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        fPwdButtonAction(self)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.emailTxtFiled {
            emailI_iconImageView.image = UIImage(named: "email_iconSelected")
        }else if textField == self.OTPTxtFiled{
            OTP_iconImageView.image = UIImage(named: "phone_iconSelected")
        }else if textField == self.passwardTxtFiled {
            pwd_iconImageView.image = UIImage(named: "pwd_iconSelected")
        }else if textField == self.confirmPasswardTxtFiled{
            Cpwd_iconImageView.image = UIImage(named: "pwd_iconSelected")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.emailTxtFiled {
            emailI_iconImageView.image = UIImage(named: "email_iconDeselected")
        }else if textField == self.OTPTxtFiled{
            OTP_iconImageView.image = UIImage(named: "phone_iconDeselected")
        }else if textField == self.passwardTxtFiled {
            pwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")
        }else if textField == self.confirmPasswardTxtFiled{
            // Call login api
            Cpwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")
        }
    }
    
}

