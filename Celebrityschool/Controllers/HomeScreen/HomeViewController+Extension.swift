//
//  HomeViewController+Extension.swift
//  Celebrityschool
//
//  Created by Hiren on 14/08/21.
//

import UIKit

extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let leasson = homeData?.lessons{
            return leasson.count
        }else{
            return 0
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainCollectionViewCell.reuseIdentifier, for: indexPath) as! MainCollectionViewCell
        if let leasson = homeData?.lessons{
            cell.loadCell(leasson[indexPath.row])
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HeaderCollectionReusableView.reuseIdentifier, for: indexPath) as? HeaderCollectionReusableView else {
                    fatalError("Invalid view type")
            }
            
            if let banner = homeData?.banner , let model = homeData?.all_lessons_purchased{
                headerView.loadHeaderCell(banner, purchasedmodel: model)
            }
            return headerView
        default:
            fatalError("Unexpected element kind")
        }
    }
    
    
}

extension HomeViewController:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        // Use this view to calculate the optimal size based on the collection view's width
        let height = collectionView.frame.height / 1.3
        return CGSize(width: collectionView.frame.width, height: height)
                                                 
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let Cwidth = collectionView.frame.width
        var height:CGFloat = 0.0
        if let leasson = homeData?.lessons{
            let title = leasson[indexPath.row].title
            switch title {
            case "Courses":
                height = Cwidth / 1.4
            case "Trending Courses":
                height = Cwidth / 1.6
            default:
                height = 0.0
            }
        }
       
        return .init(width: Cwidth, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 20, right: 0)
    }
}
