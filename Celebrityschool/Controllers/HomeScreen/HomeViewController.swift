//
//  HomeViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 02/08/21.
//

import UIKit

class HomeViewController: UIViewController {

    private var viewModel: HomeApiServices!
    var homeData:HomeDataModel?

    @IBOutlet weak var homeCollectionView: UICollectionView!{
        didSet{
            homeCollectionView.delegate = self
            homeCollectionView.dataSource = self
            homeCollectionView.register(HeaderCollectionReusableView.nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:HeaderCollectionReusableView.reuseIdentifier)
            homeCollectionView.register(MainCollectionViewCell.nib, forCellWithReuseIdentifier: MainCollectionViewCell.reuseIdentifier)
        }
    }

    @IBOutlet weak var cc_CollcetionViewTop: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = HomeApiServices()
        setupViewModel()
        observerCall()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var topSafeArea: CGFloat = 0.0

        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
        } else {
            topSafeArea = topLayoutGuide.length
        }
        
        if topSafeArea == 20{
            topSafeArea = topSafeArea + 20
        }else{
            topSafeArea = topSafeArea + 44
        }
        
        cc_CollcetionViewTop.constant = -topSafeArea

        // safe area values are now available to use
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func observerCall(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.allCorsePurchasedNotification(notification:)), name: Notification.Name("AllCorsePurchasedSuccesFully"), object: nil)
    }
    
    @objc func allCorsePurchasedNotification(notification: Notification) {
        setupViewModel()
    }
    
    
    private func setupViewModel() {

        viewModel.HomeApiCall(false)
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            
            DispatchQueue.main.async {
                strongSelf.homeData = data
                strongSelf.homeCollectionView.reloadData()
            }
            
        }
        
//        viewModel.noDataRecieved = {[weak self] in
//        }
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
