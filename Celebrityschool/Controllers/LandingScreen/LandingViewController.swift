//
//  LandingViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 05/08/21.
//

import UIKit

class LandingViewController: UIViewController {
    
    @IBOutlet weak var login_signupV: Login_SignupView!{
        didSet{
            login_signupV.type = .login
            login_signupV.delegate = self
        }
    }
    
    @IBOutlet weak var emailButton: UIButton!{
        didSet{
            emailButton.layer.cornerRadius = 5
            emailButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var cc_loginViewHeight: NSLayoutConstraint! // 200

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        
//        switch UIDevice().type {
//        case .iPad2, .iPad3, .iPad4 , .iPad5 , .iPad6 , .iPad7 , .iPad8, .iPad9, .iPadAir, .iPadAir2, .iPadAir3, .iPadAir4, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadMini6, .iPadPro9_7, .iPadPro10_5, .iPadPro11, .iPadPro2_11, .iPadPro3_11, .iPadPro12_9, .iPadPro2_12_9, .iPadPro3_12_9, .iPadPro4_12_9, .iPadPro5_12_9:
//            cc_loginViewHeight.constant = 400
//            case .unrecognized:
//                print("Device model unrecognized");
//            default:break
//        }
    }
    
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func loginWithemailBtnAction(_ sender: Any) {
        CustomNavigationController().callLoginViewController()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
