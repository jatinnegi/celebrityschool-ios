//
//  LoginViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.
//

import UIKit

class LoginViewController: CustomNavigationController {
    
    let attributes = [
        NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 12)! // Note the !
    ]
    
    @IBOutlet weak var login_signupV: Login_SignupView!{
        didSet{
            login_signupV.type = .login
            login_signupV.delegate = self
        }
    }
    
    @IBOutlet weak var loginButton: UIButton!{
        didSet{
            loginButton.layer.cornerRadius = 5
            loginButton.layer.masksToBounds = true
        }
    }
    
    @IBOutlet var loginView: [UIView]!{
        didSet{
            loginView.forEach { $0.layer.cornerRadius = 5 }
            loginView.forEach { $0.layer.masksToBounds = true }
            loginView.forEach { $0.backgroundColor = .white }
        }
    }
    
    @IBOutlet weak var emailTxtFiled: UITextField!{
        didSet{
            emailTxtFiled.delegate = self
            emailTxtFiled.returnKeyType = .next
            emailTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Email Address",
                                         attributes: attributes)
        }
    }
    
    @IBOutlet weak var pwdTxtFiled: UITextField!{
        didSet{
            pwdTxtFiled.delegate = self
            pwdTxtFiled.returnKeyType = .done
            pwdTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Password",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var email_iconImageView: UIImageView!{
        didSet{
            email_iconImageView.image = UIImage(named: "email_iconDeselected")//email_iconSelected
        }
    }
    
    @IBOutlet weak var pwd_iconImageView: UIImageView!
    {
        didSet{
            pwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")//pwd_iconSelected
        }
    }
    
    @IBOutlet weak var cc_loginViewHeight: NSLayoutConstraint! // 200
    @IBOutlet weak var tPLabel: UILabel!
    @IBOutlet weak var PLabel: UILabel!
    @IBOutlet weak var agreeBtn: UIButton!{
        didSet{
            agreeBtn.layer.cornerRadius = 2.0
            agreeBtn.layer.masksToBounds = true
            agreeBtn.isSelected = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.SetTermsLabel()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        loginButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
//        switch UIDevice().type {
//        case .iPad2, .iPad3, .iPad4 , .iPad5 , .iPad6 , .iPad7 , .iPad8, .iPad9, .iPadAir, .iPadAir2, .iPadAir3, .iPadAir4, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadMini6, .iPadPro9_7, .iPadPro10_5, .iPadPro11, .iPadPro2_11, .iPadPro3_11, .iPadPro12_9, .iPadPro2_12_9, .iPadPro3_12_9, .iPadPro4_12_9, .iPadPro5_12_9:
//            case .unrecognized:
//                print("Device model unrecognized");
//            default:break
//        }
    }


    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func SetTermsLabel(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        tPLabel.isUserInteractionEnabled = true
        tPLabel.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(tapFunction2))
        PLabel.isUserInteractionEnabled = true
        PLabel.addGestureRecognizer(tap2)
    }
    
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        CustomNavigationController().callEbookViewController(nil, privateUrl: Constants.termsCondition)
    }
    
    @objc func tapFunction2(sender:UITapGestureRecognizer) {
        print("tap working")
        CustomNavigationController().callEbookViewController(nil, privateUrl: Constants.privacypolicy)
    }
    
    private func validateValue() -> Bool{

        if emailTxtFiled.text == "" || pwdTxtFiled.text == ""{
            AlertIndicator_Toast().showToast(Constants.KBlankFiled, vc: self)
            return false
        }
        
        if !(emailTxtFiled.text?.isEmailCheck ?? false){
            AlertIndicator_Toast().showToast(Constants.KEmailAddressIsNotValid, vc: self)
            return false
        }
        
        if !(pwdTxtFiled.text?.isValidPassword ?? false){
            AlertIndicator_Toast().showToast(Constants.KPasswardNotValid, vc: self)
            return false
        }
        
        if !agreeBtn.isSelected{
            AlertIndicator_Toast().showToast(Constants.KAgree, vc: self)
            return false
        }
        
        return true
    }
    
    // MARK: - UIButton Action
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func agreeButtonAction(_ sender: UIButton) {
        if !agreeBtn.isSelected{
            agreeBtn.isSelected = true
        }else{
            agreeBtn.isSelected = false
        }
    }
    @IBAction func loginButtonAction(_ sender: Any) {
        if validateValue(){
            SocialUtiles.sharedInstance.delegateVC = self
            SocialUtiles.sharedInstance.manuallogin(email: emailTxtFiled.text ?? "", passward: pwdTxtFiled.text ?? "")
        }
    }
    
    @IBAction func forgotButtonAction(_ sender: Any) {
        CustomNavigationController().callForgotPasswardViewController()
    }
    
    @IBAction func showHidePwdButtonAction(_ sender: UIButton) {
        if !sender.isSelected{
            sender.isSelected = true
            pwdTxtFiled.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            pwdTxtFiled.isSecureTextEntry = true
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if textField == self.emailTxtFiled {
            self.pwdTxtFiled.becomeFirstResponder()
        }else if textField == self.pwdTxtFiled{
            // Call login api
            textField.resignFirstResponder()
            loginButtonAction(self)
        }
       return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.emailTxtFiled {
            email_iconImageView.image = UIImage(named: "email_iconSelected")
        }else if textField == self.pwdTxtFiled{
            pwd_iconImageView.image = UIImage(named: "pwd_iconSelected")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.emailTxtFiled {
            email_iconImageView.image = UIImage(named: "email_iconDeselected")
        }else if textField == self.pwdTxtFiled{
            pwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")
        }
    }
}
