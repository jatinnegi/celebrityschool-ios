//
//  Login_SignupView.swift
//  Celebrityschool
//
//  Created by Hiren on 31/07/21.
//

import UIKit
enum loginType {
    case login
    case signup
    case forgotPwd
}

class Login_SignupView: UIView {
    var containerView: UIView!

    @IBOutlet var loginViews: [UIView]!{
        didSet{
            loginViews.forEach { $0.layer.cornerRadius = 5 }
            loginViews.forEach { $0.layer.masksToBounds = true }
        }
    }
   
    @IBOutlet weak var appleView: UIView!
    @IBOutlet weak var cc_appleViewHeight: NSLayoutConstraint!
    var type : loginType = .login
    var delegate:UIViewController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    override func draw(_ rect: CGRect) {
        if #available(iOS 13.0, *) {
            // Show Apple Button
            appleView.isHidden = false
            cc_appleViewHeight.constant = 40
        }else{
            // Hide Apple Button
            appleView.isHidden = true
            cc_appleViewHeight.constant = 0
        }
       
    }
    
    //MARK:- UIButton Action
    @IBAction func facebookButtonAction(_ sender: Any) {
        SocialUtiles.sharedInstance.delegateVC = delegate
        SocialUtiles.sharedInstance.facebookSign()
    }
    @IBAction func googleButtonAction(_ sender: Any) {
        SocialUtiles.sharedInstance.delegateVC = delegate
        SocialUtiles.sharedInstance.googleSign()
    }
    @IBAction func appleButtonAction(_ sender: Any) {
        SocialUtiles.sharedInstance.delegateVC = delegate
        if #available(iOS 13.0, *) {
            SocialUtiles.sharedInstance.appleSignn()
        } else {
            // Fallback on earlier versions
        }
    }
    @IBAction func signUpButtonAction(_ sender: Any) {
        CustomNavigationController().callSignupViewController()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

private extension Login_SignupView {
    
    @objc func xibSetup() {
        backgroundColor = UIColor.clear
        containerView = loadNib()
        
        
        // use bounds not frame or it'll be offset
        containerView.frame = bounds
        // Adding custom subview on top of our view
        addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView ?? UIView()]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[childView]|",
                                                      options: [],
                                                      metrics: nil,
                                                      views: ["childView": containerView ?? UIView()]))
        
    }
}

extension UIView{
    @objc func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
