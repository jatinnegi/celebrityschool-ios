//
//  SocialUtiles.swift
//  Celebrityschool
//
//  Created by Hiren on 03/08/21.
//

import UIKit
import GoogleSignIn
import FBSDKShareKit
import FBSDKLoginKit
import FBSDKCoreKit
import AuthenticationServices

class SocialUtiles{
    
    var hash: Int = 0
    var description: String = ""
    var superclass: AnyClass?
    
    //Facebook Variable
    var delegateVC : UIViewController?
    var userName:String = ""
    var userEmail:String = ""
    var userId:String = ""
    var userImg:String = ""
    var usermobile:String = ""
    
    //apple Variable
    let appleProvider = AppleSignInClient()

    
    static let sharedInstance = SocialUtiles()
    let defaults = UserDefaults.standard

    func delegate() -> UIViewController{
        if let del = delegateVC{
            return del
        }
        return UIViewController()
    }
    
    //MARK:- Google SignIn
    func googleSign(){
        GIDSignIn.sharedInstance().clientID = Constants.GoogleKey
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self.delegate()
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    func facebookSign(){
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self.delegate()) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!

                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    

    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){

                    let UserFacebookDict = result as! NSDictionary
                    self.userName = UserFacebookDict["name"]! as! String
                    if UserFacebookDict["email"] == nil
                    {
                        self.userEmail = ""
                    }
                    else{
                        self.userEmail = UserFacebookDict["email"]! as! String
                    }
                    self.userId = UserFacebookDict["id"]! as! String
                    //self.FBImg = UserFacebookDict["picture"] as! NSDictionary
                    let userPictDict = UserFacebookDict["picture"] as! NSDictionary
                    let userDataDict = userPictDict["data"] as! NSDictionary
                    self.userImg = userDataDict["url"]! as! String
                    self.usermobile = ""
                    self.SocialLogin(loginType: "facebook", pwd: "")
                }
            })
        }
    }
    
    @available(iOS 13.0, *)
    func appleSignn(){
        self.appleProvider.handleAppleIdRequest { (fullName, email, token) in
            self.userName = fullName ?? ""
            self.userEmail = email ?? ""
            self.userId = token ?? ""
            self.userImg = ""
            self.usermobile = ""
            if self.userEmail != ""{
                self.SocialLogin(loginType: "apple", pwd: "")
            }else{
                self.NotGetEmailToCall()
            }
        }
    }
    
    
    //MARK: to login with social
    func SocialLogin(loginType:String,pwd:String) {
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        if String.isNilOrEmpty(string: userEmail){
            AlertIndicator_Toast.sharedInstance.showToast(Constants.KEmailAddressIsBlank, vc: self.delegate())
            return
        }
        
        if !userEmail.isEmail{
            AlertIndicator_Toast.sharedInstance.showToast(Constants.KEmailAddressIsNotValid, vc: self.delegate())
            return
        }

        let param : Dictionary<String, Any> = [
            "email":userEmail,
            "name":userName,
            "social_id":userId,
            "profilepic":userImg,
            "deviceType":"ios",
            "social_platform" : loginType,
            "login_type":"signup",
            "password" : pwd,
            "mobile" : usermobile
        ]
        
        
        
        print("social param==\(param)")
        DispatchQueue.global(qos: .background).async {
            LoginApiServices().loginAPICall(param)
        }
    }
    
    func manualSignUp(name:String,email:String,phone:String,passward:String) {
        self.userName = name
        self.userEmail = email
        self.userId = ""
        self.userImg = ""
        self.usermobile = phone
        
        self.SocialLogin(loginType: "manual", pwd: passward)
    }
    
    
    func manuallogin(email:String,passward:String){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        let param : Dictionary<String, Any> = [
            "email":email,
            "deviceType":"ios",
            "login_type":"login",
            "password" : passward,
        ]
        
        
        print("Email Login param==\(param)")
        DispatchQueue.global(qos: .background).async {
            LoginApiServices().loginAPICall(param)
        }
  
    }
    
    func NotGetEmailToCall(){
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
            NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 12)! // Note the !
        ]
        
        let attributedString = NSAttributedString(string: Constants.KEmailAddressIsNotValid, attributes: [
            NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 14)!,
            NSAttributedString.Key.foregroundColor : UIColor.red
        ])
        
        let attributedStringBlank = NSAttributedString(string: Constants.KEmailAddressIsBlank, attributes: [
            NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 14)!,
            NSAttributedString.Key.foregroundColor : UIColor.red
        ])
        
        let alert = UIAlertController(title: "Please add email", message: nil, preferredStyle: .alert)

        alert.addTextField { txtFiled in
            txtFiled.attributedPlaceholder = NSAttributedString(string: "Type Email Address",
                                                                 attributes: attributes)
            txtFiled.tintColor = .white

        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { [unowned self] _ in
            self.userName = ""
            self.userEmail = ""
            self.userId = ""
            self.userImg = ""
            self.usermobile = ""
        }
        let loginAction = UIAlertAction(title: "Submit", style: .default) { [unowned self] _ in
            guard let email = alert.textFields?[0].text else { return } // Should never happen
            if email.isEmpty{
                alert.setValue(attributedStringBlank, forKey: "attributedMessage")
                alert.show()
               return
            }
            if !email.isEmailCheck{
                alert.setValue(attributedString, forKey: "attributedMessage")
                alert.show()
               return
            }
            self.userEmail = email
            self.SocialLogin(loginType: "apple", pwd: "")
            // Perform login action
        }
            
        cancelAction.setValue(fistColor, forKey: "titleTextColor")
        loginAction.setValue(lastColor, forKey: "titleTextColor")
        alert.addAction(cancelAction)
        alert.addAction(loginAction)
        alert.view.tintColor = .black
        alert.show()
    }


}

extension SocialUtiles:GIDSignInDelegate{
    
    func isEqual(_ object: Any?) -> Bool {return false}
    
    func `self`() -> Self {return self}
    
    func perform(_ aSelector: Selector!) -> Unmanaged<AnyObject>! {return nil}
    
    func perform(_ aSelector: Selector!, with object: Any!) -> Unmanaged<AnyObject>! {return nil}
    
    func perform(_ aSelector: Selector!, with object1: Any!, with object2: Any!) -> Unmanaged<AnyObject>! {return nil}
    
    func isProxy() -> Bool {return false}
    
    func isKind(of aClass: AnyClass) -> Bool {return false}
    
    func isMember(of aClass: AnyClass) -> Bool {return false}
    
    func conforms(to aProtocol: Protocol) -> Bool {return false}
    
    func responds(to aSelector: Selector!) -> Bool {return false}
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        if user == nil
        {
            print("No User")
        }
        else{
            print(user.userID ?? "")
            print(user.profile?.name ?? "")
            print(user.profile?.email ?? "")
            
            userId = user.userID ?? ""
            userName = user.profile?.name ?? ""
            userEmail=user.profile?.email ?? ""
            usermobile = ""
            if ((user.profile?.hasImage) != nil)
            {
                let pic = user.profile?.imageURL(withDimension: 100)
                userImg = pic!.absoluteString
                
            }
            self.SocialLogin(loginType: "google", pwd: "")
            
        }
    }
}

extension SocialUtiles: ASAuthorizationControllerDelegate {
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }

    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {

        if #available(iOS 13.0, *) {
            if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
                
                // Create an account as per your requirement
                let appleId = appleIDCredential.user
                print("appleId\(String(describing: appleId))")
                let appleUserFirstName = appleIDCredential.fullName?.givenName
                print("appleUserFirstName\(String(describing: appleUserFirstName))")
                let appleUserLastName = appleIDCredential.fullName?.familyName
                print("appleUserLastName\(String(describing: appleUserLastName))")
                let appleUserEmail = appleIDCredential.email
                print("appleUserEmail\(String(describing: appleUserEmail))")
                //Write your code
                _ = "\((appleUserFirstName ?? ""))+ \((appleUserLastName ?? ""))"
                
            } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
                print("aaaaaa")
                let appleUsername = passwordCredential.user
                print("appleUsername\(String(describing: appleUsername))")
                let applePassword = passwordCredential.password
                print("applePassword\(String(describing: applePassword))")

                //Write your code
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

extension SocialUtiles: ASAuthorizationControllerPresentationContextProviding {
    //For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.delegate().view.window!
    }
}


