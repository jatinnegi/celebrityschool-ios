//
//  MainTabBarViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 02/08/21.
//

import Foundation

import UIKit

class MainTabBarViewController: UITabBarController {
    
    var prevIndex : Int = 0
    var navHome     = UINavigationController()
    var navCommunity  = UINavigationController()
    var navCompetition  = UINavigationController()
    var navProfile  = UINavigationController()
    var model:AppConfigDataModel?
    private var viewModel: AppConfigApiServices!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = AppConfigApiServices()
        setupViewModel()
        self.delegate = self
        self.view.backgroundColor = .black
        self.tabBar.barTintColor = .black
        // Do any additional setup after loading the view.
    }
    
    private func setupViewModel() {

        viewModel.AppConfigApiCall()
       
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            strongSelf.model = data
            strongSelf.setUPTabScreen()

        }
        
        viewModel.noDataRecieved = {[weak self] in
            guard let strongSelf = self else {return}
            strongSelf.setUPTabScreen()
        }
        
    }
    private func setUPTabScreen(){
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(hexString:"#BBBFD0")!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(hexString: "#BBBFD0")!], for: .selected)
        
        // Home Load
        let item1 = HomeViewController()
        let icon1 =  UITabBarItem(title: "Home", image: UIImage(named: "Home_icon"), selectedImage: UIImage(named: "Home_iconSelected")?.withRenderingMode(.alwaysOriginal))
        item1.tabBarItem = icon1
        navHome = UINavigationController.init(rootViewController: item1)
        navHome.setNavigationBarHidden(true, animated: false)
        navHome.delegate = self
        
        
        let item2 = CommunityViewController()
        let icon2 =  UITabBarItem(title: "Community", image: UIImage(named: "Community_icon"), selectedImage: UIImage(named: "Community_iconSelected")?.withRenderingMode(.alwaysOriginal))
        item2.tabBarItem = icon2
        navCommunity = UINavigationController.init(rootViewController: item2)
        navCommunity.setNavigationBarHidden(true, animated: false)
        navCommunity.delegate = self
        

        let item3 = CompetitionViewController()
        let icon3 =  UITabBarItem(title: "Competition", image: UIImage(named: "Competition_icon"), selectedImage: UIImage(named: "Competition_iconSelected")?.withRenderingMode(.alwaysOriginal))
        item3.tabBarItem = icon3
        navCompetition = UINavigationController.init(rootViewController: item3)
        navCompetition.setNavigationBarHidden(true, animated: false)
        navCompetition.delegate = self

        // Profile Load
        let item4 = ProfileViewController()
        let icon4 =  UITabBarItem(title: "Profile", image: UIImage(named: "Profile_icon"), selectedImage:UIImage(named: "Profile_iconSelected")?.withRenderingMode(.alwaysOriginal))
        item4.tabBarItem = icon4
        navProfile = UINavigationController.init(rootViewController: item4)
        navProfile.setNavigationBarHidden(true, animated: false)
        navProfile.delegate = self
        
        var controllers:[UINavigationController] = []
        controllers.append(navHome)
        

        if let data = model{
            
            if data.chatFeature == true{
                controllers.append(navCommunity)
            }
            
            if data.competition == true{
                controllers.append(navCompetition)
            }
            
            if data.profile == true{
                controllers.append(navProfile)
            }
        }

        self.viewControllers = controllers
       
    }

    func pushViewController(_ viewController : UIViewController, animated : Bool){
        if self.selectedIndex == 0{
            navHome.pushViewController(viewController, animated: animated)
        }else if self.selectedIndex == 1{
            navProfile.pushViewController(viewController, animated: animated)
        }
    }
    
    @objc func fetchNavigationController() -> UINavigationController{
        if self.selectedIndex == 0{
            return navHome
        }
        /*else if self.selectedIndex == 1{
            return navCommunity
        }else if self.selectedIndex == 2{
            return navCompetition
        }*/
        else {
            return navProfile
        }
    }
    
    func tabBarHidden(_ hidden:Bool){
        self.tabBar.isHidden = hidden
    }

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainTabBarViewController :UITabBarControllerDelegate{
    // MARK: - Delegate methods
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        prevIndex = tabBarIndex
        if tabBarIndex == 0 {
            //do your stuff
        }else if tabBarIndex == 1{
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        print("Should select viewController: \(viewController.title ?? "") ?")
        return true;
    }
}

extension UITabBarController {
    func orderedTabBarItemViews() -> [UIView] {
        let interactionViews = tabBar.subviews.filter({$0.isUserInteractionEnabled && $0.tag == 0 }) // Checking tag == 0 to remove Custom Views (eg.: Notification Dot View)
        return interactionViews.sorted(by: {$0.frame.minX < $1.frame.minX})
    }
}

extension MainTabBarViewController:UINavigationControllerDelegate{
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool)  {
        
        if viewController.isKind(of: HomeViewController.self) || viewController.isKind(of: ProfileViewController.self)
            || viewController.isKind(of: CompetitionViewController.self) || viewController.isKind(of: CommunityViewController.self){
            self.tabBar.isHidden = false
        }else {
            self.tabBar.isHidden = true
        }
    }
    
}
