//
//  NotificationViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 17/12/21.
//

import UIKit
//import SmartechAppInbox

class NotificationViewController: CustomNavigationController {

//    var appInboxArray: [SMTAppInboxMessage]?

    override func viewDidLoad() {
        super.viewDidLoad()
//        loadNotification()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.leftBarButtonItem = backBarButton()
        self.navigationItem.title = "Notification"
    }
    
    /*func loadNotification(){
        appInboxArray = SmartechAppInbox.sharedInstance().getMessages(SMTAppInboxMessageType.ALL_MESSAGE)
        print("\(appInboxArray ?? [])")
    }*/



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
