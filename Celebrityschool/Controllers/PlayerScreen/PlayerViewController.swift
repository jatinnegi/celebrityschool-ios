//
//  PlayerViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 21/08/21.
//

import UIKit
import VersaPlayer
import CoreMedia


class PlayerViewController: UIViewController {
    @IBOutlet weak var playerView: VersaPlayerView!
    @IBOutlet weak var controls: VersaPlayerControls!
    @IBOutlet weak var titleLabel: UILabel!{
        didSet{
            titleLabel.text = ""
        }
    }
    
    @IBOutlet weak var subTitleLabel: UILabel!{
        didSet{
            subTitleLabel.text = ""
        }
    }
    @IBOutlet weak var rotatedBtn: UIButton!{
        didSet{
            rotatedBtn.isSelected = false
        }
    }
    
    var videoLeasson:VideoLeassonModel?
    var album_info:CourseModel?
    var videoUrl:String = ""
    var index:Int = 0
    private var viewModel: PlayerApiServices!
    var videoInfoData:FetchVideoLessonModel?
    var slug:String = ""
    var videoPlace:String = "0"
    var category:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = PlayerApiServices()
        setupViewModel()
        setUpTitleBar()
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        appDelegate.rotateToPotraitScapeDevice()
    }
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = true
        
        if let data = album_info{
            
            if let title = data.meta_title{
                titleLabel.text = title
            }
            
            if let a_artist = data.a_artist{
                subTitleLabel.text = a_artist
            }
            if let a_trailer_link = data.a_trailer_link{
                videoUrl = a_trailer_link
            }
            
            if let slu = data.slug{
                slug = slu
            }
            
            if let cat = data.category{
                category = cat
            }
            
        }
        
        
        if let leasson = videoLeasson{
            if let title = leasson.video_title{
                if index != 0{
                    titleLabel.text = "\(index). " + title
                }else{
                    titleLabel.text = title
                }
            }
            
            if let video_url = leasson.video_url{
                videoUrl = video_url
            }
            
            if let place = leasson.video_place{
                videoPlace = place
            }
            
            if videoPlace != "0"{
                viewModel.fetchVideoLessonApiCall(slug, videoIndex: videoPlace)

            }
        }
   
        playerSetUp()
    }
    
    private func setupViewModel() {
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            strongSelf.videoInfoData = data
            
            DispatchQueue.main.async {
                if let time_in_seconds = data.time_in_seconds{
                    let time = CMTime(seconds: Double(time_in_seconds), preferredTimescale: CMTimeScale(NSEC_PER_SEC))
                    self?.playerView.player.seek(to: time)
                    self?.playerView.updateControls(toTime: time)
                }
            }
        }
        
        viewModel.noDataRecieved = {[weak self] in
            guard self != nil else {return}
        }
        
    }
    
    func playerSetUp(){
        playerView.layer.backgroundColor = UIColor.black.cgColor
        playerView.use(controls: controls)
        playerView.controls?.behaviour.shouldShowControls = true

        print("URL:\(videoUrl)")
        if let url = URL.init(string: videoUrl) {
            let item = VersaPlayerItem(url: url)
            playerView.set(item: item)
        }
    }
    
    
    func lastUpdateTime(){
        let floatTime = Float(CMTimeGetSeconds(playerView.player.currentTime()))
        
        print("Time:\(floatTime)")
        var param:[String:Any] = [:]
        param["slug"] = slug
        param["video_index"] = videoPlace
        param["time"] = "\(floatTime)"
        param["category"] = category
        
        
        viewModel.addVideoLessonApiCall(param)
        

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:- UIButton Action
extension PlayerViewController{
    @IBAction func backButtonAction(_ sender: Any) {
        if videoPlace != "0"{
            lastUpdateTime()
        }
        appDelegate.rotateToPotraitScapeDevice()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rotatedBtnAction(_ sender: Any) {
        if !rotatedBtn.isSelected{
            rotatedBtn.isSelected = true
            appDelegate.rotateToLandsScapeDevice()
        }else{
            rotatedBtn.isSelected = false
            appDelegate.rotateToPotraitScapeDevice()
        }
        
    }
}
