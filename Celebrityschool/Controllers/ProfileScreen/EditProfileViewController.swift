//
//  EditProfileViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 06/09/21.
//

import UIKit
import SDWebImageWebPCoder


class EditProfileViewController: CustomNavigationController {
    
    @IBOutlet var scrollViewOutlet: UIScrollView?
    var exscrollView:UIScrollView?
    
    let attributes = [
        NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 12)! // Note the !
    ]
    
    @IBOutlet var allBgTxtFiledView: [UIView]!{
        didSet{
            allBgTxtFiledView.forEach { $0.layer.cornerRadius = 5 }
            allBgTxtFiledView.forEach { $0.layer.masksToBounds = true }
            allBgTxtFiledView.forEach { $0.backgroundColor = .white }
        }
    }
    
    @IBOutlet weak var firstNameTxtFiled: UITextField!{
        didSet{
            firstNameTxtFiled.delegate = self
            firstNameTxtFiled.returnKeyType = .next
            firstNameTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type First Name",
                                                                   attributes: attributes)
        }
    }
    
    @IBOutlet weak var lastNameTxtFiled: UITextField!{
        didSet{
            lastNameTxtFiled.delegate = self
            lastNameTxtFiled.returnKeyType = .next
            lastNameTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Last Name",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var emailTxtFiled: UITextField!{
        didSet{
            emailTxtFiled.delegate = self
            emailTxtFiled.returnKeyType = .next
            emailTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Email Address",
                                                                   attributes: attributes)
            emailTxtFiled.isUserInteractionEnabled = false
        }
    }
    @IBOutlet weak var phoneTxtFiled: UITextField!{
        didSet{
            phoneTxtFiled.delegate = self
            phoneTxtFiled.returnKeyType = .next
            phoneTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Phone Number",
                                                                   attributes: attributes)
        }
    }
    
    @IBOutlet weak var ageTxtFiled: UITextField!{
        didSet{
            ageTxtFiled.delegate = self
            ageTxtFiled.returnKeyType = .next
            ageTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Age",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var cityTxtFiled: UITextField!{
        didSet{
            cityTxtFiled.delegate = self
            cityTxtFiled.returnKeyType = .next
            cityTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type City Name",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var stateTxtFiled: UITextField!{
        didSet{
            stateTxtFiled.delegate = self
            stateTxtFiled.returnKeyType = .next
            stateTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type State Name",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var PincodeTxtFiled: UITextField!{
        didSet{
            PincodeTxtFiled.delegate = self
            PincodeTxtFiled.returnKeyType = .next
            PincodeTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Pincode",
                                                                   attributes: attributes)
        }
    }
    
    @IBOutlet weak var certificatedStatckView: UIStackView!{
        didSet{
            certificatedStatckView.isHidden = false
        }
    }
    @IBOutlet weak var certificateTxtFiled: UITextField!{
        didSet{
            certificateTxtFiled.delegate = self
            certificateTxtFiled.returnKeyType = .next
            certificateTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Certificate Name",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var professionTxtField: UITextField!{
        didSet{
            professionTxtField.delegate = self
            professionTxtField.returnKeyType = .next
            professionTxtField.attributedPlaceholder = NSAttributedString(string: "Type Profession",
                                                                   attributes: attributes)
        }
    }
    
    let PlaceholderText = "Tell me about"
    let placeholderfont = UIFont(name: "Roboto-Regular", size: 12)!
    let normalfont = UIFont(name: "Roboto-Regular", size: 15)!

    @IBOutlet weak var aboutMeTxtView: UITextView!{
        didSet{
            aboutMeTxtView.delegate = self
            aboutMeTxtView.returnKeyType = .done
            aboutMeTxtView.text = PlaceholderText
            aboutMeTxtView.textColor = UIColor(hexString: "#D3D6D7")!
            aboutMeTxtView.font = placeholderfont
            
            aboutMeTxtView.layer.cornerRadius = 5
            aboutMeTxtView.layer.masksToBounds = true
            aboutMeTxtView.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var saveBtn: UIButton!{
        didSet{
            saveBtn.layer.cornerRadius = 5
            saveBtn.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var profileImageBgView: UIView!{
        didSet{
            profileImageBgView.layer.cornerRadius = profileImageBgView.frame.height / 2
            profileImageBgView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
            profileImageView.layer.masksToBounds = true
            profileImageView.contentMode = .scaleAspectFit
            profileImageView.image = UIImage(named: "profile_icon")
        }
    }
    
    @IBOutlet weak var FirstNameImage: UIImageView!
    @IBOutlet weak var LastNameImage: UIImageView!
    @IBOutlet weak var EmailImage: UIImageView!
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var ageimage: UIImageView!
    @IBOutlet weak var cityImage: UIImageView!
    @IBOutlet weak var stateImage: UIImageView!
    @IBOutlet weak var pincodeImage: UIImageView!
    @IBOutlet weak var certificateImage: UIImageView!
    @IBOutlet weak var professionImage: UIImageView!
    
    private var viewModel: ProfileApiServices!
    private var viewModelTwo: CompetitionApiServices!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.setProfileData()
        viewModel = ProfileApiServices()
        viewModelTwo = CompetitionApiServices()
        setupViewModel()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async{
            self.saveBtn.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.exscrollView?.isScrollEnabled = true
        self.registerKeyboardNotifications()
    }
    
    deinit {
        self.unregisterKeyboardNotifications()
    }
    
    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.leftBarButtonItem = backBarButton()
        self.navigationItem.title = ""
    }
    
    
    func setProfileData(){
        if let fname = UserDefaults.standard.fetchString(key: "first_name") as? String{
            firstNameTxtFiled.text = fname
        }
        
        if let lname = UserDefaults.standard.fetchString(key: "last_name") as? String{
            lastNameTxtFiled.text = lname
        }
        
        if let email = UserDefaults.standard.fetchString(key: Constants.UserEmailKey) as? String{
            emailTxtFiled.text = email
        }
        
        if let mobile = UserDefaults.standard.fetchString(key: "mobile") as? String{
            phoneTxtFiled.text = mobile
        }
        
        if let city = UserDefaults.standard.fetchString(key: "city") as? String{
            cityTxtFiled.text = city
        }
        
        if let state = UserDefaults.standard.fetchString(key: "state") as? String{
            stateTxtFiled.text = state
        }
        
        if let aboutMe = UserDefaults.standard.fetchString(key: "about_me") as? String,aboutMe != ""{
            aboutMeTxtView.text = aboutMe
            aboutMeTxtView.textColor = UIColor(hexString: "#111111")!
            aboutMeTxtView.font = normalfont
        }
        
        if let image = UserDefaults.standard.fetchString(key: "profilePic") as? String{
            if image.isValidHttpsUrl{
                if let url = URL(string:image){
                    DispatchQueue.main.async{
                        self.profileImageView.sd_setImage(with: url, placeholderImage: nil, options: [.progressiveLoad])
                        self.profileImageView.contentMode = .scaleAspectFill
                        self.profileImageBgView.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
                    }
                }
            }
        }
        
        if let certificate_name = UserDefaults.standard.fetchString(key: "certificate_name") as? String,certificate_name != ""{
            certificateTxtFiled.text = certificate_name
            certificateTxtFiled.isUserInteractionEnabled = false
        }
        
        if let profession = UserDefaults.standard.fetchString(key: "profession") as? String,profession != ""{
            professionTxtField.text = profession
        }
        
        if let age = UserDefaults.standard.fetchString(key: "age") as? String,age != ""{
            ageTxtFiled.text = age
        }
        
        if let pincode = UserDefaults.standard.fetchString(key: "pincode") as? String,pincode != ""{
            PincodeTxtFiled.text = pincode
        }
    }
    
    
    private func validateValue() -> Bool{

        if PincodeTxtFiled.text?.length ?? 0 > 0{
            if PincodeTxtFiled.text?.length != 6{
                AlertIndicator_Toast().showToast(Constants.KPincode, vc: self)
                return false
            }
        }
       
        if !(phoneTxtFiled.text?.isPhoneNumber ?? false){
            AlertIndicator_Toast().showToast(Constants.KPhoneNumberIsNotValid, vc: self)
            return false
        }
        
        return true
    }
    
    private func setupViewModel() {
        viewModel.updateProfile = {[weak self] status in
            guard let strongSelf = self else {return}
            
            if status == true{
                NotificationCenter.default.post(name: Notification.Name("ProfileUpdate"), object: nil)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    strongSelf.navigationController?.popViewController(animated: true)
                }
                
            }
        }
        
        
        viewModelTwo.didReceiveProfileData = {[weak self] data in
            guard self != nil else {return}
            
            guard let dataModel = data.data else{return}
            guard let fileName = dataModel.fileUrl else{return}
            UserDefaults.standard.saveInDefault(value:fileName, key: "profilePic")
            NotificationCenter.default.post(name: Notification.Name("ProfileUpdate"), object: nil)
            if let topController = UIApplication.topViewController(){
                AlertIndicator_Toast().showToast("Image updated sucessfully", vc: topController)
            }
            
        }
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK:UIButton Action
extension EditProfileViewController{
    @IBAction func saveBtnAction(_ sender: Any) {
        if validateValue(){
            if aboutMeTxtView.text == PlaceholderText {
                aboutMeTxtView.text = ""
            }
            let dict = [
                "first_name":firstNameTxtFiled.text ?? "",
                "last_name":lastNameTxtFiled.text ?? "",
                "certificatename":certificateTxtFiled.text ?? "",
                "mobile":phoneTxtFiled.text ?? "",
                "age":ageTxtFiled.text ?? "",
                "city":cityTxtFiled.text ?? "",
                "state":stateTxtFiled.text ?? "",
                "pincode":PincodeTxtFiled.text ?? "",
                "profession":professionTxtField.text ?? "",
                "about_me":aboutMeTxtView.text ?? "",
                ] as [String : Any]
            viewModel.updateProfileApiCall(dict)
        }
    }
    
    @IBAction func profileImageChangeButtonAction(_ sender: Any) {
        ImagePickerManager().pickImage(self){[weak self] image in
            //here is the image
            guard let strongSelf = self else {return}
            DispatchQueue.main.async{
                strongSelf.profileImageView.image = image
                strongSelf.profileImageView.contentMode = .scaleAspectFill
                strongSelf.profileImageBgView.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    if let imageData = image.jpegData(compressionQuality: 1){
                        strongSelf.viewModelTwo.imageViewUpload(imageData, category: "profile", type:"image/jpg")
                    }
                }
            }
        }
    }
}

extension EditProfileViewController:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor(hexString: "#D3D6D7")! {
            textView.text = ""
            textView.textColor = UIColor(hexString: "#111111")!
            textView.font = normalfont
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = PlaceholderText
            textView.textColor = UIColor(hexString: "#D3D6D7")!
            textView.font = placeholderfont

        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return textView.text.count + (text.count - range.length) <= 100
    }
    
}

//MARK:UITextFiled Delegate
extension EditProfileViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if textField == self.firstNameTxtFiled{
            self.lastNameTxtFiled.becomeFirstResponder()
        }else if textField == self.lastNameTxtFiled{
            self.phoneTxtFiled.becomeFirstResponder()
        }else if textField == self.phoneTxtFiled{
            self.ageTxtFiled.becomeFirstResponder()
        }else if textField == self.ageTxtFiled{
            self.cityTxtFiled.becomeFirstResponder()
        }else if textField == self.cityTxtFiled {
            self.stateTxtFiled.becomeFirstResponder()
        }else if textField == self.stateTxtFiled{
            self.PincodeTxtFiled.becomeFirstResponder()
        }else if textField == self.PincodeTxtFiled{
            if let certificate_name = UserDefaults.standard.fetchString(key: "certificate_name") as? String,certificate_name != ""{
                self.professionTxtField.becomeFirstResponder()
            }else{
                self.certificateTxtFiled.becomeFirstResponder()
            }
        }else if textField == self.certificateTxtFiled{
            self.professionTxtField.becomeFirstResponder()
        }else if textField == self.professionTxtField{
            self.aboutMeTxtView.becomeFirstResponder()
        }
       return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.firstNameTxtFiled{
            FirstNameImage.image = UIImage(named: "name_iconSelected")
        }else if textField == self.lastNameTxtFiled{
            LastNameImage.image = UIImage(named: "name_iconSelected")
        }else if textField == self.phoneTxtFiled{
            phoneImage.image = UIImage(named: "phone_iconSelected")
        }else if textField == self.ageTxtFiled{
            ageimage.image = UIImage(named: "AgeSelectedicon")
        }else if textField == self.cityTxtFiled {
            cityImage.image = UIImage(named: "locationSelectedicon")
        }else if textField == self.stateTxtFiled{
            stateImage.image = UIImage(named: "locationSelectedicon")
        }else if textField == self.PincodeTxtFiled{
            pincodeImage.image = UIImage(named: "locationSelectedicon")

        }else if textField == self.certificateTxtFiled{
            certificateImage.image = UIImage(named: "certificateSelectedicon")
        }else if textField == self.professionTxtField{
            professionImage.image = UIImage(named: "ProfessionSelectedicon")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.firstNameTxtFiled{
            FirstNameImage.image = UIImage(named: "name_iconDeselected")
        }else if textField == self.lastNameTxtFiled{
            LastNameImage.image = UIImage(named: "name_iconDeselected")
        }else if textField == self.phoneTxtFiled{
            phoneImage.image = UIImage(named: "phone_iconDeselected")
        }else if textField == self.ageTxtFiled{
            ageimage.image = UIImage(named: "AgeDeselecticon")
        }else if textField == self.cityTxtFiled {
            cityImage.image = UIImage(named: "locationDeselecticon")
        }else if textField == self.stateTxtFiled{
            stateImage.image = UIImage(named: "locationDeselecticon")
        }else if textField == self.PincodeTxtFiled{
            pincodeImage.image = UIImage(named: "locationDeselecticon")

        }else if textField == self.certificateTxtFiled{
            certificateImage.image = UIImage(named: "certificateDeselecticon")
        }else if textField == self.professionTxtField{
            professionImage.image = UIImage(named: "ProfessionDeselecticon")
        }
    }
}
//MARK:KeyBoard handling
extension EditProfileViewController:ScrollViewKeyboardDelegate {
    var scrollView: UIScrollView? {
        get {
            return scrollViewOutlet
        }
        set {
            exscrollView = newValue
        }
    }
}
