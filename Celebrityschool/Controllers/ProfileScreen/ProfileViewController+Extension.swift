//
//  ProfileViewController+Extension.swift
//  Celebrityschool
//
//  Created by Hiren on 28/08/21.
//

import UIKit


extension ProfileViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileCollectionViewCell.reuseIdentifier, for: indexPath) as! ProfileCollectionViewCell
            cell.loadCell(profileCompleted)
            return cell
        }else if indexPath.row == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainCollectionViewCell.reuseIdentifier, for: indexPath) as! MainCollectionViewCell
            if let data = course{
                cell.loadCell2(modelCourse:data)
            }
           
            return cell
        }else if indexPath.row == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CoursesAnalysisMainCollectionViewCell.reuseIdentifier, for: indexPath) as! CoursesAnalysisMainCollectionViewCell
            if let data = courseProgress{
                cell.loadCell(data)
            }
            return cell
        }else if indexPath.row == 3{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CertificateMainCollectionViewCell.reuseIdentifier, for: indexPath) as! CertificateMainCollectionViewCell
            if let data = course{
                cell.loadCell2(modelCourse:data)
            }
            
            if profileCompleted >= 100.0{
                cell.lockHide = true
            }else{
                cell.lockHide = false
            }
           
            return cell
        }else if indexPath.row == 4{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RestorePurchaseCollectionViewCell.reuseIdentifier, for: indexPath) as! RestorePurchaseCollectionViewCell
            return cell
        }else if indexPath.row == 5{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CouponCodeCollectionViewCell.reuseIdentifier, for: indexPath) as! CouponCodeCollectionViewCell
            return cell
        }else if indexPath.row == 6 || indexPath.row == 7{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CommonCollectionViewCell.reuseIdentifier, for: indexPath) as! CommonCollectionViewCell
            if indexPath.row == 6{
                cell.titleLabel.text = "Terms of Use"
            }else{
                cell.titleLabel.text = "Privacy Policy"
            }
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileLogoutCollectionViewCell.reuseIdentifier, for: indexPath) as! ProfileLogoutCollectionViewCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 6{
            CustomNavigationController().callEbookViewController(nil, privateUrl: Constants.termsCondition)

        }else if indexPath.row == 7{
            CustomNavigationController().callEbookViewController(nil, privateUrl: Constants.privacypolicy)

        }
    }
}

extension ProfileViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat = collectionView.frame.width
        var height:CGFloat = 0.0
        if indexPath.row == 0{
            if let aboutMe = UserDefaults.standard.fetchString(key: "about_me") as? String,aboutMe != ""{
                if profileCompleted >= 100.0{
                    height = headerheight + heightForView(text: description, font: UIFont(name: "Roboto-Regular", size: 12)!, width: UIScreen.main.bounds.width) - 20
                }else{
                    height = headerheight + heightForView(text: description, font: UIFont(name: "Roboto-Regular", size: 12)!, width: UIScreen.main.bounds.width) + 60
                }
            }else{
                height = headerheight
            }
        }else if indexPath.row == 1{
            if course?.count ?? 0 > 0{
                height = width / 2
            }else{
                height = 0
            }
        }else if indexPath.row == 2{
            if courseProgress?.count ?? 0 > 0{
                height = 367.0
            }else{
                height = 0
            }
        }else if indexPath.row == 3{
            if course?.count ?? 0 > 0{
                height = width / 1.8
            }else{
                height = 0
            }
        }else if indexPath.row == 4{
            if let data = model,data.restorePurchase == true{
                height = 80.0
            }else{
                height = 0
            }
        }else if indexPath.row == 5{
            if let data = model,data.couponCode == true{
                height = 80.0
            }else{
                height = 0
            }
        }else if indexPath.row == 6 || indexPath.row == 7{
            height = 60.0
        }
        else{
            height = 80.0
        }
        
        
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
}
