//
//  ProfileViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 02/08/21.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileCollcetionView: UICollectionView!{
        didSet{
            profileCollcetionView.delegate = self
            profileCollcetionView.dataSource = self
            profileCollcetionView.register(ProfileCollectionViewCell.nib, forCellWithReuseIdentifier: ProfileCollectionViewCell.reuseIdentifier)
            profileCollcetionView.register(ProfileLogoutCollectionViewCell.nib, forCellWithReuseIdentifier: ProfileLogoutCollectionViewCell.reuseIdentifier)
            profileCollcetionView.register(MainCollectionViewCell.nib, forCellWithReuseIdentifier: MainCollectionViewCell.reuseIdentifier)
            profileCollcetionView.register(CoursesAnalysisMainCollectionViewCell.nib, forCellWithReuseIdentifier: CoursesAnalysisMainCollectionViewCell.reuseIdentifier)
            profileCollcetionView.register(CertificateMainCollectionViewCell.nib, forCellWithReuseIdentifier: CertificateMainCollectionViewCell.reuseIdentifier)
            profileCollcetionView.register(RestorePurchaseCollectionViewCell.nib, forCellWithReuseIdentifier: RestorePurchaseCollectionViewCell.reuseIdentifier)
            profileCollcetionView.register(CouponCodeCollectionViewCell.nib, forCellWithReuseIdentifier: CouponCodeCollectionViewCell.reuseIdentifier)
            profileCollcetionView.register(CommonCollectionViewCell.nib, forCellWithReuseIdentifier: CommonCollectionViewCell.reuseIdentifier)
        }
    }
    
    private var viewModel: CoursePurchaseApiServices!
    var profileData:AppData?
    var courseProgress:[CourseProgressModel]?
    var course:[CourseModel]?
    var headerheight: CGFloat = 350
    var model:AppConfigDataModel?
    
    var score = 100.0 / 7.0
    var profileCompleted:Double = 0.0
    
    @IBOutlet weak var cc_CollcetionViewTop: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        observerCall()
        viewModel = CoursePurchaseApiServices()
        setupViewModel()
        self.profileload()
        if let appdata = UserDefaults.standard.fetchString(key: "AppConfig") as? [String:Any]{
            model = AppConfigDataModel(dictionary: appdata)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var topSafeArea: CGFloat = 0.0

        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
        } else {
            topSafeArea = topLayoutGuide.length
        }
        
        cc_CollcetionViewTop.constant = -topSafeArea
        
        switch UIDevice().type {
        case .iPad2, .iPad3, .iPad4 , .iPad5 , .iPad6 , .iPad7 , .iPad8, .iPad9, .iPadAir, .iPadAir2, .iPadAir3, .iPadAir4, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadMini6, .iPadPro9_7, .iPadPro10_5, .iPadPro11, .iPadPro2_11, .iPadPro3_11, .iPadPro12_9, .iPadPro2_12_9, .iPadPro3_12_9, .iPadPro4_12_9, .iPadPro5_12_9:
            headerheight = 550
            case .unrecognized:
                print("Device model unrecognized");
            default:break
        }
        // safe area values are now available to use
    }
    
    private func observerCall(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadProfile(notification:)), name: Notification.Name("ProfileUpdate"), object: nil)
    }
    
    @objc func reloadProfile(notification: Notification) {
        DispatchQueue.main.async {
            self.profileload()
        }
    }
    
    private func setupViewModel() {
        
        viewModel.PurchasedCourseApiCall()
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            
            DispatchQueue.main.async {
                strongSelf.courseProgress = data.course_progress
                strongSelf.course = data.course_purchased
                strongSelf.profileCollcetionView.reloadData()
            }
            
        }
        
//        viewModel.noDataRecieved = {[weak self] in
//        }
        
    }
    
    
    func profileload(){
        profileCompleted = 0.0
        if let fname = UserDefaults.standard.fetchString(key: "first_name") as? String, fname != ""{
            profileCompleted += score
        }
        
        if let lname = UserDefaults.standard.fetchString(key: "last_name") as? String, lname != ""{
            profileCompleted += score
        }
        
        
        if let city = UserDefaults.standard.fetchString(key: "city") as? String, city != ""{
            profileCompleted += score
        }
        
        if let state = UserDefaults.standard.fetchString(key: "state") as? String, state != ""{
            profileCompleted += score
        }
        
        
        if let aboutMe = UserDefaults.standard.fetchString(key: "about_me") as? String,aboutMe != ""{
            profileCompleted += score
        }
        
//        if let image = UserDefaults.standard.fetchString(key: "profilePic") as? String, image != ""{
//            profileCompleted += score
//        }
        
        if let certificate_name = UserDefaults.standard.fetchString(key: "certificate_name") as? String,certificate_name != ""{
            profileCompleted += score
        }
        
        if let profession = UserDefaults.standard.fetchString(key: "profession") as? String,profession != ""{
            profileCompleted += score
        }
        
        self.profileCollcetionView.reloadData()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
