//
//  SignupViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 31/07/21.
//

import UIKit

class SignupViewController: CustomNavigationController {
    
    @IBOutlet var scrollViewOutlet: UIScrollView?
    var exscrollView:UIScrollView?
    
    let attributes = [
        NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 12)! // Note the !
    ]

    @IBOutlet weak var signupButton: UIButton!{
        didSet{
            signupButton.layer.cornerRadius = 5
            signupButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var profileImageBgView: UIView!{
        didSet{
            profileImageBgView.layer.cornerRadius = profileImageBgView.frame.height / 2
            profileImageBgView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
            profileImageView.layer.masksToBounds = true
            profileImageView.contentMode = .scaleAspectFit
            profileImageView.image = UIImage(named: "profile_icon")
        }
    }
    
    @IBOutlet var allBgTxtFiledView: [UIView]!{
        didSet{
            allBgTxtFiledView.forEach { $0.layer.cornerRadius = 5 }
            allBgTxtFiledView.forEach { $0.layer.masksToBounds = true }
            allBgTxtFiledView.forEach { $0.backgroundColor = .white }
        }
    }
    @IBOutlet weak var nameTxtFiled: UITextField!{
        didSet{
            nameTxtFiled.delegate = self
            nameTxtFiled.returnKeyType = .next
            nameTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Full Name",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var emailTxtFiled: UITextField!{
        didSet{
            emailTxtFiled.delegate = self
            emailTxtFiled.returnKeyType = .next
            emailTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Email Address",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var phoneNumTxtFiled: UITextField!{
        didSet{
            phoneNumTxtFiled.delegate = self
            phoneNumTxtFiled.returnKeyType = .next
            phoneNumTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Phone Number",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var passwardTxtFiled: UITextField!{
        didSet{
            passwardTxtFiled.delegate = self
            passwardTxtFiled.returnKeyType = .next
            passwardTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Password",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var confirmPasswardTxtFiled: UITextField!{
        didSet{
            confirmPasswardTxtFiled.delegate = self
            confirmPasswardTxtFiled.returnKeyType = .done
            confirmPasswardTxtFiled.attributedPlaceholder = NSAttributedString(string: "Type Confirm Password",
                                                                   attributes: attributes)
        }
    }
    
    @IBOutlet weak var name_iconImageView: UIImageView!{
        didSet{
            name_iconImageView.image = UIImage(named: "name_iconDeselected")//name_iconSelected
        }
    }
    @IBOutlet weak var email_iconImageView: UIImageView!{
        didSet{
            email_iconImageView.image = UIImage(named: "email_iconDeselected")//email_iconSelected
        }
    }
    @IBOutlet weak var phone_iconImageView: UIImageView!{
        didSet{
            phone_iconImageView.image = UIImage(named: "phone_iconDeselected")//phone_iconSelected
        }
    }
    @IBOutlet weak var pwd_iconImageView: UIImageView!{
        didSet{
            pwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")//pwd_iconSelected
        }
    }
    @IBOutlet weak var Cpwd_iconImageView: UIImageView!{
        didSet{
            Cpwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")//pwd_iconSelected
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async{
            self.signupButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.exscrollView?.isScrollEnabled = true
        self.registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    deinit {
        self.unregisterKeyboardNotifications()
    }

    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationItem.leftBarButtonItem = backBarButton()
        self.navigationItem.title = ""
       
    }
    
    private func validateValue() -> Bool{
        
        if !(emailTxtFiled.text?.isEmailCheck ?? false){
            AlertIndicator_Toast().showToast(Constants.KEmailAddressIsNotValid, vc: self)
            return false
        }
        
        if !(phoneNumTxtFiled.text?.isPhoneNumber ?? false){
            AlertIndicator_Toast().showToast(Constants.KPhoneNumberIsNotValid, vc: self)
            return false
        }
        
        if  passwardTxtFiled.text != confirmPasswardTxtFiled.text{
            AlertIndicator_Toast().showToast(Constants.KPasswardNotMatch, vc: self)
            return false
        }
        
        if !(confirmPasswardTxtFiled.text?.isValidPassword ?? false){
            AlertIndicator_Toast().showToast(Constants.KPasswardNotValid, vc: self)
            return false
        }
        
        return true
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:UIButton Action
extension SignupViewController{
    @IBAction func signupButtonAction(_ sender: Any) {
        if validateValue(){
            SocialUtiles.sharedInstance.delegateVC = self
            SocialUtiles.sharedInstance.manualSignUp(name: nameTxtFiled.text ?? "", email: emailTxtFiled.text ?? "", phone: phoneNumTxtFiled.text ?? "", passward: confirmPasswardTxtFiled.text ?? "")
        }
    }
    
    @IBAction func cShowHidePwdButtonAction(_ sender: UIButton) {
        if !sender.isSelected{
            sender.isSelected = true
            confirmPasswardTxtFiled.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            confirmPasswardTxtFiled.isSecureTextEntry = true
        }
    }
    @IBAction func showhidePwdButtonAction(_ sender: UIButton) {
        if !sender.isSelected{
            sender.isSelected = true
            passwardTxtFiled.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            passwardTxtFiled.isSecureTextEntry = true
        }
    }
    
    @IBAction func profileImageChangeButtonAction(_ sender: Any) {
        /*ImagePickerManager().pickImage(self){ image in
            //here is the image
            self.profileImageView.image = image
            self.profileImageView.contentMode = .scaleAspectFill
            self.profileImageBgView.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }*/
    }
}
//MARK:UITextFiled Delegate
extension SignupViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        if textField == self.nameTxtFiled{
            self.emailTxtFiled.becomeFirstResponder()
        }else if textField == self.emailTxtFiled{
            self.phoneNumTxtFiled.becomeFirstResponder()
        }else if textField == self.phoneNumTxtFiled{
            self.passwardTxtFiled.becomeFirstResponder()
        }else if textField == self.passwardTxtFiled {
            self.confirmPasswardTxtFiled.becomeFirstResponder()
        }else if textField == self.confirmPasswardTxtFiled{
            // Call login api
            textField.resignFirstResponder()
            signupButtonAction(self)
        }
       return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.nameTxtFiled{
            name_iconImageView.image = UIImage(named: "name_iconSelected")
        }else if textField == self.emailTxtFiled{
            email_iconImageView.image = UIImage(named: "email_iconSelected")
        }else if textField == self.phoneNumTxtFiled{
            phone_iconImageView.image = UIImage(named: "phone_iconSelected")
        }else if textField == self.passwardTxtFiled {
            pwd_iconImageView.image = UIImage(named: "pwd_iconSelected")
        }else if textField == self.confirmPasswardTxtFiled{
            Cpwd_iconImageView.image = UIImage(named: "pwd_iconSelected")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.nameTxtFiled{
            name_iconImageView.image = UIImage(named: "name_iconDeselected")
        }else if textField == self.emailTxtFiled{
            email_iconImageView.image = UIImage(named: "email_iconDeselected")
        }else if textField == self.phoneNumTxtFiled{
            phone_iconImageView.image = UIImage(named: "phone_iconDeselected")
        }else if textField == self.passwardTxtFiled {
            pwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")
        }else if textField == self.confirmPasswardTxtFiled{
            // Call login api
            Cpwd_iconImageView.image = UIImage(named: "pwd_iconDeselected")
        }
    }
}
//MARK:KeyBoard handling
extension SignupViewController:ScrollViewKeyboardDelegate {
    var scrollView: UIScrollView? {
        get {
            return scrollViewOutlet
        }
        set {
            exscrollView = newValue
        }
    }
    
}


