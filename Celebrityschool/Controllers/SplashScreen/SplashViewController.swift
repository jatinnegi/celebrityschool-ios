//
//  SplashViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 28/08/21.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var launchAppIcon: UIImageView!
    
    @IBOutlet weak var cc_CollcetionViewBottom: NSLayoutConstraint!
    @IBOutlet weak var cc_CollcetionViewTop: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.animateLogo()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.setUpTitleBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var topSafeArea: CGFloat = 0.0
        var bottomSafeArea: CGFloat = 0.0

        if #available(iOS 11.0, *) {
            topSafeArea = view.safeAreaInsets.top
            bottomSafeArea = view.safeAreaInsets.bottom
        } else {
            topSafeArea = topLayoutGuide.length
            bottomSafeArea = bottomLayoutGuide.length
        }
        
        cc_CollcetionViewTop.constant = -topSafeArea
        cc_CollcetionViewBottom.constant = +bottomSafeArea

        // safe area values are now available to use
    }
    
    private func setUpTitleBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func animateLogo(){
        self.launchAppIcon.transform = CGAffineTransform(translationX: 0, y: -200).concatenating(CGAffineTransform(scaleX:0.5, y:0.5))
        self.launchAppIcon.alpha = 0
        
        self.titleLabel.transform = CGAffineTransform(translationX: 0, y: -200).concatenating(CGAffineTransform(scaleX:0.5, y:0.5))
        self.titleLabel.alpha = 0

        UIView.animate(withDuration: 2.0, delay: 0.5, usingSpringWithDamping: 0.8, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.launchAppIcon.transform = .identity
            self.launchAppIcon.alpha = 1
            
            self.titleLabel.transform = .identity
            self.titleLabel.alpha = 1
        }, completion: {_ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                // Put your code which should be executed with a delay here
                if !UserDefaults.standard.isLoggedIn(){
                    CustomNavigationController().setRootviewAsLandingViewController()
                }else{
                    CustomNavigationController().setRootviewAsTabBarViewController()
                }
            }
        })
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
