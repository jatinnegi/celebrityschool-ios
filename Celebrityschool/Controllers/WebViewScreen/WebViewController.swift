//
//  WebViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 05/09/21.
//

import UIKit
import WebKit

class WebViewController: CustomNavigationController{
    @IBOutlet weak var webView: WKWebView!{
        didSet{
            webView.uiDelegate = self
            webView.navigationDelegate = self
        }
    }
    var videoModel:VideoLeassonModel?
    private var viewModel: EbookApiServices!
    var model:EbookDataModel?
    var urlString:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        if videoModel != nil{
            viewModel = EbookApiServices()
            setupViewModel()
        }else{
            if let uString = urlString{
                if let url = URL(string: uString){
                    webView.load(URLRequest(url: url))
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.async {
            self.setUpTitleBar()
        }
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func setUpTitleBar(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)

        navigationItem.leftBarButtonItem = backBarButton()
        self.navigationItem.title = ""
        if let model = videoModel{
            self.navigationItem.title = model.video_title
            self.webView.backgroundColor = .white
        }else{
            UINavigationBar.appearance().backgroundColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        }
        
    }
    
    private func setupViewModel() {
       
        if let data = videoModel{
            viewModel.EbookApiCall(data.video_url ?? "")
        }
       
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            DispatchQueue.main.async() {
                strongSelf.model = data
                if let description = data.ebook_description{
                    strongSelf.webView.loadHTMLString(description, baseURL: nil)
                }
            }
        }
        
//        viewModel.noDataRecieved = {[weak self] in
//        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WebViewController: WKUIDelegate, WKNavigationDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.hideProgressHUD()
        }
        print("Finished loading")
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        print("Start loading")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("\(error.localizedDescription)")
    }
}
