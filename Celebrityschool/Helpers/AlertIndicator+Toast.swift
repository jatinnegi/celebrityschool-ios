//
//  AlertIndicator+Toast.swift
//  Celebrityschool
//
//  Created by Hiren on 04/08/21.
//

import UIKit
import Toast_Swift

class AlertIndicator_Toast: NSObject {

    var style = ToastStyle()
    static let sharedInstance = AlertIndicator_Toast()
    
    override init() {
        super.init()
//        .layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        style.backgroundColor = UIColor(hexString: "#D3D6D7")!
        style.messageColor = .black
        style.messageFont = UIFont(name: "Roboto-Regular", size:18)!
    }
    
    func showToast(_ msg:String,vc:UIViewController) {
        // present the toast with the new style
        vc.view.makeToast(msg, duration: 2.0, position: .bottom, style: style)
    }
}
