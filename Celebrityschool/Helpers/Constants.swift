//
//  Constants.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let app =  UIApplication.shared
let fistColor = UIColor(hexString: "#8036E7")!
let lastColor = UIColor(hexString: "#FF3365")!
let token = UserDefaults.standard.fetchString(key: Constants.UserTokenKey) as! String
let email = UserDefaults.standard.fetchString(key: Constants.UserEmailKey) as? String ?? ""
typealias CompletionHandler = (_ success: Bool, _ response: Any?) -> Void
typealias ProgressBlock = ((Progress) -> Void)

struct Constants {

    // MARK: General Constants
    static let DeviceTokenKey = "device_token"
    static let DeviceInfoKey = "device_info"
    static let DeviceTypeKey = "device_type"
    static let EmptyString = ""
    
    // MARK: User Defaults
    static let UserDefaultsDeviceTokenKey = "DeviceTokenKey"
    static let UserTokenKey = "UserTokenKey"
    static let UserEmailKey = "UserEmailKey"

    
    // MARK: Enums
    enum RequestType: NSInteger {
        case GET
        case POST
        case MultiPartPost
        case DELETE
        case PUT
    }
    
    // MARK: Numerical Constants
    static let StatusSuccess = 1
    static let ResponseStatusSuccess = 200
    static let ResponseStatusCreated = 201
    static let ResponseStatusAccepted = 202
    static let ResponseStatusNoResponse = 204
    static let ResponseStatusForbidden = 401
    static let ResponseStatusAleradyExist = 409
    static let ResponseStatusEmailNotFound = 422
    static let ResponseStatusServerError = 500
    static let ResponseInvalidCredential = 401
    static let ResponseStatusUserNotExist = 404
    
    // MARK: Network Keys
    static let StagingEnviroment = "STAGING"
    static let LiveEnviroment = "LIVE"
    
    
    static let kMessageInternalServer = "Internal Server Error"
    static let kMessageInvalidCredential = "Invalid Credentials"
    static let KInternetConnectionFaild = "No internet connection found. Please try again later"
    static let KInternetConnectionFaildTitle = "Internet Connection Issue"
    static let KLogoutMsg = "Are you sure you want to logout?"
    static let KPaymentFailedTitle = "Payment Failed. Please retry again"
    
    
    static let KBlankFiled = "Filed is blank"
    static let KEmailAddressIsNotValid = "Email address is not valid"
    static let KEmailAddressIsBlank = "Email address is blank"
    static let KPhoneNumberIsNotValid = "Phone Number is not valid"
    static let KPasswardNotMatch = "Password Not Matched"
    static let KPasswardNotValid = "Password Not valid"
    static let KOTPNotValid = "OTP Not valid"
    static let KNOTPurchased = "Please purchase the course"
    static let KAgree = "Please accept our terms & Conditions"
    static let KPincode = "Invalid Pincode"
    static let KProfileNotCompleted = "Please Complete your Profile"
    static let restorePurchase = "All previous purchases have been restored!"
    
    
    // MARK:SocialNetworking Key
    static let GoogleKey = "181308746478-agbi71hvmmq4m5jdu1rqb41svjrlb3u2.apps.googleusercontent.com"
    
    static let feature_img = "https://www.celebrityschool.in/images/artist_image/"
    static let a_thumb = "https://www.celebrityschool.in/images/album_image/"
    static let video_thumb = "https://www.celebrityschool.in/images/video_thumb/"
    static let shareMainUrl = "https://www.celebrityschool.in/"
    static let termsCondition = "https://www.celebrityschool.in/terms-and-conditions-new"
    static let privacypolicy = "https://www.celebrityschool.in/privacy-policy-new"

    static let socketURL = "https://chat.celebrityschool.in"
    
    //urls for api call
    struct EndUrls {
        static let signup_loginUrl = "user/mobile_login_signup"
        static let forgotPwdUrl = "user/reset_password_mobile/generateotp"
        static let verifyOTPUrl = "user/reset_password_mobile/verify_otp"
        static let allLessonUrl = "category/mobile_allLesson"
        static let albumVideoUrl = "video/album_video_lessons"
        static let fetchProfileUrl = "user/fetch_user_profile"
        static let updateProfileUrl = "user/update_profile"
        static let orderCreateUrl = "mobile_order/create_order_entry_ios"
        static let orderUpdateUrl = "mobile_order/update_order_entry_ios"
        static let ebookUrl = "ebook/mobile_app"
        static let purchasedlessons = "mobile_order/fetch_purchased_lessons"
        static let AddlatestVideoInfo  = "latest_video_mobile/set_latest_video"
        static let FetchlatestVideoInfo  = "latest_video_mobile/fetch_video_lesson"
        static let GetCertificate = "getCertificate/mobile_certificate"
        static let CouponCode = "mobile_order/verify_promocode"
        static let configUrl = "mconfig/con"
        static let compitionUrl = "competition/get-all-competition"
        static let ImageVideoUrl = "file/upload"
        static let compitionDetailFetch = "competition/create-user-details"
        static let CommunitylistUrl = "community/get-all"
    }
      
}
public enum Model : String {

    //Simulator
    case simulator     = "simulator/sandbox",

    //iPod
    iPod1              = "iPod 1",
    iPod2              = "iPod 2",
    iPod3              = "iPod 3",
    iPod4              = "iPod 4",
    iPod5              = "iPod 5",
    iPod6              = "iPod 6",
    iPod7              = "iPod 7",

    //iPad
    iPad2              = "iPad 2",
    iPad3              = "iPad 3",
    iPad4              = "iPad 4",
    iPadAir            = "iPad Air ",
    iPadAir2           = "iPad Air 2",
    iPadAir3           = "iPad Air 3",
    iPadAir4           = "iPad Air 4",
    iPad5              = "iPad 5", //iPad 2017
    iPad6              = "iPad 6", //iPad 2018
    iPad7              = "iPad 7", //iPad 2019
    iPad8              = "iPad 8", //iPad 2020
    iPad9              = "iPad 9", //iPad 2021

    //iPad Mini
    iPadMini           = "iPad Mini",
    iPadMini2          = "iPad Mini 2",
    iPadMini3          = "iPad Mini 3",
    iPadMini4          = "iPad Mini 4",
    iPadMini5          = "iPad Mini 5",
    iPadMini6          = "iPad Mini 6",

    //iPad Pro
    iPadPro9_7         = "iPad Pro 9.7\"",
    iPadPro10_5        = "iPad Pro 10.5\"",
    iPadPro11          = "iPad Pro 11\"",
    iPadPro2_11        = "iPad Pro 11\" 2nd gen",
    iPadPro3_11        = "iPad Pro 11\" 3rd gen",
    iPadPro12_9        = "iPad Pro 12.9\"",
    iPadPro2_12_9      = "iPad Pro 2 12.9\"",
    iPadPro3_12_9      = "iPad Pro 3 12.9\"",
    iPadPro4_12_9      = "iPad Pro 4 12.9\"",
    iPadPro5_12_9      = "iPad Pro 5 12.9\"",

    //iPhone
    iPhone4            = "iPhone 4",
    iPhone4S           = "iPhone 4S",
    iPhone5            = "iPhone 5",
    iPhone5S           = "iPhone 5S",
    iPhone5C           = "iPhone 5C",
    iPhone6            = "iPhone 6",
    iPhone6Plus        = "iPhone 6 Plus",
    iPhone6S           = "iPhone 6S",
    iPhone6SPlus       = "iPhone 6S Plus",
    iPhoneSE           = "iPhone SE",
    iPhone7            = "iPhone 7",
    iPhone7Plus        = "iPhone 7 Plus",
    iPhone8            = "iPhone 8",
    iPhone8Plus        = "iPhone 8 Plus",
    iPhoneX            = "iPhone X",
    iPhoneXS           = "iPhone XS",
    iPhoneXSMax        = "iPhone XS Max",
    iPhoneXR           = "iPhone XR",
    iPhone11           = "iPhone 11",
    iPhone11Pro        = "iPhone 11 Pro",
    iPhone11ProMax     = "iPhone 11 Pro Max",
    iPhoneSE2          = "iPhone SE 2nd gen",
    iPhone12Mini       = "iPhone 12 Mini",
    iPhone12           = "iPhone 12",
    iPhone12Pro        = "iPhone 12 Pro",
    iPhone12ProMax     = "iPhone 12 Pro Max",
    iPhone13Mini       = "iPhone 13 Mini",
    iPhone13           = "iPhone 13",
    iPhone13Pro        = "iPhone 13 Pro",
    iPhone13ProMax     = "iPhone 13 Pro Max",

    unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#
// MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {

var type: Model {
    var systemInfo = utsname()
    uname(&systemInfo)
    let modelCode = withUnsafePointer(to: &systemInfo.machine) {
        $0.withMemoryRebound(to: CChar.self, capacity: 1) {
            ptr in String.init(validatingUTF8: ptr)
        }
    }

    let modelMap : [String: Model] = [

        //Simulator
        "i386"      : .simulator,
        "x86_64"    : .simulator,

        //iPod
        "iPod1,1"   : .iPod1,
        "iPod2,1"   : .iPod2,
        "iPod3,1"   : .iPod3,
        "iPod4,1"   : .iPod4,
        "iPod5,1"   : .iPod5,
        "iPod7,1"   : .iPod6,
        "iPod9,1"   : .iPod7,

        //iPad
        "iPad2,1"   : .iPad2,
        "iPad2,2"   : .iPad2,
        "iPad2,3"   : .iPad2,
        "iPad2,4"   : .iPad2,
        "iPad3,1"   : .iPad3,
        "iPad3,2"   : .iPad3,
        "iPad3,3"   : .iPad3,
        "iPad3,4"   : .iPad4,
        "iPad3,5"   : .iPad4,
        "iPad3,6"   : .iPad4,
        "iPad6,11"  : .iPad5, //iPad 2017
        "iPad6,12"  : .iPad5,
        "iPad7,5"   : .iPad6, //iPad 2018
        "iPad7,6"   : .iPad6,
        "iPad7,11"  : .iPad7, //iPad 2019
        "iPad7,12"  : .iPad7,
        "iPad11,6"  : .iPad8, //iPad 2020
        "iPad11,7"  : .iPad8,
        "iPad12,1"  : .iPad9, //iPad 2021
        "iPad12,2"  : .iPad9,

        //iPad Mini
        "iPad2,5"   : .iPadMini,
        "iPad2,6"   : .iPadMini,
        "iPad2,7"   : .iPadMini,
        "iPad4,4"   : .iPadMini2,
        "iPad4,5"   : .iPadMini2,
        "iPad4,6"   : .iPadMini2,
        "iPad4,7"   : .iPadMini3,
        "iPad4,8"   : .iPadMini3,
        "iPad4,9"   : .iPadMini3,
        "iPad5,1"   : .iPadMini4,
        "iPad5,2"   : .iPadMini4,
        "iPad11,1"  : .iPadMini5,
        "iPad11,2"  : .iPadMini5,
        "iPad14,1"  : .iPadMini6,
        "iPad14,2"  : .iPadMini6,

        //iPad Pro
        "iPad6,3"   : .iPadPro9_7,
        "iPad6,4"   : .iPadPro9_7,
        "iPad7,3"   : .iPadPro10_5,
        "iPad7,4"   : .iPadPro10_5,
        "iPad6,7"   : .iPadPro12_9,
        "iPad6,8"   : .iPadPro12_9,
        "iPad7,1"   : .iPadPro2_12_9,
        "iPad7,2"   : .iPadPro2_12_9,
        "iPad8,1"   : .iPadPro11,
        "iPad8,2"   : .iPadPro11,
        "iPad8,3"   : .iPadPro11,
        "iPad8,4"   : .iPadPro11,
        "iPad8,9"   : .iPadPro2_11,
        "iPad8,10"  : .iPadPro2_11,
        "iPad13,4"  : .iPadPro3_11,
        "iPad13,5"  : .iPadPro3_11,
        "iPad13,6"  : .iPadPro3_11,
        "iPad13,7"  : .iPadPro3_11,
        "iPad8,5"   : .iPadPro3_12_9,
        "iPad8,6"   : .iPadPro3_12_9,
        "iPad8,7"   : .iPadPro3_12_9,
        "iPad8,8"   : .iPadPro3_12_9,
        "iPad8,11"  : .iPadPro4_12_9,
        "iPad8,12"  : .iPadPro4_12_9,
        "iPad13,8"  : .iPadPro5_12_9,
        "iPad13,9"  : .iPadPro5_12_9,
        "iPad13,10" : .iPadPro5_12_9,
        "iPad13,11" : .iPadPro5_12_9,

        //iPad Air
        "iPad4,1"   : .iPadAir,
        "iPad4,2"   : .iPadAir,
        "iPad4,3"   : .iPadAir,
        "iPad5,3"   : .iPadAir2,
        "iPad5,4"   : .iPadAir2,
        "iPad11,3"  : .iPadAir3,
        "iPad11,4"  : .iPadAir3,
        "iPad13,1"  : .iPadAir4,
        "iPad13,2"  : .iPadAir4,
        

        //iPhone
        "iPhone3,1" : .iPhone4,
        "iPhone3,2" : .iPhone4,
        "iPhone3,3" : .iPhone4,
        "iPhone4,1" : .iPhone4S,
        "iPhone5,1" : .iPhone5,
        "iPhone5,2" : .iPhone5,
        "iPhone5,3" : .iPhone5C,
        "iPhone5,4" : .iPhone5C,
        "iPhone6,1" : .iPhone5S,
        "iPhone6,2" : .iPhone5S,
        "iPhone7,1" : .iPhone6Plus,
        "iPhone7,2" : .iPhone6,
        "iPhone8,1" : .iPhone6S,
        "iPhone8,2" : .iPhone6SPlus,
        "iPhone8,4" : .iPhoneSE,
        "iPhone9,1" : .iPhone7,
        "iPhone9,3" : .iPhone7,
        "iPhone9,2" : .iPhone7Plus,
        "iPhone9,4" : .iPhone7Plus,
        "iPhone10,1" : .iPhone8,
        "iPhone10,4" : .iPhone8,
        "iPhone10,2" : .iPhone8Plus,
        "iPhone10,5" : .iPhone8Plus,
        "iPhone10,3" : .iPhoneX,
        "iPhone10,6" : .iPhoneX,
        "iPhone11,2" : .iPhoneXS,
        "iPhone11,4" : .iPhoneXSMax,
        "iPhone11,6" : .iPhoneXSMax,
        "iPhone11,8" : .iPhoneXR,
        "iPhone12,1" : .iPhone11,
        "iPhone12,3" : .iPhone11Pro,
        "iPhone12,5" : .iPhone11ProMax,
        "iPhone12,8" : .iPhoneSE2,
        "iPhone13,1" : .iPhone12Mini,
        "iPhone13,2" : .iPhone12,
        "iPhone13,3" : .iPhone12Pro,
        "iPhone13,4" : .iPhone12ProMax,
        "iPhone14,4" : .iPhone13Mini,
        "iPhone14,5" : .iPhone13,
        "iPhone14,2" : .iPhone13Pro,
        "iPhone14,3" : .iPhone13ProMax,
    ]

    guard let mcode = modelCode, let map = String(validatingUTF8: mcode), let model = modelMap[map] else { return Model.unrecognized }
    if model == .simulator {
        if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
            if let simMap = String(validatingUTF8: simModelCode), let simModel = modelMap[simMap] {
                return simModel
            }
        }
    }
    return model
    }
}
