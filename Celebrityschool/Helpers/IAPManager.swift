//
//  IAPManager.swift
//  Celebrityschool
//
//  Created by Hiren on 21/08/21.
//

import Foundation
import StoreKit

public class IAPManager: NSObject, SKProductsRequestDelegate,
                         SKPaymentTransactionObserver {
    
    enum IAPManagerError: Error {
        case noProductIDsFound
        case noProductsFound
        case paymentWasCancelled
        case productRequestFailed
        case somethingWentWrong
    }
    
    var onReceiveProductsHandler: ((Result<SKProduct, IAPManagerError>) -> Void)?
    

    // This is the initializer for your IAPManager class
    //
    // An alternative, and more scaleable way of doing this
    // is to also accept a callback in the initializer, and call
    // that callback in places like the paymentQueue function, and
    // in all functions in this class, in place of calls to functions
    // in RemoveAdsManager.
    static let shared = IAPManager()
    
    var orderCreate:OrderDataModel?
    var receData:[String:Any] = [:]
    var courseProductId:String = ""

    // Call this when you want to begin a purchase
    // for the productID you gave to the initializer
    func beginPurchase(withProductId productId:String, andWithHandler productsReceiveHandler: @escaping (_ result: Result<SKProduct, IAPManagerError>) -> Void){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        courseProductId = productId
        // Keep the handler (closure) that will be called when requesting for
        // products on the App Store is finished.
        onReceiveProductsHandler = productsReceiveHandler
        
        // If the user can make payments
        if SKPaymentQueue.canMakePayments(){
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.showProgressHUD()
            }
            
            // Create a new request
            let request = SKProductsRequest(productIdentifiers: [productId])
            request.delegate = self
            request.start()
        
        }
        else{
            // Otherwise, tell the user that
            // they are not authorized to make payments,
            // due to parental controls, etc
            faliureMsg(IAPManagerError.productRequestFailed.localizedDescription)
        }
    }

    // Call this when you want to restore all purchases
    // regardless of the productID you gave to the initializer
    public func beginRestorePurchases(){
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.hideProgressHUD()
        }
    }

    // This is called when a SKProductsRequest receives a response
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse){
        // Let's try to get the first product from the response
        // to the request
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.hideProgressHUD()
        }
        if let product = response.products.first{
            // We were able to get the product! Make a new payment
            // using this product
            let payment = SKPayment(product: product)
                // add the new payment to the queue
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().add(payment)
                onReceiveProductsHandler?(.success(product))
            
        }
        else{
            // Something went wrong! It is likely that either
            // the user doesn't have internet connection, or
            // your product ID is wrong!
            //
            // Tell the user in requestFailed() by sending an alert,
            // or something of the sort
            faliureMsg(IAPManagerError.noProductsFound.localizedDescription)

        }
    }

    // This is called when the user restores their IAP sucessfully
    public func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue){
        // For every transaction in the transaction queue...
        for transaction in queue.transactions{
            // If that transaction was restored
            if transaction.transactionState == .restored{
                // get the producted ID from the transaction
                courseProductId = transaction.payment.productIdentifier

                // finish the payment
                SKPaymentQueue.default().finishTransaction(transaction)
                DispatchQueue.main.async{
                    ProgressHUD.sharedInstance.hideProgressHUD()
                }
                
                if let topController = UIApplication.topViewController(){
                    AlertIndicator_Toast().showToast(Constants.restorePurchase, vc: topController)
                }
            }
        }
    }

    // This is called when the state of the IAP changes -- from purchasing to purchased, for example.
    // This is where the magic happens :)
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]){
        for transaction in transactions{
            // get the producted ID from the transaction
            courseProductId = transaction.payment.productIdentifier

            // In this case, we have only one IAP, so we don't need to check
            // what IAP it is.
            // However, if you have multiple IAPs, you'll need to use productID
            // to check what functions you should run here!

            switch transaction.transactionState{
            case .purchasing:
                // if the user is currently purchasing the IAP,
                // we don't need to do anything.
                //
                // You could use this to show the user
                // an activity indicator, or something like that
                DispatchQueue.main.async{
                    ProgressHUD.sharedInstance.showProgressHUD()
                }
                break
            case .purchased:
                // the user sucessfully purchased the IAP!
                DispatchQueue.main.async{
                    ProgressHUD.sharedInstance.hideProgressHUD()
                }
                SKPaymentQueue.default().finishTransaction(transaction)
                validateRecepiteCheck(transaction)
            case .restored:
                // the user restored their IAP!
                DispatchQueue.main.async{
                    ProgressHUD.sharedInstance.hideProgressHUD()
                }
                SKPaymentQueue.default().finishTransaction(transaction)
            case .failed:
                // The transaction failed!
                // finish the transaction
                DispatchQueue.main.async{
                    ProgressHUD.sharedInstance.hideProgressHUD()
                }
//                onReceiveProductsHandler?(.failure(.paymentWasCancelled))
                faliureMsg(IAPManagerError.paymentWasCancelled.localizedDescription)
                SKPaymentQueue.default().finishTransaction(transaction)
            case .deferred:
                // This happens when the IAP needs an external action
                // in order to proceeded, like Ask to Buy
                DispatchQueue.main.async{
                    ProgressHUD.sharedInstance.hideProgressHUD()
                }
                break
            @unknown default:
                fatalError()
            }
        }
    }
    
    func start() {
        SKPaymentQueue.default().add(self)
    }
    
    func faliureMsg(_ msg:String){
        ProgressHUD.sharedInstance.hideProgressHUD()
        let alert = UIAlertController(title: Constants.KPaymentFailedTitle, message: msg, defaultActionButtonTitle: "Ok", tintColor: .black)
        alert.show()
    }

}

// MARK: - IAPManagerError Localized Error Descriptions
extension IAPManager.IAPManagerError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .noProductIDsFound: return "No In-App Purchase product identifiers were found."
        case .noProductsFound: return "No In-App Purchases were found."
        case .productRequestFailed: return "Unable to fetch available In-App Purchase products at the moment."
        case .paymentWasCancelled: return "In-App Purchase was cancelled."
        case .somethingWentWrong: return "Something went wrong. Please try later"
        }
    }
}

extension IAPManager {
    
    func validateRecepiteCheck(_ transaction:SKPaymentTransaction){
        validateReceipt(appStoreReceiptURL: Bundle.main.appStoreReceiptURL) { status in
            if status{
                if !self.receData.isEmpty{
                    var param:[String:Any] = [:]
                    
                    if let receipt = self.receData["receipt"] as? [String:Any]{
                        if let in_app = receipt["in_app"] as? [[String:Any]]{
                            if let transaction_id = in_app[0]["transaction_id"]{
                                param["payment_id"] = transaction_id
                            }
                        }
                    }
                    
                    
                    param["txnid"] = self.orderCreate?.txnid
                    param["album_id"] = self.orderCreate?.album_id
                    param["enviroment"] = self.receData["environment"]

                    CourseApiServices().orderUpdateApiCall(param) { status, response in
                        if status{
                            if var data = response as? OrderUpdateModel{
                                var model = data.data
                                model?.amount = self.orderCreate?.amount
                                model?.course_name = self.orderCreate?.course_name
                                model?.orderAlbum_id = self.orderCreate?.album_id
                                data.data = model
                                CustomNavigationController().callSuccessFailureViewController(data, CouponModel(dictionary: [:]))
                            }
                        }
                    }
                }
            }
        }
    }
   
    
    
    private func receiptData(appStoreReceiptURL : URL?) -> Data? {

        guard let receiptURL = appStoreReceiptURL,
            let receipt = NSData(contentsOf: receiptURL as URL) else {
            return nil
        }

        do {
            let receiptData = receipt.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            var requestContents:[String:Any] = [:]
//            if courseProductId == "com.Celebrityschool.Course.0" || courseProductId == "com.Celebrityschool.Course.00"{
            requestContents = ["receipt-data" : receiptData, "password" : "44cc4169a8764219a8c33926392bdc76", "exclude-old-transactions" : false]
//            }else{
//                requestContents = ["receipt-data" : receiptData]
//            }
           
            let requestData = try JSONSerialization.data(withJSONObject: requestContents, options: [])
            return requestData
        }
        catch let error as NSError {
            print(error)
        }

        return nil
    }

    private func validateReceiptInternal(appStoreReceiptURL : URL?, isProd: Bool , onCompletion: @escaping (Int?) -> Void) {

        let serverURL = isProd ? "https://buy.itunes.apple.com/verifyReceipt" : "https://sandbox.itunes.apple.com/verifyReceipt"

        guard let receiptData = receiptData(appStoreReceiptURL: appStoreReceiptURL),
              let url = URL(string: serverURL)  else {
            onCompletion(nil)
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = receiptData
        
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }

        let task = URLSession.shared.dataTask(with: request, completionHandler: {data, response, error -> Void in
            
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }

            guard let data = data, error == nil else {
                onCompletion(nil)
                return
            }

            do {
                guard let json = try JSONSerialization.jsonObject(with: data, options:[]) as? [String:Any] else {
                    onCompletion(nil)
                    return
                }
                print(json)
                self.receData = json
                guard let statusCode = json["status"] as? Int else {
                    onCompletion(nil)
                    return
                }
                onCompletion(statusCode)
            }
            catch let error as NSError {
                print(error)
                onCompletion(nil)
            }
        })
        task.resume()
    }

    public func validateReceipt(appStoreReceiptURL : URL?, onCompletion: @escaping (Bool) -> Void) {

        validateReceiptInternal(appStoreReceiptURL: appStoreReceiptURL, isProd: true) { (statusCode: Int?) -> Void in
            guard let status = statusCode else {
                onCompletion(false)
                return
            }

            // This receipt is from the test environment, but it was sent to the production environment for verification.
            if status == 21007 {
                self.validateReceiptInternal(appStoreReceiptURL: appStoreReceiptURL, isProd: false) { (statusCode: Int?) -> Void in
                    guard let statusValue = statusCode else {
                        onCompletion(false)
                        return
                    }

                    // 0 if the receipt is valid
                    if statusValue == 0 {
                        onCompletion(true)
                    } else {
                        onCompletion(false)
                    }

                }

            // 0 if the receipt is valid
            } else if status == 0 {
                onCompletion(true)
            } else {
                onCompletion(false)
            }
        }
    }
}

