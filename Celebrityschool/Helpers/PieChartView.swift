//
//  PieChartView.swift
//  Celebrityschool
//
//  Created by Hiren on 28/08/21.
//

import UIKit

public class DoughnutView: UIView, CAAnimationDelegate {

    public var data: [Float]? {
        didSet { setNeedsDisplay() }
    }

    public var colors: [UIColor]? {
        didSet { setNeedsDisplay() }
    }

    @IBInspectable public var spacerWidth: CGFloat = 0 {
        didSet { setNeedsDisplay() }
    }

    @IBInspectable public var thickness: CGFloat = 70 {
        didSet { setNeedsDisplay() }
    }

    public override func draw(_ rect: CGRect) {
        guard
            let data = data, !data.isEmpty,
            let colors = colors, data.count == colors.count
            else { return }

        let center = CGPoint(x: bounds.size.width / 2.0, y: bounds.size.height / 2.0)
        let radius = min(bounds.size.width, bounds.size.height) / 2.0 - spacerWidth
        let total: CGFloat = data.reduce(0) { $0 + CGFloat($1) } / (2 * .pi)
        var startAngle = CGFloat.pi * 0.3

        UIColor.white.setStroke()
        
//        let animation = CABasicAnimation(keyPath: "strokeEnd")
//        animation.fromValue = 0
//        animation.toValue = 1
//        animation.duration = 0.5
//        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
//        animation.delegate = self
        

        for (color, value) in zip(colors, data) {
            let endAngle = startAngle + CGFloat(value) / total

            let slice = UIBezierPath()
            slice.addArc(withCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
            slice.addArc(withCenter: center, radius: radius - thickness, startAngle: endAngle, endAngle: startAngle, clockwise: false)
            slice.close()

            color.setFill()
            slice.fill()

            slice.lineWidth = spacerWidth
            slice.stroke()

            startAngle = endAngle
            
//            let sliceLayer = CAShapeLayer()
//                    sliceLayer.path = slice.cgPath
//                    sliceLayer.fillColor = nil
//                    sliceLayer.strokeColor = slice.color.cgColor
//                    sliceLayer.lineWidth = canvasWidth * 2 / 8
//                    sliceLayer.strokeEnd = 1
//                    sliceLayer.add(animation, forKey: animation.keyPath)
        }
        
        
        
        
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        self.backgroundColor = UIColor.clear
        setNeedsDisplay()
    }
}
