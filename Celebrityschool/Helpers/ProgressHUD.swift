//
//  ProgressHUD.swift
//  Celebrityschool
//
//  Created by Hiren on 04/08/21.
//

import UIKit
import MBProgressHUD

class ProgressHUD: NSObject {
    
    static let sharedInstance = ProgressHUD()
    
    override init() {
        super.init()
    }
    
    
    /// To show activity indicator
    func showProgressHUD() {
        if let topController = UIApplication.topViewController(){
            MBProgressHUD.showAdded(to: topController.view, animated: true)
        }
       
    }
    
    
    /// To Hide activity indicator
    func hideProgressHUD() {
        if let topController = UIApplication.topViewController(){
            MBProgressHUD.hide(for: topController.view, animated: true)
        }
    }
    
    func showProgressHUD(_ withMsg:String){ if let topController = UIApplication.topViewController(){
            let hud:MBProgressHUD = MBProgressHUD.showAdded(to: topController.view, animated: true)
            hud.label.text = withMsg
            hud.label.textAlignment = .center
            hud.label.textColor = .white
            hud.backgroundView.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }

}
