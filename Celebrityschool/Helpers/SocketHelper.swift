//
//  SocketHelper.swift
//  Celebrityschool
//
//  Created by Hiren on 08/02/22.
//

import UIKit
import Foundation
import SocketIO

//let kHost = "http://192.168.1.43:3001"
let kConnectUser = "Join"
let kUserList = "userList"
let kExitUser = "exitUser"

struct CustomData : SocketData {
   let communityId: String
   let communityType: String

   func socketRepresentation() -> SocketData {
       return ["communityId": communityId, "communityType": communityType]
   }
}

final class SocketHelper: NSObject {
    
    static let shared = SocketHelper()
    var sendSocketStatus : ((SocketIOStatus) -> Void)?
    var status: SocketIOStatus! {
        didSet {
            sendSocketStatus?(status)
        }
    }
    private var manager: SocketManager!
    private var socket: SocketIOClient!
    
    override init() {
        super.init()
        configureSocketClient()
    }
    
    private func configureSocketClient() {
        
        guard let url = URL(string: Constants.socketURL) else {
            return
        }
        
        manager = SocketManager(socketURL: url, config: [.path("/socket.io")])

    }
    
    func establishConnection() {
        
        guard let _socket = manager?.defaultSocket else{
            return
        }
        socket = _socket
        
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            self.status = .connected
            
        }

        socket.on(clientEvent: .error) {data, ack in
            print(data)
        }

        socket.on(clientEvent: .disconnect) {data, ack in
            print(data)
        }

        socket.connect()
    }
    
    func closeConnection() {
        
        guard let socket = manager?.defaultSocket else{
            return
        }
        
        socket.disconnect()
    }
    
    func checkConnection(){
        guard let socket = manager?.defaultSocket else{
            return
        }
        
        let socketConnectionStatus = socket.status
        
        switch socketConnectionStatus {
        case .connected:
           print("socket connected")
            status = socketConnectionStatus
        case .connecting:
           print("socket connecting")
        case .disconnected:
           print("socket disconnected")
        case .notConnected:
           print("socket not connected")
            socket.connect()
        }
    }
    
    func joinChatRoom(nickname: [String:String], completion: () -> Void) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        print("\(nickname)")
        print("\(socket.status)")
        socket.emit("join", nickname)
        completion()
    }
        
    func leaveChatRoom(nickname: [String:String], completion: () -> Void) {
        
        guard let socket = manager?.defaultSocket else{
            return
        }
        
        socket.emit(kExitUser, nickname)
        completion()
    }
    
    func getMessage(completion: @escaping (_ messageInfo: [Message]) -> Void) {
        
        guard let _socket = manager?.defaultSocket else {
            return
        }
        
        socket = _socket
        
        socket.on("message") { (dataArray, socketAck) -> Void in

            guard let object = dataArray.first as? NSMutableDictionary else{
                return
            }
            
            let addOnRes = socketModel(dictionary: object as! [String : Any])
            
            guard let mainObejctArray = addOnRes.message else{return}

            completion(mainObejctArray)
            
        }
    }
    
    func sendMessage(message: [String:String]) {
        
        guard let socket = manager?.defaultSocket else {
            return
        }
        
        print("sendMessage: \(message)")
        socket.emit("sendMessage",message)
    }
}
