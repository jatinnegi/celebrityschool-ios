//
//  CustomNavigationController+Extension.swift
//  Celebrityschool
//
//  Created by Hiren on 04/08/21.
//

import Foundation
import UIKit

extension CustomNavigationController{
    
    //MARK: SplashViewController
    func setSplashViewController(){
        let rootViewController = SplashViewController()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = navigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    //MARK: LoginViewController
    func setRootviewAsLandingViewController(){
        UserDefaults.standard.setLoggedIn(value: false)
        UserDefaults.standard.removeUserDefault(key: Constants.UserTokenKey)
        UserDefaults.standard.removeUserDefault(key: Constants.UserEmailKey)
        UserDefaults.standard.resetDefaults()

        let rootViewController = LandingViewController()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.setNavigationBarHidden(true, animated: false)
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = navigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    //MARK: TabBarViewController
    func setRootviewAsTabBarViewController(){

        let rootViewController = MainTabBarViewController()
        let navigationController = UINavigationController(rootViewController: rootViewController)
        navigationController.setNavigationBarHidden(true, animated: false)
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = navigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
    func callLoginViewController() {
        let rootViewController = LoginViewController()
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    
    func callSignupViewController() {
        let rootViewController = SignupViewController()
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    func callForgotPasswardViewController(){
        let rootViewController = ForgotPasswardViewController()
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    
    func callCourseDetailViewController(_ model:CourseModel){
        let rootViewController = CourseDetailViewController()
        rootViewController.albumInfo = model
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    func callPlayerViewController(_ albumModel:CourseModel,leassonModel:VideoLeassonModel?,index:Int = 0){
        let rootViewController = PlayerViewController()
        rootViewController.album_info = albumModel
        rootViewController.videoLeasson = leassonModel
        rootViewController.index = index
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    func callEbookViewController(_ leassonModel:VideoLeassonModel?, privateUrl:String){
        let rootViewController = WebViewController()
        rootViewController.videoModel = leassonModel
        rootViewController.urlString = privateUrl
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    
    func callShare(_ model: Any){
        var objectsToShare:[Any] = []
        if let data = model as?  CourseModel{
            var shareURL = Constants.shareMainUrl
            
            if let meta_title = data.meta_title {
                objectsToShare.append(meta_title)
            }
            
            if let category = data.category{
                objectsToShare.append(category)
            }
            
            if let slug = data.slug{
                shareURL =  shareURL + slug
            }

            if let link = URL(string: shareURL){
                objectsToShare.append(link)
            }
        }
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        if let topController = UIApplication.topViewController() {
            topController.present(activityVC, animated: true, completion: nil)
        }
        
    }
    
    
    func callSuccessFailureViewController(_ model:OrderUpdateModel,_ model2:CouponModel?){
        let rootViewController = SuccessFailurePopupViewController()
        rootViewController.OrderModel = model
        rootViewController.CouponModel = model2
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: false)
        }
    }
    
    
    func callCertificateViewController(_ model:[CourseModel],lockHide:Bool){
        let rootViewController = CertificatesViewController()
        rootViewController.courseData = model
        rootViewController.lockHide = lockHide
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: false)
        }
    }
    
    func CertificateDetailViewControllerCall(_ albumId:String){
        let rootViewController = CertificateDetailViewController()
        rootViewController.albumid = albumId
        rootViewController.modalPresentationStyle = .overFullScreen
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.present(rootViewController, animated: true, completion: nil)
        }
    }
    
    
    func callEditProfileViewController(){
        let rootViewController = EditProfileViewController()
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
    func callLogOutViewController(){
        let vc = LogoutPopViewController()
        vc.modalPresentationStyle = .overCurrentContext
        // Keep animated value as false
        // Custom Modal presentation animation will be handled in VC itself
        if let topController = UIApplication.shared.windows.first!.rootViewController{
            topController.present(vc, animated: true)
        }
    }
    
    
    /*func downloadImage(with urlString : String , imageCompletionHandler: @escaping (UIImage?) -> Void){
        guard let url = URL.init(string: urlString) else {
            return  imageCompletionHandler(nil)
        }
        let resource = ImageResource(downloadURL: url)
        
        KingfisherManager.shared.retrieveImage(with: resource, options: nil, progressBlock: nil) { result in
            switch result {
            case .success(let value):
                imageCompletionHandler(value.image)
            case .failure:
                imageCompletionHandler(nil)
            }
        }
    }*/
    
    func callNotificationViewController(){
        let rootViewController = NotificationViewController()
        if let topController = UIApplication.topViewController() {
            topController.navigationController?.pushViewController(rootViewController, animated: true)
        }
    }
    
}
