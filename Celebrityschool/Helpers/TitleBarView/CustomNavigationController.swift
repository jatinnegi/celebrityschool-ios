//
//  CustomNavigationController.swift
//  Celebrityschool
//
//  Created by Hiren on 31/07/21.
//

import Foundation
import UIKit

class CustomNavigationController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

//        let color = UIColor(red: 15 / 255, green: 15 / 255, blue: 15 / 255, alpha: 1.0)
        UINavigationBar.appearance().isTranslucent = true


        // Or, Set to new colour for just this navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]

        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        
//        self.navigationController?.navigationBar.layer.masksToBounds = false
//        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
//        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
//        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        self.navigationController?.navigationBar.layer.shadowRadius = 2

//        let image = UIImage(named: "logo")
//        let imageView = UIImageView(image: image)
//        imageView.contentMode = .scaleAspectFit
//        navigationItem.titleView = imageView
        
//        let firstImage = barImageView(imageName: "picture2")
//                let secondButton = barButton(imageName: "picture3", selector: #selector(secondButtonPressed))
//
//                navigationItem.rightBarButtonItems = [firstImage, secondButton]
    }
    

    private func imageView(imageName: String) -> UIImageView {
        let logo = UIImage(named: imageName)
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.image = logo
        logoImageView.widthAnchor.constraint(equalToConstant: 35).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 35).isActive = true
        return logoImageView
    }

    func barImageView(imageName: String) -> UIBarButtonItem {
        return UIBarButtonItem(customView: imageView(imageName: imageName))
    }

    func barButton(imageName: String, selector: Selector) -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.widthAnchor.constraint(equalToConstant: 35).isActive = true
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.addTarget(self, action: selector, for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    func backBarButton() -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "back_whiteicon"), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.widthAnchor.constraint(equalToConstant: 35).isActive = true
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.addTarget(self, action: #selector(self.onBackButtonPressed(_:)), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    func rightBarButton() -> UIBarButtonItem {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "close_whiteicon"), for: .normal)
        button.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        button.widthAnchor.constraint(equalToConstant: 35).isActive = true
        button.heightAnchor.constraint(equalToConstant: 35).isActive = true
        button.addTarget(self, action: #selector(self.onCloseButtonPressed(_:)), for: .touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    @objc func onBackButtonPressed(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onCloseButtonPressed(_ sender: Any){
        self.navigationController?.dismiss(animated: true)
    }

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
