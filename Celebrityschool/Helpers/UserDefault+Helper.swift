//
//  UserDefault+Helper.swift
//  Celebrityschool
//
//  Created by Hiren on 02/08/21.
//

import Foundation
import UIKit

private enum Defaults: String {
    case isLoggedIn
    case userID
}

extension UserDefaults{
    
    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        UserDefaults.standard.setValue(value, forKey: Defaults.isLoggedIn.rawValue)
        UserDefaults.standard.synchronize()
    }

    func isLoggedIn()-> Bool {
        return UserDefaults.standard.bool(forKey: Defaults.isLoggedIn.rawValue)
    }


    
    //******************* REMOVE NSUSER DEFAULT *******************
    func removeUserDefault(key:String) {
        UserDefaults.standard.removeObject(forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    
    //******************* SAVE STRING IN USER DEFAULT *******************
    func saveInDefault(value:Any,key:String) {
        UserDefaults.standard.setValue(value, forKey: key)
        UserDefaults.standard.synchronize()
     }
    
    
    //******************* FETCH STRING FROM USER DEFAULT *******************
    func fetchString(key:String)->AnyObject {
        if (UserDefaults.standard.object(forKey: key) != nil) {
            return UserDefaults.standard.value(forKey: key)! as AnyObject
        }
         else {
            return "" as AnyObject
        }
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    static let messagesKey = "mockMessages"
    
    // MARK: Mock Messages
    
    func setMockMessages(count: Int) {
        set(count, forKey: UserDefaults.messagesKey)
        synchronize()
    }
    
    func mockMessagesCount() -> Int {
        if let value = object(forKey: UserDefaults.messagesKey) as? Int {
            return value
        }
        return 20
    }
    
    static func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if isFirstLaunch {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
        return isFirstLaunch
    }
}
