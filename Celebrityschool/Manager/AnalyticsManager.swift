//
//  AnalyticsManager.swift
//  Celebrityschool
//
//  Created by Hiren on 01/12/21.
//

import UIKit
import Smartech


class AnalyticsManager: NSObject {
    
    static let sharedInstance = AnalyticsManager()
    
    override init() {
        super.init()
    }
    
    func SmarTechOnLoginORSignUP(_ param:[String : Any], model : AppData, type:String){
        
        let sign_up_channel:String = param["social_platform"] as? String ?? ""
        let login_type:String = param["login_type"] as? String ?? ""
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let sign_up_date = formatter.string(from: Date())
//        let sign_up_date = formatter.date(from: date)
        
        
        
        if login_type == "Login Successful."{
            
            let dictionary: [String : Any] = [
                "full_name" :"\(model.first_name ?? "") \(model.last_name ?? "")",
                "email": model.email ?? "",
                "sign_up_channel" : sign_up_channel,
            ]
            
            Smartech.sharedInstance().trackEvent("login_user", andPayload:dictionary)

        }else{
            
            let dictionary: [String : Any] = [
                "full_name" :"\(model.first_name ?? "") \(model.last_name ?? "")",
                "email": model.email ?? "",
                "sign_up_date" : sign_up_date,
                "sign_up_channel" : sign_up_channel,
                "mobile" : model.mobile ?? ""
            ]
            
            Smartech.sharedInstance().trackEvent("signup", andPayload:dictionary)
        }
        
        Smartech.sharedInstance().setUserIdentity(model.email ?? "")
        Smartech.sharedInstance().getUserIdentity()
        Smartech.sharedInstance().login(model.email ?? "")
  
    }
    
    
    func SmarTechOnFetchProfile(model : AppData){
        
        let profilePushDictionary: [String : Any] = [
            "full_name" :"\(model.first_name ?? "") \(model.last_name ?? "")",
            "mobile": model.mobile ?? "",
            "age" : model.age ?? "",
            "state" : model.state ?? "",
            "city" : model.city ?? "",
            "email" : model.email ?? "",
        ]
        
        Smartech.sharedInstance().updateUserProfile(profilePushDictionary)

    }
    
    
    func logoutAndClearUserIdentity(){
        Smartech.sharedInstance().logoutAndClearUserIdentity(false)
    }
    
    func SmarTechOnbuy_now( _ model: CourseModel){
        
        let payloadDictionary: [String : Any] = [
            "celebrity_name" : model.a_artist ?? "",
            "course_category" : model.category ?? "",
            "amount" : Int(model.a_price ?? "0") ?? 0
        ]

        Smartech.sharedInstance().trackEvent("buy_now", andPayload:payloadDictionary)

    }
    
    func SmartTechAllCourseBuy(_ data:[String:Any]){
        Smartech.sharedInstance().trackEvent("buy_now", andPayload:data)
    }
    
    
    func SmarTechOnProductPurchase( _ model: OrderUpdateDataModel){
        
        let fname = UserDefaults.standard.fetchString(key: "first_name") as? String ?? ""
        
        let lname = UserDefaults.standard.fetchString(key: "last_name") as? String ?? ""
            
           
        let mobile = UserDefaults.standard.fetchString(key: "mobile") as? String ?? ""
        
        let email = UserDefaults.standard.fetchString(key: Constants.UserEmailKey) as? String ?? ""

        var course_category = ""
        var celebrity_name = ""
        if let data = UserDefaults.standard.value(forKey: "albumData") as? [String:String]{
            celebrity_name = data["celebrity_name"] ?? ""
            course_category = data["course_category"] ?? ""
            UserDefaults.standard.removeObject(forKey: "albumData")
        }
        
        let payloadDictionary: [String : Any] = [
            "coursename" : model.course_name ?? "",
            "course_category":course_category,
            "celebrity_name": celebrity_name,
            "amount":Int(model.amount ?? "0") ?? 0,
            "full_name" :"\(fname) \(lname)",
            "mobile": mobile,
            "email":email
        ]
        
        Smartech.sharedInstance().trackEvent("Product Purchase", andPayload:payloadDictionary)
    }
    
    func SmartTechEvent(_ data:[String:Any], eventName:String){
        Smartech.sharedInstance().trackEvent(eventName, andPayload:data)
    }
    
}
