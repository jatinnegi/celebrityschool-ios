//
//  AppleSignInClient.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import Foundation
import AuthenticationServices


class AppleSignInClient: NSObject {
    var completionHandler: (_ fullname: String?, _ email: String?, _ token: String?) -> Void = { _, _, _ in }
    
    @available(iOS 13.0, *) // sign in with apple is not available below ios13
    
    @objc func handleAppleIdRequest(block: @escaping (_ fullname: String?, _ email: String?, _ token : String?) -> Void) {
        completionHandler = block
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()
        request.requestedScopes = [.fullName,.email]
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.performRequests()
    }
    
     @available(iOS 13.0, *)
    func getCredentialState(userId: String) {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: userId) {  (credentialState, error) in
             switch credentialState {
                case .authorized:
                    // The Apple ID credential is valid.
                    break
                case .revoked:
                    // The Apple ID credential is revoked.
                    break
             case .notFound: break
                    // No credential was found, so show the sign-in UI.
                default:
                    break
             }
        }
    }
    
    
}

@available(iOS 13.0, *)

extension AppleSignInClient : ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {

        print(error.localizedDescription)

    }
    
     func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
                    
            // Create an account as per your requirement

            let userIdentifier = appleIDCredential.user
            let fullname = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            if let identityTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding:  .utf8)
            {
                print("identityTokenString\(identityTokenString)")
                completionHandler(fullname?.givenName, email, identityTokenString)
            } else {
                completionHandler(fullname?.givenName, email, nil)

            }
            getCredentialState(userId: userIdentifier)
            
        }
        
    }
}
