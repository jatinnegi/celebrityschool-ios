//
//  ProfileManager.swift
//  Celebrityschool
//
//  Created by Hiren on 10/08/21.
//

import Foundation
class ProfileManager: NSObject {
    
    static let sharedInstance = ProfileManager()
    
    override init() {
        super.init()
    }
    
    func saveProfileData(_ model:AppData){
        UserDefaults.standard.saveInDefault(value: model.first_name ?? "", key: "first_name")
        UserDefaults.standard.saveInDefault(value: model.last_name ?? "", key: "last_name")
        UserDefaults.standard.saveInDefault(value: model.certificate_name ?? "", key: "certificate_name")
        if let profileImage = model.profilePic, profileImage != ""{
            UserDefaults.standard.saveInDefault(value:profileImage, key: "profilePic")
        }
        UserDefaults.standard.saveInDefault(value: model.mobile ?? "", key: "mobile")
        UserDefaults.standard.saveInDefault(value: model.pincode ?? "", key: "pincode")
        UserDefaults.standard.saveInDefault(value: model.city ?? "", key: "city")
        UserDefaults.standard.saveInDefault(value: model.state ?? "", key: "state")
        UserDefaults.standard.saveInDefault(value: model.age ?? "", key: "age")
        UserDefaults.standard.saveInDefault(value: model.profession ?? "", key: "profession")
        UserDefaults.standard.saveInDefault(value: model.about_me ?? "", key: "about_me")
        if model.userid != nil && model.userid != 0{
            UserDefaults.standard.saveInDefault(value: "\(model.userid ?? 0)", key: "UserId")
        }

    }
    
}
