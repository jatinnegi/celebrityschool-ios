//
//  RemoteNotificationManager.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import UIKit
import UserNotifications
import UserNotificationsUI
import SmartPush
import Smartech


//MARK: Register PushNotifications
extension AppDelegate:UNUserNotificationCenterDelegate{
    
    
    func checkPushRegisterOrNot(){
        let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
        if !isRegisteredForRemoteNotifications {
             // User is registered for notification
            self.registerForRNotifications(app)
        }
    }
   
    func registerForRNotifications(_ application: UIApplication){
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
         (granted, error) in
         //Parse errors and track state
            if granted {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        UserDefaults.standard.saveInDefault(value: deviceTokenString, key: Constants.UserDefaultsDeviceTokenKey)
        appDelegate.remoteNotificaionToken(deviceToken)
        print("DeviceTokenString==\(deviceTokenString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
        appDelegate.failToRegisterForRemoteNotifications(error)
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      SmartPush.sharedInstance().willPresentForegroundNotification(notification)
      completionHandler([.alert, .badge, .sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      SmartPush.sharedInstance().didReceive(response)
      completionHandler()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
       
        // Print full message.
        print(userInfo)
    }
    
    
    //MARK:- SmartechDelegate Method
    func handleDeeplinkAction(withURLString deeplinkURLString: String, andCustomPayload customPayload: [AnyHashable : Any]?) {
        print("Deeplink: \(deeplinkURLString)")
        if customPayload != nil {
            print("Custom Payload: \(customPayload!)")
        }
    }
}


class RemoteNotificationManager: NSObject {
    var userConversationThreadId: NSNumber = NSNumber.init(value: 0)
    var postActivityId: NSNumber = NSNumber.init(value: 0)
    
    public func showAlertAfterRemoteNotification(userInfo: Dictionary<String, Any>) {
        
    }
    
    public func resetNotificationTray(block: () -> ()) {
        
    }
}
