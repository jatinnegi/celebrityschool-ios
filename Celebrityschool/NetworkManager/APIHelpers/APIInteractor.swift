//
//  APIInteractor.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import UIKit

protocol APIInteractor:AnyObject {
    
    func putObject(request: Request,completion: @escaping CompletionHandler) -> Void
    
    func getObject(request: Request,completion: @escaping CompletionHandler) -> Void
    
    func postObject(request: Request,completion: @escaping CompletionHandler) -> Void
    
    func deleteObject(request: Request,completion: @escaping CompletionHandler) -> Void
    
    func multiPartObjectPost(request: Request,progress: ProgressBlock?, completion: @escaping CompletionHandler) -> Void
    
}
