//
//  APIInteractorProvider.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import UIKit

class APIInteractorProvider: NSObject {
    
    var shouldUseRealAPI: Bool = true
    
    static let sharedInstance = APIInteractorProvider()
    
    private override init() {
        super.init()
    }
    
    public func getAPIInetractor() -> APIInteractor {
        if shouldUseRealAPI {
            return RealAPI() as APIInteractor
        } else {
            return TestAPI() as APIInteractor
        }
    }
}
