//
//  TestAPI.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import UIKit

class TestAPI: NSObject, APIInteractor {
    
    //MARK: - AFNetworking
    
    func getObject(request: Request,completion: @escaping CompletionHandler) {}
    
    func postObject(request: Request,completion: @escaping CompletionHandler) {}
    
    func putObject(request: Request,completion: @escaping CompletionHandler) {}
    
    func deleteObject(request: Request,completion: @escaping CompletionHandler) {}
    
    func multiPartObjectPost(request: Request,progress: ProgressBlock?, completion: @escaping CompletionHandler) -> Void {}
    
}
