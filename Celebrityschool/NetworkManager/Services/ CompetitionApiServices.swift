//
//   CompetitionApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

//import UIKit
import UIKit
import Alamofire

class CompetitionApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: (([CompetitionDataModel]) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    var didReceiveDataImageVideo: ((Bool) -> Void)?
    var didReceiveStudentData: ((Bool) -> Void)?
    
    var didReceiveProfileData: ((ParticipateModel) -> Void)?
    
    init() {}

    func CompetitionApiCall(_ progress:Bool){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.parameters = [:]
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.compitionUrl)
        
        print("Reuest==\(reuest)")
        if progress{
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.showProgressHUD()
            }
        }
        
        apiCall.postObject(request:reuest) { [weak self] status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = CompetitionModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data, data.count > 0{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
    
    func imageViewUpload(_ fileData:Data,category:String,type:String){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
       
        let finalURL = "https://fileservice.celebrityschool.in/v1/file/upload"
        var imageName = "file.jpg"
        if type == "video/mp4"{
            imageName = "file.mp4"
        }

        let parameters = ["category": category,"image":imageName] //Optional for extra parameter
        
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }

        AF.upload(multipartFormData: { MultipartFormData in

                MultipartFormData.append(fileData, withName: "image" , fileName:  imageName, mimeType: type)
                    for(key,value) in parameters {
                        MultipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                }, to: finalURL, method: .post, headers: ["x-auth-token": UserDefaults.standard.fetchString(key: Constants.UserTokenKey) as? String ?? "" ,"Content-type": "multipart/form-data"])
            .responseJSON { response in switch response.result {
                
                   case .success(let JSON):
                       print("Success with JSON: \(JSON)")
                           if let responseDictionary = JSON as? [String: Any]{
                               
                               print("Response==\(responseDictionary)")
                               let addOnRes = ParticipateModel(dictionary: responseDictionary)
                               if let meta = addOnRes.meta{
                                   if category != "profile"{
                                       if let topController = UIApplication.topViewController(){
                                           AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                                       }
                                   }
                               }
                               if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                                   self.didReceiveDataImageVideo?(true)
                                   if category == "profile"{
                                       self.didReceiveProfileData?(addOnRes)
                                   }
                               }else{
                                   self.didReceiveDataImageVideo?(false)
                               }
                               
                           }
                   case .failure(let error):
                       print("Request failed with error: \(error)")
                   }
                
                DispatchQueue.main.async{
                    ProgressHUD.sharedInstance.hideProgressHUD()
                }
           }
    }
    
    func StudentApiCall(_ param:[String:Any]){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.compitionDetailFetch)
        
        print("Reuest==\(reuest)")
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        
        apiCall.postObject(request:reuest) { [weak self] status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let metadata = responseDictionary["meta"] as? [String: Any]
                    if let meta = metadata{
                        if let topController = UIApplication.topViewController(){
                            AlertIndicator_Toast().showToast(meta["message"] as? String ?? "", vc: topController)
                        }
                    }
                    
                    if let meta = metadata, let statuscode = meta["statuscode"] as? Int , statuscode == 200{
                        strongSelf.didReceiveStudentData?(true)
                    }else{
                        strongSelf.didReceiveStudentData?(false)
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }


}
