//
//  AppConfigApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 15/01/22.
//

import UIKit
import Alamofire

class AppConfigApiServices {
    
    var didReceiveData: ((AppConfigDataModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {}
    
    func AppConfigApiCall(){
        
        if !Reachability.isConnectedToNetwork() {
            DispatchQueue.main.async{
                self.noDataRecieved?()
            }
            return
        }
        
        let urlString = "https://chat.celebrityschool.in/v1/feature/get-app-config"
    
        
        
        AF.request(URL.init(string: urlString)!, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: nil).responseJSON {[weak self] response in
            guard let strongSelf = self else {return}
              switch response.result {
                case .success(let JSON):
                  print(JSON)
                  if let responseDictionary = JSON as? [String: Any]{
                      let addOnRes = AppConfigModel(dictionary: responseDictionary)
                      if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                          if let modelData = addOnRes.data{
                              if let data = responseDictionary["data"]{
                                  UserDefaults.standard.saveInDefault(value:data, key: "AppConfig")
                              }
                              strongSelf.didReceiveData?(modelData)
                          }else{
                              strongSelf.noDataRecieved?()
                          }
                      }else{
                          strongSelf.noDataRecieved?()
                      }
                  }else{
                      strongSelf.noDataRecieved?()
                  }
                break
                case .failure(let error):
                  strongSelf.noDataRecieved?()
                  print(error)
              }
        }
    }
}
    
