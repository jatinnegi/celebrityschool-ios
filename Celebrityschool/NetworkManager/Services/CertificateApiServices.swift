//
//  CertificateApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 15/09/21.
//

import UIKit

class CertificateApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((CertificateGetModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {}

    func fetchCertificateApiCall(_ albumId:String){
        
        reuest.parameters = ["album_id":albumId]
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.GetCertificate)")
        
        print("Reuest==\(reuest)")
        
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        
        apiCall.postObject(request:reuest) { [weak self] status, response in

            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = CertificateModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
}

