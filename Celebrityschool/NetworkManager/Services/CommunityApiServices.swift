//
//  CommunityApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 22/11/21.
//

import UIKit

class CommunityApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((CommunityDataModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {
    }

    func CommunityApiCall(){
        
        if !Reachability.isConnectedToNetwork() {
            return
        }
        
        
        reuest.parameters = [:]
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.CommunitylistUrl)
        
        print("Reuest==\(reuest)")
        
        
        apiCall.getObject(request:reuest) { [weak self] status, response in
            
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = CommunityModel(dictionary: responseDictionary)

                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            UserDefaults.standard.saveInDefault(value: "\(data.userId ?? 0)", key: "UserId")
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
    
}
