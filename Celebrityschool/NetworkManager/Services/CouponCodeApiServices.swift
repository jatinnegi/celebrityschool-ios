//
//  CouponCodeApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 08/10/21.
//


import UIKit

class CouponCodeApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((CouponModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {}

    func coponCodeApiCall(_ param:[String:Any]){
        
        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.CouponCode)")
        
        print("Reuest==\(reuest)")
        
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        
        apiCall.postObject(request:reuest) { [weak self] status, response in

            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = CouponModel(dictionary: responseDictionary)
                    var data2 = OrderUpdateModel(dictionary: [:])
                    data2.couponCode = true
                    CustomNavigationController().callSuccessFailureViewController(data2, addOnRes)
//                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
//                        strongSelf.didReceiveData?(addOnRes)
//                    }else{
//                        if let meta = addOnRes.meta{
//                            if let topController = UIApplication.topViewController(){
//                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
//                            }
//                        }
//                        strongSelf.noDataRecieved?()
//                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
}
