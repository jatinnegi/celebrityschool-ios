//
//  CourseApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 14/08/21.
//

import UIKit

class CourseApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((AlbumDataModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {}

    func CourseApiCall(_ progress:Bool,_ albumId:Int){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.parameters = [:]
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.albumVideoUrl)/\(albumId)")
        
        print("Reuest==\(reuest)")
        if progress{
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.showProgressHUD()
            }
        }
        
        apiCall.getObject(request:reuest) { [weak self] status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = AlbumInfoModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
    
    
    func orderCreateApiCall(_ param:[String:Any],completion: @escaping CompletionHandler) -> Void{
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.orderCreateUrl)")
        
        print("Reuest==\(reuest)")
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        
        apiCall.postObject(request:reuest) {status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = OrderModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                        
                        if let data = addOnRes.data{
                            completion(true,data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func orderUpdateApiCall(_ param:[String:Any],completion: @escaping CompletionHandler) -> Void{
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.orderUpdateUrl)")
        
        print("Reuest==\(reuest)")
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        
        apiCall.postObject(request:reuest) {status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = OrderUpdateModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                        if let topController = UIApplication.topViewController(){
                            AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            completion(true,addOnRes)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func productBuyCall(_ plan_id:String, a_price:String, product_id: String){
        
        var param:[String:Any] = [:]
        param["album_id"] = "\(plan_id)"
        param["album_amount"] = a_price
        CourseApiServices().orderCreateApiCall(param) { status, response in
            
            if status{
                IAPManager.shared.orderCreate = response as? OrderDataModel
                IAPManager.shared.beginPurchase(withProductId: product_id) { result in
                    switch result {
                    case .success(_): break
                    case .failure(_): break
                    }
                }
            }
            
        }
    }
}
