//
//  CoursePurchaseApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 12/09/21.
//

import UIKit

class CoursePurchaseApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((PurchaseModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {}

    func PurchasedCourseApiCall(){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.purchasedlessons)")
        
        print("Reuest==\(reuest)")
//        DispatchQueue.main.async{
//            ProgressHUD.sharedInstance.showProgressHUD()
//        }
        
        apiCall.postObject(request:reuest) { [weak self] status, response in
            
//            DispatchQueue.main.async{
//                ProgressHUD.sharedInstance.hideProgressHUD()
//            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = PurchaseCourseModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
}
