//
//  EbookApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 05/09/21.
//


import UIKit

class EbookApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((EbookDataModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {}

    func EbookApiCall(_ ebookId:String){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.parameters = ["ebook_id":ebookId]
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.ebookUrl)")
        
        print("Reuest==\(reuest)")
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        
        apiCall.postObject(request:reuest) { [weak self] status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = EbookModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data?.ebook{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
}
