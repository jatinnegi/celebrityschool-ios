//
//  HomeApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 07/08/21.
//

import UIKit

class HomeApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((HomeDataModel) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {
        configAPICall()
    }

    func HomeApiCall(_ progress:Bool){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        
        reuest.parameters = [:]
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.allLessonUrl)
        
        print("Reuest==\(reuest)")
        if progress{
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.showProgressHUD()
            }
        }
        
        apiCall.getObject(request:reuest) { [weak self] status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = HomeModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
    
    
    func configAPICall(){
        
        reuest.parameters = [:]
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.configUrl)
        
        
        apiCall.getObject(request:reuest) { [weak self] status, response in
            

            guard self != nil else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = configModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                        if let data = addOnRes.data{
                            if let active = data.is_active, active == 0{
                                UserDefaults.standard.saveInDefault(value: false, key: "is_active")
                            }else{
                                UserDefaults.standard.saveInDefault(value: true, key: "is_active")

                            }
                        }else{
                            UserDefaults.standard.saveInDefault(value: true, key: "is_active")
                        }
                    }else{
                        UserDefaults.standard.saveInDefault(value: true, key: "is_active")
                    }
                }
            }else{
                UserDefaults.standard.saveInDefault(value: true, key: "is_active")
            }
        }
        
    }
}
