//
//  LoginApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import UIKit

class LoginApiServices: NSObject {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    func loginAPICall(_ param:[String: Any]){

        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.signup_loginUrl)
        
        print("Reuest==\(reuest)")
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        apiCall.postObject(request:reuest) {status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    
                    let addOnRes = AppResponseModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                        if let topController = UIApplication.topViewController(){
                            AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                        }
                        
                        if let data = addOnRes.data{
                            ProfileManager.sharedInstance.saveProfileData(data)
                            AnalyticsManager.sharedInstance.SmarTechOnLoginORSignUP(param, model: data, type: meta.message ?? "")
//                            AnalyticsManager.sharedInstance.SmarTechOnFetchProfile(model: data)
                            UserDefaults.standard.saveInDefault(value: data.token ?? "", key: Constants.UserTokenKey)
                            UserDefaults.standard.saveInDefault(value: data.email ?? "", key: Constants.UserEmailKey)
                        }
                        
                        UserDefaults.standard.setLoggedIn(value: true)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            CustomNavigationController().setRootviewAsTabBarViewController()
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            appDelegate.checkPushRegisterOrNot()
                        }
                        
                        print("Email==\(UserDefaults.standard.fetchString(key: Constants.UserEmailKey) as? String ?? "")")
                        print("Token==\(UserDefaults.standard.fetchString(key: Constants.UserTokenKey) as? String ?? "")")
                        
                        
                        
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                    }
                }
           }
        }
    }
    
    func forgotPwdAPICall(_ param:[String: Any],completion: @escaping CompletionHandler) -> Void{
        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.forgotPwdUrl)
        
        print("Reuest==\(reuest)")
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        apiCall.postObject(request:reuest) {status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = AppResponseModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                        if let topController = UIApplication.topViewController(){
                            AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                        }
                        completion(true, [:])
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        completion(false, [:])
                    }
                }
            }else{
                completion(false, [:])
            }
        }
    }
    
    
    func verifyAPICall(_ param:[String: Any],completion: @escaping CompletionHandler) -> Void{
        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.verifyOTPUrl)
        
        print("Reuest==\(reuest)")
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
        apiCall.postObject(request:reuest) {status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = AppResponseModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                        if let topController = UIApplication.topViewController(){
                            AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                        }
                        completion(true, [:])
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        completion(false, [:])
                    }
                }
            }else{
                completion(false, [:])
            }
        }
    }
}
