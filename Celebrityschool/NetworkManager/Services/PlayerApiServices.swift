//
//  PlayerApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 14/09/21.
//


import UIKit

class PlayerApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var isRefreshing: ((Bool) -> Void)?
    var didReceiveData: ((FetchVideoLessonModel) -> Void)?
    var addPlayerInfo: ((Bool) -> Void)?
    var noDataRecieved: (() -> Void)?
    
    init() {}

    func fetchVideoLessonApiCall(_ slug:String,videoIndex:String){
        
        reuest.parameters = ["slug":slug,"video_index":videoIndex]
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.FetchlatestVideoInfo)")
        
        print("Reuest==\(reuest)")

        apiCall.postObject(request:reuest) { [weak self] status, response in

            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = PlayerModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }
        }
    }
    
    
    
    
    func addVideoLessonApiCall(_ param:[String:Any]){
        
        reuest.parameters = param
        reuest.urlPath = Request.getUrl(path:"\(Constants.EndUrls.AddlatestVideoInfo)")
        
        print("Reuest==\(reuest)")

        apiCall.postObject(request:reuest) { [weak self] status, response in

            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = PlayerModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{
                        strongSelf.addPlayerInfo?(true)
                    }else{
                        strongSelf.addPlayerInfo?(false)
                    }
                }
            }
        }
    }
}
