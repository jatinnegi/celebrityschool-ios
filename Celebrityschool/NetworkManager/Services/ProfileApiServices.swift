//
//  ProfileApiServices.swift
//  Celebrityschool
//
//  Created by Hiren on 10/08/21.
//

import UIKit

class ProfileApiServices {
    
    let apiCall = RealAPI()
    let reuest = Request()
    
    var didReceiveData: ((AppData) -> Void)?
    var noDataRecieved: (() -> Void)?
    var updateProfile: ((Bool) -> Void)?

    init() {}
    
    
    func fetchProfileApiCall(){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.fetchProfileUrl)
        
        print("Reuest==\(reuest)")
        
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
       
        apiCall.getObject(request:reuest) { [weak self] status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = AppResponseModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            strongSelf.didReceiveData?(data)
                        }
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                    }
                }
            }else{
                strongSelf.noDataRecieved?()
            }

        }
    }
    
    func updateProfileApiCall(_ dict:[String:Any]){
        
        if !Reachability.isConnectedToNetwork() {
            let alert = UIAlertController(title: Constants.KInternetConnectionFaildTitle, message: Constants.KInternetConnectionFaild, defaultActionButtonTitle: "Ok", tintColor: .black)
            alert.show()
            return
        }
        
        reuest.parameters = dict
        reuest.urlPath = Request.getUrl(path:Constants.EndUrls.updateProfileUrl)
        
        print("Reuest==\(reuest)")
        
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.showProgressHUD()
        }
       
        apiCall.postObject(request:reuest) { [weak self] status, response in
            
            DispatchQueue.main.async{
                ProgressHUD.sharedInstance.hideProgressHUD()
            }
            guard let strongSelf = self else {return}
            if status == true{
                
                if let responseDictionary = response as? [String: Any]{
                    
                    print("Response==\(responseDictionary)")
                    let addOnRes = AppResponseModel(dictionary: responseDictionary)
                    if let meta = addOnRes.meta,let statuscode = meta.statuscode , statuscode == 200{

                        if let data = addOnRes.data{
                            ProfileManager.sharedInstance.saveProfileData(data)
                        }
                        
                        if let topController = UIApplication.topViewController(){
                            AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                        }
                        
                        strongSelf.updateProfile?(true)
                        
                    }else{
                        if let meta = addOnRes.meta{
                            if let topController = UIApplication.topViewController(){
                                AlertIndicator_Toast().showToast(meta.message ?? "", vc: topController)
                            }
                        }
                        strongSelf.noDataRecieved?()
                        strongSelf.updateProfile?(false)
                    }
                }
            }
        }
    }
}
