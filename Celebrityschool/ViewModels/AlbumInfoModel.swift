//
//  AlbumInfoModel.swift
//  Celebrityschool
//
//  Created by Hiren on 16/08/21.
//

import Foundation

struct AlbumInfoModel {
    var data: AlbumDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = AlbumDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct AlbumDataModel{
    
    var album_info: CourseModel?
    var is_course_purchased: CoursePurchasedModel?
    var video_lessons: [VideoLeassonModel]?
    var students_rating:studentsRatingModel?
    
    init(dictionary: [String: Any]) {
        album_info = CourseModel(dictionary: dictionary["album_info"] as? [String: Any] ?? [:])
        is_course_purchased = CoursePurchasedModel(dictionary: dictionary["is_course_purchased"] as? [String: Any] ?? [:])
        students_rating = studentsRatingModel(dictionary: dictionary["students_rating"] as? [String: Any] ?? [:])
        
        if let arrayleassons = dictionary["video_lessons"] as? NSArray,arrayleassons.count > 0{
            video_lessons = modelsFromDictionaryArray(array: arrayleassons)
        }
    }
    
    func modelsFromDictionaryArray(array:NSArray) -> [VideoLeassonModel]
    {
        var models:[VideoLeassonModel] = []
        for item in array
        {
            models.append(VideoLeassonModel(dictionary: item as! [String: Any]))
        }
        return models
    }
}

struct VideoLeassonModel{
    var id: Int?
    var album_id: Int?
    var video_title: String?
    var video_url: String?
    var video_thumb: String?
    var premium: String?
    var description: String?
    var video_place: String?
    var status:Int?
    
    init(dictionary: [String: Any]) {
        id = dictionary["id"] as? Int
        album_id = dictionary["album_id"] as? Int
        video_title = dictionary["video_title"] as? String
        video_url = dictionary["video_url"] as? String
        video_thumb = dictionary["video_thumb"] as? String
        premium = dictionary["premium"] as? String
        description = dictionary["description"] as? String
        video_place = dictionary["video_place"] as? String
        status = dictionary["status"] as? Int
    }
}

struct CoursePurchasedModel{
    var Credit: Bool?
    var product_id: String?
    var apple_id: String?
   
    
    init(dictionary: [String: Any]) {
        Credit = dictionary["Credit"] as? Bool
        product_id = dictionary["product_id"] as? String
        apple_id = dictionary["apple_id"] as? String
    }
}

struct studentsRatingModel{
    var message: String?
    var student_count: String?
    var rating: String?
    var total_rating_count: String?
    
    init(dictionary: [String: Any]) {
        message = dictionary["message"] as? String
        student_count = dictionary["student_count"] as? String
        rating = dictionary["rating"] as? String
        total_rating_count = dictionary["total_rating_count"] as? String

    }
}

