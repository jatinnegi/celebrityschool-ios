//
//  AppConfigModel.swift
//  Celebrityschool
//
//  Created by Hiren on 15/01/22.
//

import Foundation

struct AppConfigModel {
    var data: AppConfigDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = AppConfigDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct AppConfigDataModel{
    var chatFeature: Bool = false
    var notificationCenter: Bool = false
    var communityModule: Bool = false
    var competition: Bool = false
    var profile: Bool = false
    var restorePurchase:Bool = false
    var couponCode:Bool = false
    var createdAt: String?
    var updatedAt: String?
   
    init(dictionary: [String: Any]) {
        chatFeature = dictionary["chatFeature"] as? Bool ?? false
        notificationCenter = dictionary["notificationCenter"] as? Bool ?? false
        communityModule = dictionary["communityModule"] as? Bool ?? false
        competition = dictionary["competition"] as? Bool ?? false
        profile = dictionary["profile"] as? Bool ?? false
        restorePurchase = dictionary["restorePurchase"] as? Bool ?? false
        couponCode = dictionary["couponCode"] as? Bool ?? false
        updatedAt = dictionary["updatedAt"] as? String
        createdAt = dictionary["createdAt"] as? String
    }

}

