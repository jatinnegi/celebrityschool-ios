//
//  AppSettings.swift
//  Celebrityschool
//
//  Created by Hiren on 28/07/21.

import UIKit
//import JSONModel

class AppSettings: NSObject {
    var NetworkMode: String = ""
    var LocalURL: String = ""
    var ProductionURL: String = ""
    var ProductionURL2: String = ""
    var StagingURL: String = ""
    var URLPathSubstring: String = ""
    var URLPathSubstring2: String = ""
    var EnableSecureConnection: Bool = false
    var EnablePullToRefresh: Bool = false
    var EnableBanner: Bool = false
    var EnableCoreData: Bool = false
    var EnableTwitter: Bool = false
    var EnableFacebook: Bool = false
    
    override init() {
        super.init()
    }
    
    convenience init(dictionary: [String: Any]) {
        self.init()
        NetworkMode = dictionary.getString(forKey: "NetworkMode")
        LocalURL = dictionary.getString(forKey: "LocalURL")
        ProductionURL = dictionary.getString(forKey: "ProductionURL")
        ProductionURL2 = dictionary.getString(forKey: "ProductionURL2")
        StagingURL = dictionary.getString(forKey: "StagingURL")
        URLPathSubstring = dictionary.getString(forKey: "URLPathSubstring")
        URLPathSubstring2 = dictionary.getString(forKey: "URLPathSubstring2")
        EnableSecureConnection = dictionary.getBool(forKey: "EnableSecureConnection")
        EnablePullToRefresh = dictionary.getBool(forKey: "EnablePullToRefresh")
        
    }
}
