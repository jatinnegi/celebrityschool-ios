//
//  CommunityModel.swift
//  Celebrityschool
//
//  Created by Hiren on 04/12/21.
//

import Foundation

struct CommunityModel {
    var data: CommunityDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = CommunityDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct CommunityDataModel{
    var userId:Int?
    var allCategory:[CommunityCategoryModel]?
    
    init(dictionary: [String: Any]) {
        userId = dictionary["userId"] as? Int
        if let arrayleassons = dictionary["allCategory"] as? NSArray,arrayleassons.count > 0{
            allCategory = modelsFromDictionaryArray(array: arrayleassons)
        }
    }
    
    func modelsFromDictionaryArray(array:NSArray) -> [CommunityCategoryModel]
    {
        var models:[CommunityCategoryModel] = []
        for item in array
        {
            models.append(CommunityCategoryModel(dictionary: item as! [String: Any]))
        }
        return models
    }
}

struct CommunityCategoryModel {
    var image: String?
    var communityType: String?
    var artist: String?
    var id: Int?
    var lastmessage: lastmessageModel?
    
    init(dictionary: [String: Any]) {
        image = dictionary["image"] as? String
        communityType = dictionary["communityType"] as? String
        artist = dictionary["artist"] as? String
        id = dictionary["id"] as? Int
        lastmessage = lastmessageModel(dictionary: dictionary["lastmessage"] as? [String: Any] ?? [:])
    }
}

struct lastmessageModel {
    var communityId: Int?
    var userId: Int?
    var createdAt: String?
    var communityType: String?
    var expire_at: String?
    var message: String?
    var updatedAt: String?
    var userName: String?
    
    init(dictionary: [String: Any]) {
        communityId = dictionary["communityId"] as? Int
        userId = dictionary["userId"] as? Int
        createdAt = dictionary["createdAt"] as? String
        communityType = dictionary["communityType"] as? String
        expire_at = dictionary["expire_at"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        message = dictionary["message"] as? String
        userName = dictionary["userName"] as? String
    }

}
