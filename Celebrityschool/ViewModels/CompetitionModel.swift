//
//  CompetitionModel.swift
//  Celebrityschool
//
//  Created by Hiren on 13/01/22.
//

import Foundation

struct CompetitionModel {
    var data: [CompetitionDataModel]?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        if let arrayCourse = dictionary["data"] as? NSArray,arrayCourse.count > 0{
            data = modelsFromDictionaryArray(array: arrayCourse)
        }
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct CompetitionDataModel{
    var title: String?
    var competition_id: Int?
    var main_banner: String?
    var banner: String?
    var card: String?
    var description: String?
    var prizes: String?
    var rules: String?
    var how_to_participate: String?
    var is_paid: Bool? = false
    var is_participated: Bool? = false
    var upload_type: String?
    var category:String?
    var apple_id:String?
    var product_id:String?
    var amount_ios:Int?
   
    init(dictionary: [String: Any]) {
        title = dictionary["title"] as? String
        competition_id = dictionary["competition_id"] as? Int
        main_banner = dictionary["main_banner"] as? String
        banner = dictionary["banner"] as? String
        card = dictionary["card"] as? String
        description = dictionary["description"] as? String
        prizes = dictionary["prizes"] as? String
        rules = dictionary["rules"] as? String
        how_to_participate = dictionary["how_to_participate"] as? String
        is_paid = dictionary["is_paid"] as? Bool
        is_participated = dictionary["is_participated"] as? Bool
        upload_type = dictionary["upload_type"] as? String
        category = dictionary["category"] as? String
        apple_id = dictionary["apple_id"] as? String
        product_id = dictionary["product_id"] as? String
        amount_ios = dictionary["amount_ios"] as? Int
    }
}

func modelsFromDictionaryArray(array:NSArray) -> [CompetitionDataModel]
{
    var models:[CompetitionDataModel] = []
    for item in array
    {
        models.append(CompetitionDataModel(dictionary: item as! [String: Any]))
    }
    return models
}

struct ParticipateModel {
    var data: ParticipateDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = ParticipateDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct ParticipateDataModel{
    var _id: String?
    var userId: Int?
    var userEmail: String?
    var imageId: String?
    var fileUrl: String?
    var category: String?
    var fileType: String?
    var createdAt: String?
    var updatedAt: String?
    var __v: Int?
   
    init(dictionary: [String: Any]) {
        _id = dictionary["_id"] as? String
        userId = dictionary["userId"] as? Int
        userEmail = dictionary["userEmail"] as? String
        imageId = dictionary["imageId"] as? String
        fileUrl = dictionary["fileUrl"] as? String
        category = dictionary["category"] as? String
        fileType = dictionary["fileType"] as? String
        createdAt = dictionary["createdAt"] as? String
        __v = dictionary["__v"] as? Int
    }
}

