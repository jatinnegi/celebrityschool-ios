//
//  CouponModel.swift
//  Celebrityschool
//
//  Created by Hiren on 08/10/21.
//

import Foundation

struct CouponModel {
    var data: CouponDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = CouponDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct CouponDataModel{
    var promocode: String?
    var album_id: Int?
    var celebrity_name: String?
    var course_name: String?
    var slug: String?
   
    init(dictionary: [String: Any]) {
        promocode = dictionary["promocode"] as? String
        album_id = dictionary["album_id"] as? Int
        celebrity_name = dictionary["celebrity_name"] as? String
        course_name = dictionary["course_name"] as? String
        slug = dictionary["slug"] as? String

    }

}

