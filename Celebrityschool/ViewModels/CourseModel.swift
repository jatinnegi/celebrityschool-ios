//
//  CourseModel.swift
//  Celebrityschool
//
//  Created by Hiren on 07/08/21.
//

import Foundation

struct CourseModel{
    var id: Int?
    var meta_title: String?
    var meta_description: String?
    var a_name: String?
    var slug: String?
    var a_desc: String?
    var a_thumb: String?
    var a_thumb_alt:String?
    var a_workbook:String?
    var a_likes:Int?
    var a_trailer_link:String?
    var a_artist:String?
    var category:String?
    var a_price:String?
    var status:Int?
    var rank:String?
    var created_at:String?
    var feature_img:String?
    var mobile_card_image:String?
    var mobile_album_banner:String?
    var total_length:String?
    
    init(dictionary: [String: Any]) {
        id = dictionary["id"] as? Int
        meta_title = dictionary["meta_title"] as? String
        meta_description = dictionary["meta_description"] as? String
        a_name = dictionary["a_name"] as? String
        slug = dictionary["slug"] as? String
        a_desc = dictionary["a_desc"] as? String
        a_thumb = dictionary["a_thumb"] as? String
        a_thumb_alt = dictionary["a_thumb_alt"] as? String
        a_workbook = dictionary["a_workbook"] as? String
        a_likes = dictionary["a_workbook"] as? Int
        a_trailer_link = dictionary["a_trailer_link"] as? String
        a_artist = dictionary["a_artist"] as? String
        category = dictionary["category"] as? String
        a_price = dictionary["a_price"] as? String
        status = dictionary["status"] as? Int
        rank = dictionary["rank"] as? String
        created_at = dictionary["created_at"] as? String
        feature_img = dictionary["feature_img"] as? String
        mobile_card_image = dictionary["mobile_card_image"] as? String
        mobile_album_banner = dictionary["mobile_album_banner"] as? String
        total_length = dictionary["total_length"] as? String

    }
}
