//
//  EbookModel.swift
//  Celebrityschool
//
//  Created by Hiren on 05/09/21.
//

import Foundation

struct EbookModel {
    var data: EbookMainModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = EbookMainModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct EbookMainModel {
    var ebook: EbookDataModel?
    
    init(dictionary: [String: Any]) {
        ebook = EbookDataModel(dictionary: dictionary["ebook"] as? [String: Any] ?? [:])
    }
}

struct EbookDataModel{
    var ebook_title: String?
    var id: Int?
    var album_id: Int?
    var ebook_id: String?
    var ebook_description: String?
   
    init(dictionary: [String: Any]) {
        ebook_title = dictionary["ebook_title"] as? String
        id = dictionary["id"] as? Int
        album_id = dictionary["album_id"] as? Int
        ebook_id = dictionary["ebook_id"] as? String
        ebook_description = dictionary["ebook_description"] as? String
    }
}
