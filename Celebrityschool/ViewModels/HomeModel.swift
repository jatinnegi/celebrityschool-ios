//
//  HomeModel.swift
//  Celebrityschool
//
//  Created by Hiren on 14/08/21.
//

import Foundation


struct HomeModel {
    var data: HomeDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = HomeDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}


struct HomeDataModel{
    
    var title: String?
    var banner: HomeBannerModel?
    var lessons: [LeassonModel]?
    var all_lessons_purchased:AllessonsPurchasedModel?
    
    init(dictionary: [String: Any]) {
        title = dictionary["title"] as? String
        banner = HomeBannerModel(dictionary: dictionary["banner"] as? [String: Any] ?? [:])
        all_lessons_purchased = AllessonsPurchasedModel(dictionary: dictionary["all_lessons_purchased"] as? [String: Any] ?? [:])
        
        if let arrayleassons = dictionary["lessons"] as? NSArray,arrayleassons.count > 0{
            lessons = modelsFromDictionaryArray(array: arrayleassons)
        }
    }
    
    func modelsFromDictionaryArray(array:NSArray) -> [LeassonModel]
    {
        var models:[LeassonModel] = []
        for item in array
        {
            models.append(LeassonModel(dictionary: item as! [String: Any]))
        }
        return models
    }
}

struct HomeBannerModel{
    var image: String?
    
    init(dictionary: [String: Any]) {
        image = dictionary["image"] as? String
    }
}


struct LeassonModel {
    
    var title: String?
    var course: [CourseModel]?
    var button: String?
    var style: String?
    
    init(dictionary: [String: Any]) {
        title = dictionary["title"] as? String
        if let arrayleassons = dictionary["course"] as? NSArray,arrayleassons.count > 0{
            course = modelsFromDictionaryArray(array: arrayleassons)
        }
        
        button = dictionary["button"] as? String
        style = dictionary["style"] as? String
    }
    
    
    func modelsFromDictionaryArray(array:NSArray) -> [CourseModel]
    {
        var models:[CourseModel] = []
        for item in array
        {
            models.append(CourseModel(dictionary: item as! [String: Any]))
        }
        return models
    }
}

struct AllessonsPurchasedModel {
    var Credit: Bool?
    var product_id: String?
    var apple_id: String?
    var amount: Int?
    var ios_amount: Int?
    var album_id: Int?
   
    
    init(dictionary: [String: Any]) {
        Credit = dictionary["Credit"] as? Bool
        product_id = dictionary["product_id"] as? String
        apple_id = dictionary["apple_id"] as? String
        amount = dictionary["amount"] as? Int
        ios_amount = dictionary["ios_amount"] as? Int
        album_id = dictionary["album_id"] as? Int
    }
}

struct configModel {
    var data: ConfigDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = ConfigDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct ConfigDataModel{
    var is_active: Int?
    
    init(dictionary: [String: Any]) {
        is_active = dictionary["is_active"] as? Int
    }
}
