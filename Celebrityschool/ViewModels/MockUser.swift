/*
 MIT License

 Copyright (c) 2017-2019 MessageKit

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import Foundation
struct socketModel {
    var user: String?
    var message: [Message]?
   
    
    init(dictionary: [String: Any]) {
        user = dictionary["user"] as? String
        if let arrayleassons = dictionary["text"] as? NSArray,arrayleassons.count > 0{
            message = modelsFromDictionaryArray(array: arrayleassons)
        }
    }
    
    
    func modelsFromDictionaryArray(array:NSArray) -> [Message]
    {
        var models:[Message] = []
        for item in array
        {
            models.append(Message(dictionary: item as! [String: Any]))
        }
        return models
    }
}

struct Message {
    var communityId : String?
    var communityType: String?
    var createdAt: String?
    var expire_at: String?
    var message : String?
    var updatedAt : String?
    var userId : Int?
    var userName : String?
    
    
    init(dictionary: [String: Any]) {
        communityId = dictionary["communityId"] as? String
        communityType = dictionary["communityType"] as? String
        createdAt = dictionary["createdAt"] as? String
        expire_at = dictionary["expire_at"] as? String
        message = dictionary["message"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        userId = dictionary["userId"] as? Int
        userName = dictionary["userName"] as? String
    }
}
