//
//  OrderModel.swift
//  Celebrityschool
//
//  Created by Hiren on 27/08/21.
//

import Foundation

struct OrderModel {
    var data: OrderDataModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = OrderDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct OrderDataModel{
    var course_name: String?
    var txnid: String?
    var album_id: String?
    var amount: String?
   
    
    init(dictionary: [String: Any]) {
        course_name = dictionary["course_name"] as? String
        txnid = dictionary["txnid"] as? String
        album_id = dictionary["album_id"] as? String
        amount = dictionary["amount"] as? String
    }
}


struct OrderUpdateModel {
    var data: OrderUpdateDataModel?
    var meta: metaData?
    var couponCode:Bool = false
    
    init(dictionary: [String: Any]) {
        data = OrderUpdateDataModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
        couponCode = dictionary["couponCode"] as? Bool ?? false
    }
}

struct OrderUpdateDataModel{
    var course_name: String?
    var txnid: String?
    var album_id: Int?
    var payment_id: String?
    var email: String?
    var amount:String?
    var Credit:Bool?
    var album_amount:Int?
    var apple_id:String?
    var product_id:String?
    var orderAlbum_id:String?
    

    init(dictionary: [String: Any]) {
        course_name = dictionary["course_name"] as? String
        txnid = dictionary["txnid"] as? String
        album_id = dictionary["album_id"] as? Int
        payment_id = dictionary["payment_id"] as? String
        email = dictionary["email"] as? String
        amount = dictionary["amount"] as? String
        Credit = dictionary["Credit"] as? Bool
        album_amount = dictionary["album_amount"] as? Int
        apple_id = dictionary["apple_id"] as? String
        product_id = dictionary["product_id"] as? String
        orderAlbum_id = dictionary["orderAlbum_id"] as? String
        
    }

}

