//
//  PlayerModel.swift
//  Celebrityschool
//
//  Created by Hiren on 14/09/21.
//

import Foundation

struct PlayerModel {
    var data: FetchVideoLessonModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = FetchVideoLessonModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct FetchVideoLessonModel{
    var email: String?
    var time_in_seconds: Int?
    var slug: String?
    var video_index: String?
   
    init(dictionary: [String: Any]) {
        email = dictionary["email"] as? String
        time_in_seconds = dictionary["time_in_seconds"] as? Int
        slug = dictionary["slug"] as? String
        video_index = dictionary["video_index"] as? String
    }
}


struct CertificateModel {
    var data: CertificateGetModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = CertificateGetModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct CertificateGetModel{
    var certificate_b64: String?
    
    init(dictionary: [String: Any]) {
        certificate_b64 = dictionary["certificate_b64"] as? String
    }
}
