//
//  ProfileModel.swift
//  Celebrityschool
//
//  Created by Hiren on 10/08/21.
//

import Foundation

struct ProfileModel{
    var id: Int?
    var social_id: String?
    var social_platform: String?
    var first_name: String?
    var last_name: String?
    var name: String?
    var certificate_name: String?
    var email:String?
    var profilePic:String?
    var ip:String?
    var mobile:String?
    var pincode:Int?
    var city:String?
    var state:String?
    var age:Int?
    var profession:String?
    var status:Int?
    var mailstatus:Int?
    var devicetype:String?
    var password:String?
    var about_me:String?
    
    init(dictionary: [String: Any]) {
        id = dictionary["id"] as? Int
        social_id = dictionary["social_id"] as? String
        social_platform = dictionary["social_platform"] as? String
        first_name = dictionary["first_name"] as? String
        last_name = dictionary["last_name"] as? String
        name = dictionary["name"] as? String
        certificate_name = dictionary["certificate_name"] as? String
        email = dictionary["email"] as? String
        profilePic = dictionary["profilePic"] as? String
        ip = dictionary["ip"] as? String
        mobile = dictionary["mobile"] as? String
        pincode = dictionary["pincode"] as? Int
        city = dictionary["city"] as? String
        state = dictionary["state"] as? String
        age = dictionary["age"] as? Int
        profession = dictionary["profession"] as? String
        status = dictionary["status"] as? Int
        mailstatus = dictionary["mailstatus"] as? Int
        devicetype = dictionary["devicetype"] as? String
        password = dictionary["password"] as? String
        about_me = dictionary["about_me"] as? String
           
    }
}
