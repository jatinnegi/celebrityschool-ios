//
//  PurchaseCourseModel.swift
//  Celebrityschool
//
//  Created by Hiren on 12/09/21.
//

import Foundation

struct PurchaseCourseModel {
    var data: PurchaseModel?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = PurchaseModel(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }
}

struct PurchaseModel {
    var course_purchased: [CourseModel]?
    var course_progress: [CourseProgressModel]?
    
    init(dictionary: [String: Any]) {
        if let arrayCourse = dictionary["course_purchased"] as? NSArray,arrayCourse.count > 0{
            course_purchased = modelsFromDictionaryArray(array: arrayCourse)
        }
        
        if let arrayCoursePurchased = dictionary["course_progress"] as? NSArray,arrayCoursePurchased.count > 0{
            course_progress = modelsFromDictionaryArray(array: arrayCoursePurchased)
        }
    }
}


struct CourseProgressModel{
    var category: String?
    var current_lesson: Int?
    var album_id: Int?
    var total_lessons: Int?
    var course_complete_percent: Int?
    var total_percentage: Int?
    var selected: Bool = false
   
    init(dictionary: [String: Any]) {
        category = dictionary["category"] as? String
        current_lesson = dictionary["current_lesson"] as? Int
        album_id = dictionary["album_id"] as? Int
        total_lessons = dictionary["total_lessons"] as? Int
        course_complete_percent = dictionary["course_complete_percent"] as? Int
        total_percentage = dictionary["total_percentage"] as? Int
        selected = dictionary["selected"] as? Bool ?? false

    }
}

func modelsFromDictionaryArray(array:NSArray) -> [CourseModel]
{
    var models:[CourseModel] = []
    for item in array
    {
        models.append(CourseModel(dictionary: item as! [String: Any]))
    }
    return models
}

func modelsFromDictionaryArray(array:NSArray) -> [CourseProgressModel]
{
    var models:[CourseProgressModel] = []
    for item in array
    {
        models.append(CourseProgressModel(dictionary: item as! [String: Any]))
    }
    return models
}


