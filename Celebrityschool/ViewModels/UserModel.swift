//
//  UserModel.swift
//  Celebrityschool
//
//  Created by Hiren on 02/08/21.
//

import Foundation

struct AppResponseModel {
    var data: AppData?
    var meta: metaData?
    
    init(dictionary: [String: Any]) {
        data = AppData(dictionary: dictionary["data"] as? [String: Any] ?? [:])
        meta = metaData(dictionary: dictionary["meta"] as? [String: Any] ?? [:])
    }   
}

struct metaData{
    var message: String?
    var flag: String?
    var statuscode: Int?
    
    init(dictionary: [String: Any]) {
        message = dictionary["message"] as? String ?? dictionary["msg"] as? String
        flag = dictionary["flag"] as? String
        statuscode = dictionary["statuscode"] as? Int
    }
}

struct AppData{
    var token: String?
    var first_name: String?
    var last_name: String?
    var certificate_name: String?
    var email: String?
    var profilePic: String?
    var mobile: String?
    var pincode: String?
    var city: String?
    var state: String?
    var age: String?
    var profession: String?
    var about_me: String?
    var userid: Int?
    var profile: ProfileModel?
    
    init(dictionary: [String: Any]) {
        token = dictionary["token"] as? String
        first_name = dictionary["first_name"] as? String
        last_name = dictionary["last_name"] as? String
        certificate_name = dictionary["certificate_name"] as? String ?? dictionary["certificatename"] as? String ?? ""
        email = dictionary["email"] as? String
        profilePic = dictionary["profilePic"] as? String
        mobile = dictionary["mobile"] as? String
        pincode = dictionary["pincode"] as? String
        city = dictionary["city"] as? String
        state = dictionary["state"] as? String
        age = dictionary["age"] as? String
        profession = dictionary["profession"] as? String
        about_me = dictionary["about_me"] as? String
        userid = dictionary["id"] as? Int
        if let profileData = dictionary["profile"] as? [String:Any]{
            profile = ProfileModel(dictionary: profileData)
        }
    }
}

struct UserInfo: Codable {
    var name: String?
    var email: String?
    var userid: String?
    var profileimage: String?
    var authType: String?
    var deviceToken: String?

    enum CodingKeys: String, CodingKey {
        case name
        case email
        case userid
        case profileimage
        case authType
    }



    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        name = try values.decode(String.self, forKey: .name)
        email = try values.decode(String.self, forKey: .email)
        userid = try values.decode(String.self, forKey: .userid)
        profileimage = try values.decode(String.self, forKey: .profileimage)
        authType = try values.decode(String.self, forKey: .authType)
    }
}
