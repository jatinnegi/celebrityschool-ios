//
//  CorseDetailMainCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 19/08/21.
//

import UIKit

class CorseDetailMainCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CorseDetailMainCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CorseDetailMainCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            mainCollectionView.register(CorseDetailsCollectionViewCell.nib, forCellWithReuseIdentifier: CorseDetailsCollectionViewCell.reuseIdentifier)
        }
    }
    
    let layout = UICollectionViewFlowLayout()
    var videoLeassonData:[VideoLeassonModel] = []
    let width = UIScreen.main.bounds.size.width
    let height = UIScreen.main.bounds.size.width / 3
    var albumData:AlbumDataModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.preservesSuperviewLayoutMargins = false
        self.layoutMargins = .zero
//        layout.scrollDirection = .vertical
//        layout.minimumLineSpacing = 0
//        layout.minimumInteritemSpacing = 0
////        layout.itemSize = CGSize(width: width, height: height)
////        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//        mainCollectionView.setCollectionViewLayout(layout, animated: false)
    }
    
    func loadCell(_ model:[VideoLeassonModel]){
        videoLeassonData = model
        titleLabel.text = "\(model.count) Lessons"
        DispatchQueue.main.async{
            self.layoutIfNeeded()
            self.mainCollectionView.layoutIfNeeded()
            self.mainCollectionView.reloadData()
        }
    }

}

extension CorseDetailMainCollectionViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoLeassonData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CorseDetailsCollectionViewCell.reuseIdentifier, for: indexPath) as! CorseDetailsCollectionViewCell
        cell.loadCell(videoLeassonData[indexPath.row],index: indexPath.row + 1)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let purchased = albumData?.is_course_purchased,let credit = purchased.Credit, credit == false{
            if let topController = UIApplication.topViewController(){
                AlertIndicator_Toast().showToast(Constants.KNOTPurchased, vc: topController)
            }
        }else{
            if let album = albumData?.album_info{
                let model = videoLeassonData[indexPath.row]
                if model.video_url?.isValidSchemedUrl == false{
                    CustomNavigationController().callEbookViewController(model, privateUrl: "")
                }else{
                    CustomNavigationController().callPlayerViewController(album, leassonModel: model,index:indexPath.row + 1)
                }
            }
        }
    }
}

extension CorseDetailMainCollectionViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.width / 3
        
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 5, bottom: 0, right: 10)
    }
    
}

