//
//  CorseDetailsCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 19/08/21.
//

import UIKit
import SDWebImageWebPCoder

class CorseDetailsCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CorseDetailsCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CorseDetailsCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var leassonImageView: UIImageView!{
        didSet{
//            leassonImageView.image = UIImage(named: "")
            leassonImageView.layer.cornerRadius = 5
            leassonImageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel!{
        didSet{
            titleLabel.text = ""
        }
    }
    @IBOutlet weak var subTitleLabel: UILabel!{
        didSet{
            subTitleLabel.text = ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell(_ model:VideoLeassonModel,index:Int){
        if let image = model.video_thumb{
            if let url = URL(string:Constants.video_thumb + image){
                leassonImageView.sd_setImage(with: url, placeholderImage: nil, options: [.progressiveLoad])

            }
        }
        
        if let title = model.video_title{
            titleLabel.text = "\(index). " + title
           
        }
        
        if let description = model.description{
            subTitleLabel.text = description
        }
    }

}
