//
//  CourseHeaderCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 19/08/21.
//

import UIKit
import SDWebImageWebPCoder


class CourseHeaderCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CourseHeaderCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CourseHeaderCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var buynowButton: UIButton!
    @IBOutlet weak var lenghtLabel: UILabel!
    @IBOutlet weak var studentsLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var cc_buyNowHeight: NSLayoutConstraint!
    
    var albumData:AlbumDataModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buynowButton.layer.cornerRadius = 5
        buynowButton.layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async{
            self.buynowButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }
    
    func loadCell(_ model:AlbumDataModel){
        albumData = model

        if let albuminfo = model.album_info{
            self.commonDataSet(albuminfo)
        }
        
        if let purchased = model.is_course_purchased,let credit = purchased.Credit, credit == true{
            buynowButton.isHidden = true
            cc_buyNowHeight.constant = 0
        }else{
            buynowButton.isHidden = false
            cc_buyNowHeight.constant = 50
        }
        
        if let  students_rating = model.students_rating{
            if let student_count = students_rating.student_count{
                studentsLabel.text = student_count
            }
            
            if let rating = students_rating.rating{
                ratingLabel.text = "\(rating).0"
            }
        }
    }
    
    func loadCellbefore(_ model:CourseModel){
        self.commonDataSet(model)
    }
    
    
    func commonDataSet(_ model:CourseModel?){
        if let album_info = model{
            if let name = album_info.a_artist{
                nameLabel.text = name
            }

            if let description = album_info.a_desc{
                descriptionLabel.text = description
            }

            if let category = album_info.category{
                subTitleLabel.text = category
            }
            
            if let image = album_info.mobile_album_banner{
                if let url = URL(string:image){
                    bannerImageView.contentMode = .scaleAspectFill
                    bannerImageView.sd_setImage(with: url, placeholderImage: nil, options: [.progressiveLoad])

                }
            }
            
            if let legth = album_info.total_length{
                lenghtLabel.text = legth
            }
            
        }
    }
    
    
    @IBAction func shareButtonAction(_ sender: Any) {
        if let album_info = albumData?.album_info{
            CustomNavigationController().callShare(album_info)
        }
    }
    
    @IBAction func favButtonAction(_ sender: Any) {
    }
    
    @IBAction func onPlayButtonPressed(_ sender: Any){
        if let album = albumData?.album_info{
            CustomNavigationController().callPlayerViewController(album, leassonModel: VideoLeassonModel(dictionary: [:]),index:0)
        }
    }

    @IBAction func buyButtonAction(_ sender: Any) {
        
//        var data = OrderUpdateModel(dictionary: [:])
//        var model = data.data
//        var meta = data.meta
//        meta?.statuscode = 200
//
//        model?.amount = "2999"
//        model?.course_name = "Vikas Khanna's Modern Cooking Course"
//        model?.album_amount = "3999"
//        model?.product_id = "com.Celebrityschool.Course.00"
//        model?.Credit = false
//        model?.album_id = "0"
//        data.data = model
//        data.meta = meta
//        CustomNavigationController().callSuccessFailureViewController(data)
        

        guard let plan_id = albumData?.album_info?.id else{return}
        guard let a_price = albumData?.album_info?.a_price else{return}

        guard let purchased = self.albumData?.is_course_purchased else{return}

        guard let product_id = purchased.product_id else{return}

        CourseApiServices().productBuyCall("\(plan_id)", a_price: a_price, product_id: product_id)
        
        if let data = albumData?.album_info{
            AnalyticsManager.sharedInstance.SmarTechOnbuy_now(data)
            
            let dict:[String:String] = [
                "course_category":data.category ?? "",
                "celebrity_name":data.a_artist ?? "",
            ]
            UserDefaults.standard.set(dict, forKey: "albumData")

        }
        
    }
}
