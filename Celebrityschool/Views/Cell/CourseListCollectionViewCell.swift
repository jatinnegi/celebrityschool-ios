//
//  CourseListCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 11/08/21.
//

import UIKit
import SDWebImageWebPCoder


class CourseListCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "CourseListCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CourseListCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var bgView: UIView!{
        didSet{
            self.bgView.layer.cornerRadius = 5
            self.bgView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var courseImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    
    func loadCell(_ model:CourseModel){
        
        if let image = model.mobile_card_image{
            if let url = URL(string:image){
                courseImageView.sd_setImage(with: url, placeholderImage: nil, options: [.progressiveLoad])

            }
        }
    }

}
