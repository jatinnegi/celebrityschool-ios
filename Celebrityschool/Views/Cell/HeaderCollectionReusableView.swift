//
//  HeaderCollectionReusableView.swift
//  Celebrityschool
//
//  Created by Hiren on 11/08/21.
//

import UIKit
import StoreKit
import SDWebImageWebPCoder

class HeaderCollectionReusableView: UICollectionReusableView {
    
    static let reuseIdentifier = "HeaderCollectionReusableView"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "HeaderCollectionReusableView", bundle: nil)
        }
    }
    
    @IBOutlet weak var bannerImage:UIImageView!
    @IBOutlet weak var titleLabel: UILabel!{
        didSet{
            titleLabel.isHidden = true
        }
    }
    
    @IBOutlet weak var subTitleLabel: UILabel!{
        didSet{
            subTitleLabel.isHidden = true
        }
    }
    

    @IBOutlet weak var getStratedLbel: UILabel!
    @IBOutlet weak var getStartedButton: UIButton!{
        didSet{
            getStartedButton.layer.cornerRadius = 5
            getStartedButton.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var getStartedView: UIView!{
        didSet{
            self.getStartedView.isHidden = true
        }
    }
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var cc_getStartedViewHeight: NSLayoutConstraint!//73
    {
        didSet{
            self.cc_getStartedViewHeight.constant = 0
        }
    }
    
    var model:AllessonsPurchasedModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        DispatchQueue.main.async{
//            self.getStartedButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
//        }
    }
    
    
    func loadHeaderCell(_ bannerModel:HomeBannerModel,purchasedmodel:AllessonsPurchasedModel){
        model = purchasedmodel
        
//        if let image = bannerModel.image{
//            if let url = URL(string:image){
//                bannerImage.sd_setImage(with: url, placeholderImage: UIImage(named: "NewInsideBg"), options: [.progressiveLoad])
//            }
//        }

//            if let title = bannerModel?.title{
//                headerView.titleLabel.text = title
//            }
        
        if let credit = purchasedmodel.Credit, credit == true{
            getStartedView.isHidden = true
            cc_getStartedViewHeight.constant = 0
        }else{
            getStartedView.isHidden = false
            cc_getStartedViewHeight.constant = 73
            DispatchQueue.main.async{
                self.getStartedButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
            }
        }
    }
    
    @IBAction func getStartedButtonAction(_ sender: Any) {
        
        guard let plan_id = model?.album_id else{return}
        guard let a_price = model?.ios_amount else{return}
        guard let product_id = self.model?.product_id else{return}
        
        
        let payloadDictionary: [String : Any] = [
            "celebrity_name" : plan_id ,
            "course_category" : "All Course",
            "amount" : Int(a_price) 
        ]
        
        AnalyticsManager.sharedInstance.SmartTechAllCourseBuy(payloadDictionary)


        CourseApiServices().productBuyCall("\(plan_id)", a_price: "\(a_price)", product_id: product_id)

    }
}
