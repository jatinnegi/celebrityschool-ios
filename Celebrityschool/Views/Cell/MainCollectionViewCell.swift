//
//  MainCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 11/08/21.
//

import UIKit


class MainCollectionViewCell: UICollectionViewCell {

    static let reuseIdentifier = "MainCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "MainCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    var leassoData:LeassonModel?
    var courseData:[CourseModel]?
    
    @IBOutlet weak var mainCollectionView: UICollectionView!{
        didSet{
            mainCollectionView.delegate = self
            mainCollectionView.dataSource = self
            mainCollectionView.register(CourseListCollectionViewCell.nib, forCellWithReuseIdentifier: CourseListCollectionViewCell.reuseIdentifier)
        }
    }
    
    let layout = UICollectionViewFlowLayout()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell(_ model:LeassonModel){
        leassoData = model
        courseData = []
        if let title = leassoData?.title{
            titleLabel.text = title
        }
        layout.scrollDirection = .horizontal //.horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        mainCollectionView.setCollectionViewLayout(layout, animated: false)
        mainCollectionView.reloadData()
    }
    
    func loadCell2(modelCourse:[CourseModel]){
        leassoData = nil
        courseData = modelCourse
        titleLabel.text = "Your Courses"
        titleLabel.font = UIFont(name: "Roboto-Medium", size: 14.0)!
        layout.scrollDirection = .horizontal //.horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        mainCollectionView.setCollectionViewLayout(layout, animated: false)
        mainCollectionView.reloadData()
    }

}

extension MainCollectionViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if leassoData?.course?.count ?? 0 > 0{
            return leassoData?.course?.count ?? 0
        }else if courseData?.count ?? 0 > 0{
            return courseData?.count ?? 0
        }else{
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CourseListCollectionViewCell.reuseIdentifier, for: indexPath) as! CourseListCollectionViewCell
        if leassoData?.course?.count ?? 0 > 0{
            if let coursedata = leassoData?.course?[indexPath.row]{
                cell.loadCell(coursedata)
            }
        }else if courseData?.count ?? 0 > 0{
            if let coursedata = courseData?[indexPath.row]{
                cell.loadCell(coursedata)
            }
        }
        return cell
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if leassoData?.course?.count ?? 0 > 0{
            if let coursedata = leassoData?.course?[indexPath.row]{
                CustomNavigationController().callCourseDetailViewController(coursedata)
            }
        }else if courseData?.count ?? 0 > 0{
            if let coursedata = courseData?[indexPath.row]{
                CustomNavigationController().callCourseDetailViewController(coursedata)
            }
        }
        
    }
    
    
}

extension MainCollectionViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width :CGFloat = (UIScreen.main.bounds.size.width) / 3.5
        var height:CGFloat = mainCollectionView.frame.height
        
        if let leasson = leassoData{
            let title = leasson.title
            switch title {
            case "Courses":
                width = (UIScreen.main.bounds.size.width) / 2.3
                height = mainCollectionView.frame.height
            case "Trending Courses":
                width = (UIScreen.main.bounds.size.width) / 2.9
                height = mainCollectionView.frame.height
            default:
                height = 0.0
            }
        }
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 5, bottom: 0, right: 10)
    }
    
}
