//
//  CertificateCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 14/09/21.
//

import UIKit

class CertificateCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "CertificateCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CertificateCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var certificateImage: UIImageView!
    @IBOutlet weak var lockUnlockImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
