//
//  CertificateMainCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 14/09/21.
//

import UIKit

class CertificateMainCollectionViewCell: UICollectionViewCell {
   
    static let reuseIdentifier = "CertificateMainCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CertificateMainCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var mainCollcetionView: UICollectionView!{
        didSet{
            mainCollcetionView.delegate = self
            mainCollcetionView.dataSource = self
            mainCollcetionView.register(CertificateCollectionViewCell.nib, forCellWithReuseIdentifier: CertificateCollectionViewCell.reuseIdentifier)
        }
    }
    
    let layout = UICollectionViewFlowLayout()
    
    @IBOutlet weak var seeView: UIView!
    
    var courseData:[CourseModel]?
    var lockHide:Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell2(modelCourse:[CourseModel]){
        courseData = modelCourse
        layout.scrollDirection = .horizontal //.horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        mainCollcetionView.setCollectionViewLayout(layout, animated: false)
        mainCollcetionView.reloadData()
    }

    @IBAction func seeAllButtonAction(_ sender: Any) {
        if let data = courseData{
            CustomNavigationController().callCertificateViewController(data,lockHide: lockHide ?? false)
        }
    }
}

extension CertificateMainCollectionViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return courseData?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CertificateCollectionViewCell.reuseIdentifier, for: indexPath) as! CertificateCollectionViewCell
        cell.lockUnlockImage.isHidden = lockHide ?? false
        return cell
      
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let chekc = lockHide, chekc == true{
            if let albumid = courseData?[indexPath.row].id{
                CustomNavigationController().CertificateDetailViewControllerCall("\(albumid)")
            }
        }else{
            if let topController = UIApplication.topViewController(){
                AlertIndicator_Toast().showToast(Constants.KProfileNotCompleted, vc: topController)
            }
        }
    }
}

extension CertificateMainCollectionViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat = (UIScreen.main.bounds.size.width) / 2
        var height:CGFloat = mainCollcetionView.frame.height
        
        if courseData?.count ?? 0 == 0{
            height = 0
        }
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left: 0, bottom: 0, right: 10)
    }
    
}

