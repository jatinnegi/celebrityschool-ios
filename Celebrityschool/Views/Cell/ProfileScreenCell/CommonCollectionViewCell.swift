//
//  CommonCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 31/01/22.
//

import UIKit

class CommonCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "CommonCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CommonCollectionViewCell", bundle: nil)
        }
    }

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
