//
//  CouponCodeCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 08/10/21.
//

import UIKit

class CouponCodeCollectionViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "CouponCodeCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CouponCodeCollectionViewCell", bundle: nil)
        }
    }

    let attributes = [
        NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 14)! // Note the !
    ]
    
    @IBOutlet weak var couponCodeTxtfiled: UITextField!{
        didSet{
            couponCodeTxtfiled.delegate = self
            couponCodeTxtfiled.keyboardType = .default
            couponCodeTxtfiled.autocapitalizationType = .allCharacters
            couponCodeTxtfiled.attributedPlaceholder = NSAttributedString(string: "Enter Coupon Code",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var bgView: UIView!{
        didSet{
            bgView.layer.cornerRadius = 5
            bgView.layer.masksToBounds = true
            bgView.backgroundColor = .white 
        }
    }
    @IBOutlet weak var doneButton: UIButton!{
        didSet{
            doneButton.layer.cornerRadius = 5
            doneButton.layer.masksToBounds = true
        }
    }
    
    private var viewModel: CouponCodeApiServices!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async{
            self.doneButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }
    
 

    @IBAction func doneButtonAction(_ sender: Any) {
        couponCodeTxtfiled.resignFirstResponder()
        guard let couponCode = couponCodeTxtfiled.text, couponCode != "" else {
            return
        }
        let props = [
            "promocode":couponCode ,
            "source": "ios"
            ] as [String : Any]


        couponCodeTxtfiled.text = ""
        viewModel = CouponCodeApiServices()
        viewModel.coponCodeApiCall(props)
//        var data = CouponModel(dictionary: [:])
//        var model = data.data
//        var meta = data.meta
//        meta?.statuscode = 200
//
//        model?.course_name = "Vikas Khanna's Modern Cooking Course"
//        model?.promocode = "DEMO68888"
//        model?.celebrity_name = "Vikas Khanna's"
//        model?.course_name = "Communication Skills"
//        model?.slug = "sabira-merchant-communication-skills-classes"
//        model?.album_id = 6
//
//        data.data = model
//        data.meta = meta
//
//        var data2 = OrderUpdateModel(dictionary: [:])
//        data2.couponCode = true
        
//        var data = OrderUpdateModel(dictionary: [:])
//        var model = data.data
//        var meta = data.meta
//        meta?.statuscode = 201
//
//        model?.amount = "2999"
//        model?.course_name = "Vikas Khanna's Modern Cooking Course"
//        model?.album_amount = 3999
//        model?.product_id = "com.Celebrityschool.Course.00"
//        model?.Credit = false
//        model?.album_id = 0
//        data.data = model
//        data.meta = meta
        
//        CustomNavigationController().callSuccessFailureViewController(data2, data)
        
    }
}
//MARK:UITextFiled Delegate
extension CouponCodeCollectionViewCell:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
