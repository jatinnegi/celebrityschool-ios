//
//  CourseAnalysisCategoryCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 28/08/21.
//

import UIKit

class CourseAnalysisCategoryCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "CourseAnalysisCategoryCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CourseAnalysisCategoryCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var bgView: UIView!{
        didSet{
            bgView.layer.cornerRadius = bgView.frame.height / 2.5
            bgView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var bgImageView: UIImageView!{
        didSet{
            bgImageView.isHidden = true
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func loadCell(_ model:CourseProgressModel){
        if let category = model.category{
            titleLabel.text = category
        }
        
        if model.selected {
            bgImageView.isHidden = false
//            bgView.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
            titleLabel.textColor = .white
        }else{
            bgImageView.isHidden = true
            titleLabel.textColor = .black
        }
    }

}
