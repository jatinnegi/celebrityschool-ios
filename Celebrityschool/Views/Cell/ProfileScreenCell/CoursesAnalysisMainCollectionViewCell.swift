//
//  CoursesAnalysisMainCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 28/08/21.
//

import UIKit

class CoursesAnalysisMainCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "CoursesAnalysisMainCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "CoursesAnalysisMainCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var courseAnalysisCategoryCollcetionView: UICollectionView!{
        didSet{
            courseAnalysisCategoryCollcetionView.delegate = self
            courseAnalysisCategoryCollcetionView.dataSource = self
            courseAnalysisCategoryCollcetionView.register(CourseAnalysisCategoryCollectionViewCell.nib, forCellWithReuseIdentifier: CourseAnalysisCategoryCollectionViewCell.reuseIdentifier)
        }
    }
    @IBOutlet weak var pieChartView: DoughnutView!{
        didSet{
            pieChartView.colors = [UIColor(hexString: "#7C01E9")!, UIColor(hexString: "#F41890")!]

        }
    }
    
    @IBOutlet weak var pendingPercentageLabel: UILabel!
    @IBOutlet weak var compltedPercentageLabel: UILabel!
    let layout = UICollectionViewFlowLayout()
    var coursePModel:[CourseProgressModel]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        courseAnalysisCategoryCollcetionView.delegate = self
        courseAnalysisCategoryCollcetionView.dataSource = self
    }
    
    
    func loadCell(_ model:[CourseProgressModel]){
        coursePModel = model
        if coursePModel?.count ?? 0 > 0{
            if var data = coursePModel?[0]{
                data.selected = true
                coursePModel?[0] = data
                drawPieChart(data)
            }
        }
        
        layout.scrollDirection = .horizontal //.horizontal
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        courseAnalysisCategoryCollcetionView.setCollectionViewLayout(layout, animated: false)
        courseAnalysisCategoryCollcetionView.reloadData()
    }
    
    func drawPieChart(_ data :CourseProgressModel){
        let completeCourse:Float = Float(data.course_complete_percent ?? 0)
        let totalPercentage:Float = Float(data.total_percentage ?? 0)
        
        let remaingPercentage:Float = Float(totalPercentage - completeCourse)
        pieChartView.data = [completeCourse, remaingPercentage]
        
        compltedPercentageLabel.text = "\(completeCourse)%"
        pendingPercentageLabel.text = "\(remaingPercentage)%"
    }

}

extension CoursesAnalysisMainCollectionViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return coursePModel?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CourseAnalysisCategoryCollectionViewCell.reuseIdentifier, for: indexPath) as! CourseAnalysisCategoryCollectionViewCell
        if let data = coursePModel?[indexPath.row]{
            cell.loadCell(data)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for row in 0..<coursePModel!.count{
            if var data2 = coursePModel?[row]{
                data2.selected = false
                coursePModel?[row] = data2
            }
        }
    
        
        if var data = coursePModel?[indexPath.row] {
            data.selected = true
            coursePModel?[indexPath.row] = data
            drawPieChart(data)
        }
        
        courseAnalysisCategoryCollcetionView.reloadData()

    }
}

extension CoursesAnalysisMainCollectionViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width :CGFloat = 150.0
        let height:CGFloat = 50.0
        
        return .init(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 0, left:10, bottom: 0, right: 10)
    }
    
}
