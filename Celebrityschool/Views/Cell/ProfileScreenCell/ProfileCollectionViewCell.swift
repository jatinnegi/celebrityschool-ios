//
//  ProfileCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 28/08/21.
//

import UIKit
import SDWebImageWebPCoder


class ProfileCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "ProfileCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "ProfileCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var bgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var coverView: UIView!{
        didSet{
            coverView.clipsToBounds = true
            coverView.layer.cornerRadius = 15
            coverView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        }
    }
    @IBOutlet weak var imageBgView: UIView!{
        didSet{
            imageBgView.layer.cornerRadius = imageBgView.frame.height / 2
            imageBgView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var profileImageView: UIImageView!{
        didSet{
            profileImageView.layer.cornerRadius = profileImageView.frame.height / 2
            profileImageView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var aboutMeStateLabel: UILabel!{
        didSet{
            aboutMeStateLabel.isHidden = true
        }
    }
    
    @IBOutlet weak var cc_aboutMeStateLabelHeight: NSLayoutConstraint!//21
    {
        didSet{
            cc_aboutMeStateLabelHeight.constant = 0
        }
    }
    
    @IBOutlet weak var aboutMeLabel: UILabel!{
        didSet{
            aboutMeLabel.text = ""
        }
    }
    
    @IBOutlet weak var editProfileView: UIView!{
        didSet{
            editProfileView.layer.cornerRadius = 5
            editProfileView.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var sepratorLabel: UILabel!
    @IBOutlet weak var profileStatusView: UIView!{
        didSet{
            profileStatusView.isHidden = true
        }
    }
    @IBOutlet weak var cc_profileStateHeight: NSLayoutConstraint!//75
    {
        didSet{
            cc_profileStateHeight.constant = 0
        }
    }
    @IBOutlet weak var profiletitleLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!{
        didSet{
            let gradientImage = UIImage.gradientImage(with: progressView.frame,
                                                    colors: [fistColor.cgColor, lastColor.cgColor],
                                                    locations: nil)
            
            progressView.progressImage = gradientImage!
            progressView.layer.cornerRadius = progressView.frame.height / 2
            progressView.layer.masksToBounds = true
            progressView.setProgress(0.0, animated: true)
        }
    }
    
    var fullName:String = ""
    var address:String = ""
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        switch UIDevice().type {
        case .iPad2, .iPad3, .iPad4 , .iPad5 , .iPad6 , .iPad7 , .iPad8, .iPad9, .iPadAir, .iPadAir2, .iPadAir3, .iPadAir4, .iPadMini, .iPadMini2, .iPadMini3, .iPadMini4, .iPadMini5, .iPadMini6, .iPadPro9_7, .iPadPro10_5, .iPadPro11, .iPadPro2_11, .iPadPro3_11, .iPadPro12_9, .iPadPro2_12_9, .iPadPro3_12_9, .iPadPro4_12_9, .iPadPro5_12_9:
            self.bgViewHeight.constant = 400
            case .unrecognized:
                print("Device model unrecognized");
            default:break
        }
        DispatchQueue.main.async{
            self.coverView.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }
    
    func loadCell(_ profileCompleted :Double){
        if let fname = UserDefaults.standard.fetchString(key: "first_name") as? String, fname != ""{
            fullName = fname
        }
        
        if let lname = UserDefaults.standard.fetchString(key: "last_name") as? String, lname != ""{
            fullName = fullName + " " + lname
        }
        
        nameLabel.text = fullName
        
        if let city = UserDefaults.standard.fetchString(key: "city") as? String, city != ""{
            address = city
        }
        
        if let state = UserDefaults.standard.fetchString(key: "state") as? String, state != ""{
            address = address + ", " + state
        }
        
        cityLabel.text = address
        
        if let aboutMe = UserDefaults.standard.fetchString(key: "about_me") as? String,aboutMe != ""{
            aboutMeLabel.text = aboutMe
            aboutMeStateLabel.isHidden = false
            cc_aboutMeStateLabelHeight.constant = 21
            sepratorLabel.isHidden = false
        }else{
            aboutMeStateLabel.isHidden = true
            cc_aboutMeStateLabelHeight.constant = 0
            aboutMeLabel.text = ""
            sepratorLabel.isHidden = true
        }
        
        if let image = UserDefaults.standard.fetchString(key: "profilePic") as? String, image != ""{
            if image.isValidHttpsUrl{
                if let url = URL(string:image){
                    profileImageView.sd_setImage(with: url, placeholderImage: nil, options: [.progressiveLoad])

                }
            }
        }
        
        
        if profileCompleted < 100.0{
            profiletitleLabel.text = "Profile \(Int(profileCompleted))%"
            profileStatusView.isHidden = false
            cc_profileStateHeight.constant = 75
            progressView.setProgress(Float(profileCompleted / 100), animated: true)
        }else{
            profileStatusView.isHidden = true
            cc_profileStateHeight.constant = 0
        }
    }
    
    @IBAction func editProfileButtonAction(_ sender: Any) {
        CustomNavigationController().callEditProfileViewController()
    }
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        CustomNavigationController().callNotificationViewController()
    }
}

fileprivate extension UIImage {
    static func gradientImage(with bounds: CGRect,
                            colors: [CGColor],
                            locations: [NSNumber]?) -> UIImage? {

        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = colors
        // This makes it horizontal
        gradientLayer.startPoint = CGPoint(x: 0.0,
                                        y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0,
                                        y: 0.5)

        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return image
    }
}
