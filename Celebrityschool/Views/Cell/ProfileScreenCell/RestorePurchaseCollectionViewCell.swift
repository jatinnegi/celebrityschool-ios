//
//  RestorePurchaseCollectionViewCell.swift
//  Celebrityschool
//
//  Created by Hiren on 22/09/21.
//

import UIKit

class RestorePurchaseCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "RestorePurchaseCollectionViewCell"
    
    static var nib: UINib {
        get {
            return UINib(nibName: "RestorePurchaseCollectionViewCell", bundle: nil)
        }
    }
    
    @IBOutlet weak var restoreButton: UIButton!{
        didSet{
            restoreButton.layer.cornerRadius = 5
            restoreButton.layer.masksToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async{
            self.restoreButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }
    
    @IBAction func restorePurchaseButtonAction(_ sender: Any) {
        IAPManager.shared.beginRestorePurchases()
    }

}
