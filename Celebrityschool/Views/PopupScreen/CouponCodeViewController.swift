//
//  CouponCodeViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 25/10/21.
//

import UIKit

class CouponCodeViewController: UIViewController {
    let attributes = [
        NSAttributedString.Key.foregroundColor: UIColor(hexString: "#D3D6D7")!,
        NSAttributedString.Key.font : UIFont(name: "Roboto-Regular", size: 12)! // Note the !
    ]

    @IBOutlet weak var couponCodeTxtFiled: UITextField!{
        didSet{
            couponCodeTxtFiled.delegate = self
            couponCodeTxtFiled.keyboardType = .default
            couponCodeTxtFiled.autocapitalizationType = .allCharacters
            couponCodeTxtFiled.attributedPlaceholder = NSAttributedString(string: "Enter Coupon Code",
                                                                   attributes: attributes)
        }
    }
    @IBOutlet weak var bgView: UIView!{
        didSet{
            bgView.layer.cornerRadius = 8
            bgView.layer.masksToBounds = true
            bgView.layer.borderColor = UIColor(hexString: "#E4E4E4")?.cgColor
            bgView.layer.borderWidth = 1
        }
    }
    
    private var viewModel: CouponCodeApiServices!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CouponCodeApiServices()
        setupViewModel()
        self.view.layer.cornerRadius = 8
        self.view.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    
    func setupViewModel(){
        
        viewModel.didReceiveData = {[weak self] data in
            guard let strongSelf = self else {return}
            var data2 = OrderUpdateModel(dictionary: [:])
            data2.couponCode = true
            DispatchQueue.main.async {
                strongSelf.dismiss(animated: true) {
                    CustomNavigationController().callSuccessFailureViewController(data2, data)
                }
            }
        }
      
    }

    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func doneButtonAction(_ sender: Any) {
        self.couponCodeTxtFiled.resignFirstResponder()
        guard let couponCode = self.couponCodeTxtFiled.text, couponCode != "" else {
            if let topController = UIApplication.topViewController(){
                AlertIndicator_Toast().showToast("Please enter coupon code", vc: topController)
            }
            return
        }
        
        let props = [
            "promocode":couponCode ,
            "source": "ios"
            ] as [String : Any]

        self.couponCodeTxtFiled.text = ""
        self.viewModel.coponCodeApiCall(props)
       
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:UITextFiled Delegate
extension CouponCodeViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

