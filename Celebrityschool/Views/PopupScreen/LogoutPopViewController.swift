//
//  LogoutPopViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 08/09/21.
//

import UIKit

class LogoutPopViewController: UIViewController {

    // 1
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 16
        view.clipsToBounds = true
        return view
    }()
    
    // 2
    let maxDimmedAlpha: CGFloat = 0.6
    lazy var dimmedView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = maxDimmedAlpha
        return view
    }()
    
    lazy var okButton: UIButton = {
        let button = UIButton()
        button.setTitle("Yes", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = lastColor
        button.layer.cornerRadius = 8
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(logoutVc), for: .touchUpInside)
        return button
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle("No", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = fistColor
        button.layer.cornerRadius = 8
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(closeVc), for: .touchUpInside)
        return button
    }()
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = UIColor(hexString: "#0F0F0F")!
        label.textAlignment = .center
        label.text = "Log out?"
        label.font = UIFont(name: "Roboto-Medium", size: 20)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let msgLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textColor = UIColor(hexString: "#777777")!
        label.textAlignment = .center
        label.text = "You can always access your content by signing back in"
        label.font = UIFont(name: "Roboto-Regular", size: 16)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let defaultHeight: CGFloat = 300
    
    // 3. Dynamic container constraint
    var containerViewHeightConstraint: NSLayoutConstraint?
    var containerViewBottomConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
    }
    
    func setupView() {
        view.backgroundColor = .clear
    }
    
    func setupConstraints() {
        // 4. Add subviews
        view.addSubview(dimmedView)
        view.addSubview(containerView)
        view.addSubview(cancelButton)
        view.addSubview(okButton)
        view.addSubview(msgLabel)
        view.addSubview(headerLabel)
        dimmedView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        okButton.translatesAutoresizingMaskIntoConstraints = false
        msgLabel.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // 5. Set static constraints
        NSLayoutConstraint.activate([
            // set dimmedView edges to superview
            dimmedView.topAnchor.constraint(equalTo: view.topAnchor),
            dimmedView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            dimmedView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            dimmedView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            // set container static constraint (trailing & leading)
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 10),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -10),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10),
            
            // cancel Button
            cancelButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -20),
            cancelButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            cancelButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            cancelButton.heightAnchor.constraint(equalToConstant: 50),
            
            // ok Button
            okButton.bottomAnchor.constraint(equalTo: cancelButton.topAnchor, constant: -20),
            okButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            okButton.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            okButton.heightAnchor.constraint(equalToConstant: 50),
            
            // msg Button
            msgLabel.bottomAnchor.constraint(equalTo: okButton.topAnchor, constant: -30),
            msgLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            msgLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
            
            // msg Button
            headerLabel.bottomAnchor.constraint(equalTo: msgLabel.topAnchor, constant: -20),
            headerLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            headerLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 20),
        ])
        
        // 6. Set container to default height
        containerViewHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: defaultHeight)
        // 7. Set bottom constant to 0
        containerViewBottomConstraint = containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        // Activate constraints
        containerViewHeightConstraint?.isActive = true
        containerViewBottomConstraint?.isActive = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        dimmedView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func closeVc() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func logoutVc() {
        dismiss(animated: true) {
            AnalyticsManager.sharedInstance.logoutAndClearUserIdentity()
            CustomNavigationController().setRootviewAsLandingViewController()
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
