//
//  SuccessFailurePopupViewController.swift
//  Celebrityschool
//
//  Created by Hiren on 24/08/21.
//

import UIKit

class SuccessFailurePopupViewController: UIViewController {

    @IBOutlet weak var cc_bgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var allAccessPriceLabel: UILabel!
    @IBOutlet weak var coursePriceLabel: UILabel!{
        didSet{
            coursePriceLabel.text = ""
        }
    }
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var getAllView: UIView!{
        didSet{
            getAllView.layer.cornerRadius = 5
            getAllView.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var allAccessView: UIView!{
        didSet{
            allAccessView.backgroundColor = UIColor(hexString: "#F8F8F8")
            allAccessView.layer.cornerRadius = 5
            allAccessView.layer.masksToBounds =  true
        }
    }
    @IBOutlet weak var bgView: UIView!{
        didSet{
            bgView.layer.cornerRadius = 5
            bgView.layer.masksToBounds = true
            self.bgView.alpha = 0.0
        }
    }
    @IBOutlet weak var oneTimeOfferLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var buyNowButton: UIButton!{
        didSet{
            buyNowButton.layer.cornerRadius = 5
            buyNowButton.layer.masksToBounds = true
        }
    }
    
    @IBOutlet weak var failedView: UIView!{
        didSet{
            failedView.isHidden = true
        }
    }
    @IBOutlet var topView: [UIView]!{
        didSet{
            topView.forEach { $0.layer.cornerRadius = $0.frame.height / 2 }
            topView.forEach { $0.layer.masksToBounds = true }
            topView.forEach { $0.alpha = 0 }
        }
    }
    
    var OrderModel:OrderUpdateModel?
    var credit : Bool = false
    var CouponModel:CouponModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCircleLayers()
        self.setcircleView()
        
        if let model = OrderModel, let meta = model.meta, model.couponCode == false{
            if let statuscode = meta.statuscode , statuscode == 200{
                if let data = model.data{
                    if let palbum_id = Int(data.orderAlbum_id ?? "0"), palbum_id >= 101{
                        
                        if let coursName = data.course_name{
                            subTitleLabel.text = coursName
                        }else{
                            subTitleLabel.text = ""
                        }
                        
                    }else{
                        if let coursName = data.course_name{
                            subTitleLabel.text = "\(coursName) has been successfully purchased"
                        }else{
                            subTitleLabel.text = ""
                        }
                    }
                    
                    if let credit = data.Credit, credit == true{
                        failedView.isHidden = false
                        oneTimeOfferLabel.isHidden = true
                        cc_bgViewHeight.constant = 350
                        
                        if let amount = data.amount{
                            coursePriceLabel.text = "₹ \(amount)"
                        }
                        self.credit = true
                    }else{
                        if let amount = data.album_amount{
                            allAccessPriceLabel.text = "₹ \(amount)"
                        }
                    }
                    
                    AnalyticsManager.sharedInstance.SmarTechOnProductPurchase(data)
                }
                doneButton.setTitle("Done", for: .normal)
                TitleLabel.text = "Successfully Purchased"
                
            }else{
                if let data = model.data{
                    if let coursName = data.course_name{
                        subTitleLabel.text = "\(coursName) purchase has been failed"
                    }else{
                        subTitleLabel.text = ""
                    }
                    if let amount = data.amount{
                        coursePriceLabel.text = "₹ \(amount)"
                    }
                }
                doneButton.setTitle("TRY AGAIN", for: .normal)
                TitleLabel.text = "Purchase Failed"
                failedView.isHidden = false
                oneTimeOfferLabel.isHidden = true
                cc_bgViewHeight.constant = 350
            }
        }else{
            if let model = CouponModel,let meta = model.meta{
                if let statuscode = meta.statuscode , statuscode == 200{
                    if let data = model.data{
                        
                        if let coursName = data.course_name{
                            subTitleLabel.text = "You now have access to \(coursName) Course"
                        }else{
                            subTitleLabel.text = ""
                        }
                        
                        if let album_id = data.album_id, album_id == 0{
                            subTitleLabel.text = "You Now Have Access To All CelebritySchool Courses"
                        }
                       
                    }
                    TitleLabel.text = "Coupon Code Applied Successfully"
                }else{
                    subTitleLabel.text = "Retry with a valid coupon code"
                    TitleLabel.text = "Invalid Coupon Code"
                }
                
                doneButton.setTitle("CONTINUE", for: .normal)
                failedView.isHidden = true
                oneTimeOfferLabel.isHidden = true
                allAccessView.isHidden = true
                cc_bgViewHeight.constant = 250
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        self.showAnimate()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        DispatchQueue.main.async{
            self.buyNowButton.layerGradient(startPoint: .centerRight, endPoint: .centerLeft, colorArray: [fistColor.cgColor, lastColor.cgColor], type: .axial)
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}

// MARK: - UIButton Action
extension SuccessFailurePopupViewController{
    @IBAction func doneButtonAction(_ sender: Any) {
        if doneButton.titleLabel?.text == "Done"{
             if credit == true{
                NotificationCenter.default.post(name: Notification.Name("AllCorsePurchasedSuccesFully"), object: nil)
             }else{
                NotificationCenter.default.post(name: Notification.Name("CorsePurchasedSuccesFully"), object: nil)
             }
        }
        DispatchQueue.main.async{
            ProgressHUD.sharedInstance.hideProgressHUD()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buyNowButtonAction(_ sender: Any) {
        if let model = OrderModel,let meta = model.meta{
            if let statuscode = meta.statuscode , statuscode == 200{
                if let data = model.data{
                    
                    guard let plan_id = data.album_id else{return}
                    guard let a_price = data.album_amount else{return}
                    guard let product_id = data.product_id else{return}
                    
                    let payloadDictionary: [String : Any] = [
                        "celebrity_name" : plan_id ,
                        "course_category" : "All Course",
                        "amount" : Int(a_price)
                    ]
                    
                    AnalyticsManager.sharedInstance.SmartTechAllCourseBuy(payloadDictionary)
                    
                    DispatchQueue.global(qos: .userInitiated).async {
                        print("This is run on a background queue")
                        CourseApiServices().productBuyCall("\(plan_id)", a_price: "\(a_price)", product_id: product_id)
                        
                        DispatchQueue.main.async {
                            print("This is run on the main queue, after the previous code in outer block")
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
    
}

// MARK: - Design Part
extension SuccessFailurePopupViewController{
    private func setupCircleLayers() {
        let trackLayer = createCircleShapeLayer(strokeColor: .black, fillColor: .black)
        bgView.layer.addSublayer(trackLayer)
        
        let trackLayer2 = createCircleShapeLayer2(strokeColor: .black, fillColor: .black)
        bgView.layer.addSublayer(trackLayer2)
        if failedView.isHidden == false{
            bgView.makeDashedBorderLine()
        }
    }
    
    private func createCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()

        //The farther from 0 x is for this, the more separated the movements of the 3 paths.
        let circularPath = UIBezierPath(
            arcCenter: CGPoint(x: 0, y: bgView.frame.height / 2.3),
            radius: 15,
            startAngle: 0,
            endAngle: 2 * CGFloat.pi,
            clockwise: true
        )
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = 3
        layer.fillColor = fillColor.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        return layer
    }
    
    private func createCircleShapeLayer2(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()

        //The farther from 0 x is for this, the more separated the movements of the 3 paths.
        let circularPath = UIBezierPath(
            arcCenter: CGPoint(x:UIScreen.main.bounds.size.width - 30, y: bgView.frame.height / 2.3),
            radius: 15,
            startAngle: 0,
            endAngle: 2 * CGFloat.pi,
            clockwise: true
        )
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = 3
        layer.fillColor = fillColor.cgColor
        layer.lineCap = CAShapeLayerLineCap.round
        return layer
    }
    
    func showAnimate()
    {
        self.bgView.transform = self.bgView.transform.scaledBy(x: 0.001, y: 0.001)
        self.bgView.alpha = 1.0
        UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            self.bgView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
            self.topView.forEach { $0.alpha = 1.0 }
          }, completion: { _ in
           
        })
    }
    
    func setcircleView(){
        topView.forEach { $0.layer.masksToBounds = false }
        topView.forEach { $0.layer.shadowColor = UIColor.lightGray.cgColor }
        topView.forEach { $0.layer.shadowOffset = CGSize.zero }
        topView.forEach { $0.layer.shadowOpacity = 1 }
        topView.forEach { $0.layer.shadowRadius = 4 }
    }
}

extension UIView {
    private static let lineDashPattern: [NSNumber] = [8, 4]
    private static let lineDashWidth: CGFloat = 2.0

    func makeDashedBorderLine() {
        let path = CGMutablePath()
        let shapeLayer = CAShapeLayer()
        shapeLayer.lineWidth = UIView.lineDashWidth
        shapeLayer.strokeColor = UIColor(hexString: "#F8F8F8")?.cgColor
        shapeLayer.lineDashPattern = UIView.lineDashPattern
        path.addLines(between: [CGPoint(x: self.frame.minX + 8, y:  self.frame.height / 2.3),
                                CGPoint(x: self.frame.maxX + 18, y:  self.frame.height / 2.3)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
}
