//
//  NotificationViewController.swift
//  CelebrityschoolNCE
//
//  Created by Hiren on 03/12/21.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import SmartPush


class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet var customBgView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        SmartPush.sharedInstance().loadCustomNotificationContentView(customBgView)

        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        SmartPush.sharedInstance().didReceiveCustomNotification(notification)
    }
    
    func didReceive(_ response: UNNotificationResponse, completionHandler completion: @escaping (UNNotificationContentExtensionResponseOption) -> Void) {
            SmartPush.sharedInstance().didReceiveCustomNotificationResponse(response) { (option) in
                completion(option)
            }
        }

}
