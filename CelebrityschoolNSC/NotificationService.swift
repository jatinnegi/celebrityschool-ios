//
//  NotificationService.swift
//  CelebrityschoolNSC
//
//  Created by Hiren on 03/12/21.
//

import UserNotifications
import SmartPush


class NotificationService: UNNotificationServiceExtension {

//    var contentHandler: ((UNNotificationContent) -> Void)?
//    var bestAttemptContent: UNMutableNotificationContent?
    
    let smartechServiceExtension = SMTNotificationServiceExtension()


    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
//        self.contentHandler = contentHandler
//        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
//
//        if let bestAttemptContent = bestAttemptContent {
//            // Modify the notification content here...
//            bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
//
//            contentHandler(bestAttemptContent)
//        }
        
        if SmartPush.sharedInstance().isNotification(fromSmartech:request.content.userInfo){
            smartechServiceExtension.didReceive(request, withContentHandler: contentHandler)
          }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
//        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
//            contentHandler(bestAttemptContent)
//        }
        smartechServiceExtension.serviceExtensionTimeWillExpire()

    }

}
